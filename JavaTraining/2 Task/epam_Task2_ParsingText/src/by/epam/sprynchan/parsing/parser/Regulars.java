package by.epam.sprynchan.parsing.parser;

/**
 * Created by Кирилл on 09.04.2015.
 */
public class Regulars {
    public static final String WORD_REGEX = "([A-Za-zА-Яа-яёЁ]+)";
    public static final String LISTING_REGEX = "(void|class)(.{5,30})[\\{](.|\\n){10,400}[\\}].";
    public static final String PARAGRAPH_REGEX = "(.{2,}(?=[\\r\\n]))";
    public static final String SENTENCE_REGEX = "([А-ЯЁA-Z].{2,}?(([.?!])|(?=(\\n))))";
    public static final String SENTENCE_PART_REGEX = "(([А-Яа-яёЁA-Za-z]+)[^А-Яа-яёЁA-Za-z]+)";
    public static final String PUNCTUATION_REGEX = "[:;.!?,\"']";
}
