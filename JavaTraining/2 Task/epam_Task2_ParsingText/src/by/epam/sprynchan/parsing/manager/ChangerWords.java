package by.epam.sprynchan.parsing.manager;

import by.epam.sprynchan.parsing.entities.Sentence;
import by.epam.sprynchan.parsing.entities.Text;
import by.epam.sprynchan.parsing.entities.TextComponent;
import by.epam.sprynchan.parsing.entities.Word;

/**
 * Created by Кирилл on 09.04.2015.
 */
public class ChangerWords {
    public static Text change(Text text){
        for (Sentence elem : SentenceSorter.getAllSentences(text)){
            changeWordsOrder(elem);
        }
        return text;
    }


    public static void changeWordsOrder(Sentence sentence){
        boolean flag = false;
        for(TextComponent partOfSentence : sentence.getComponents())
        {
            if(partOfSentence.getClass() == Word.class)
            {
                String temp = ((Word) partOfSentence).getWord();
                for (int i = sentence.getComponents().size()-1; i >= 0;i--) {
                    if(sentence.getComponents().get(i).getClass() == Word.class){
                        ((Word) partOfSentence).setWord(((Word) sentence.getComponents().get(i)).getWord());
                        ((Word) sentence.getComponents().get(i)).setWord(temp);
                        flag = true;
                    }
                    if(flag)
                        break;
                }
            }
        }
    }
}
