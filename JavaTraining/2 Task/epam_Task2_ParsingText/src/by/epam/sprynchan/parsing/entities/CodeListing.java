package by.epam.sprynchan.parsing.entities;

/**
 * Created by Кирилл on 09.04.2015.
 */
public class CodeListing extends SentenceComponent {
    private String listing;

    public CodeListing(String listing) {
        this.listing = listing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o.getClass() != CodeListing.class) {
            return false;
        }

        CodeListing blockCode = (CodeListing) o;

        if (!listing.equals(blockCode.listing)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return listing.hashCode();
    }

    @Override
    public String toString() {
        return listing;
    }
}
