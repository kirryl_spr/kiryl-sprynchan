package by.epam.sprynchan.parsing.entities;

import java.util.List;

/**
 * Created by Кирилл on 09.04.2015.
 */
public interface TextComponent {
    void addComponent(TextComponent component);
    TextComponent getComponent(int index);
    List<TextComponent> getComponents();
    String toString();
}
