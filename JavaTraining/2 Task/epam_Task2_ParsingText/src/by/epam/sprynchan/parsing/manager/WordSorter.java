package by.epam.sprynchan.parsing.manager;

import by.epam.sprynchan.parsing.entities.Sentence;
import by.epam.sprynchan.parsing.entities.Text;
import by.epam.sprynchan.parsing.entities.TextComponent;
import by.epam.sprynchan.parsing.entities.Word;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Кирилл on 09.04.2015.
 */
public class WordSorter {
    public static List<Word> getSortedWords(Text text){
        List<Word> result = getAllWords(text);

        Comparator<Word> cp = new Comparator<Word>() {
            @Override
            public int compare(Word o1, Word o2) {
                return o1.toString().compareTo(o2.toString());
            }
        };

        Collections.sort(result, cp);
        return result;
    }

    private static List<Word> getAllWords(Text text)
    {
        List<Word> result = new ArrayList<Word>();
        for(Sentence sentence: SentenceSorter.getAllSentences(text))
        {
            for(TextComponent partOfSentence : sentence.getComponents())
            {
                if(partOfSentence.getClass() == Word.class)
                {
                    result.add((Word)partOfSentence);
                }
            }
        }
        return result;
    }
}
