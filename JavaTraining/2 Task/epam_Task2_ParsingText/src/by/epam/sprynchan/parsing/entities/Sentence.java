package by.epam.sprynchan.parsing.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Кирилл on 07.04.2015.
 */
public class Sentence implements TextComponent{
    private List<TextComponent> elementsOfSentence;

    public Sentence(){
        elementsOfSentence = new ArrayList<TextComponent>();
    }

    public void setElementsOfSentence(List<TextComponent> elementsOfSentence) {
        this.elementsOfSentence = elementsOfSentence;
    }

    @Override
    public void addComponent(TextComponent component) {
        elementsOfSentence.add(component);
    }

    @Override
    public TextComponent getComponent(int index) {
        return elementsOfSentence.get(index);
    }

    @Override
    public List<TextComponent> getComponents() {
        return elementsOfSentence;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        for(TextComponent se : elementsOfSentence) {
            res.append(se.toString());
        }
        return new String(res);
    }
}
