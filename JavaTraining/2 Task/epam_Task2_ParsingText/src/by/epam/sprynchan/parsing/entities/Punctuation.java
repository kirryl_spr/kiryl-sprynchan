package by.epam.sprynchan.parsing.entities;

/**
 * Created by Кирилл on 09.04.2015.
 */
public class Punctuation extends SentenceComponent {
    private String punctuation;

    public Punctuation(String punctuation) {
        this.punctuation = punctuation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){ return true;}
        if (o.getClass()!=Punctuation.class){ return false;}

        Punctuation punctuation1 = (Punctuation) o;

        if (!punctuation.equals(punctuation1.punctuation)){ return false;}

        return true;
    }

    @Override
    public int hashCode() {
        return punctuation.hashCode();
    }

    @Override
    public String toString() {
        return punctuation;
    }
}
