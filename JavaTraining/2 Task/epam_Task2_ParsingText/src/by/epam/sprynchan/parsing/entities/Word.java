package by.epam.sprynchan.parsing.entities;

/**
 * Created by Кирилл on 09.04.2015.
 */
public class Word extends SentenceComponent {
    private String word;
    private char firstLetter;

    public void setFirstLetter(char firstLetter) {
        this.firstLetter = firstLetter;
    }

    public char getFirstLetter() {
        return firstLetter;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Word(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){ return true;}
        if (o.getClass()!=Word.class){ return false;}

        Word word1 = (Word) o;

        if (!word.equals(word1.word)){ return false;}

        return true;
    }

    @Override
    public int hashCode() {
        return word.hashCode();
    }

    @Override
    public String toString() {
        return word ;
    }
}
