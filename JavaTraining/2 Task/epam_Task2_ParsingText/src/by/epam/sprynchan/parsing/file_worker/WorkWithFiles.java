package by.epam.sprynchan.parsing.file_worker;

import by.epam.sprynchan.parsing.entities.Sentence;
import by.epam.sprynchan.parsing.entities.Text;
import by.epam.sprynchan.parsing.entities.Word;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by Кирилл on 09.04.2015.
 */
public class WorkWithFiles {
    public static final Logger logger = Logger.getLogger(WorkWithFiles.class);

    public static void writeTextToFile(String filename, Text text){
        try {
            File file = new File(filename);
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(text.toString());
            fileWriter.close();
        }catch (FileNotFoundException e) {
            logger.info("can't find \"" + filename + "\" file");
            e.printStackTrace();
        }  catch (IOException e) {
            logger.fatal("error in writing to \"" + filename +"\" file");
            e.printStackTrace();
        }
    }

    public static void writeSentenciesToFile(String filename, List<Sentence> list){
        try {
            File file = new File(filename);
            FileWriter fileWriter = new FileWriter(file);
            for (Sentence elem : list) {
                fileWriter.write(elem.toString()+"\n");
            }

            fileWriter.close();
        }catch (FileNotFoundException e) {
            logger.info("can't find \"" + filename + "\" file");
            e.printStackTrace();
        }  catch (IOException e) {
            logger.fatal("error in writing to \"" + filename +"\" file");
            e.printStackTrace();
        }
    }

    public static void writeWordsToFile(String filename , List<Word> list){
        try{
            File file = new File(filename);
            FileWriter fileWriter = new FileWriter(file);

            String temp = list.get(0).getWord();
            char []word = temp.toCharArray();
            list.get(0).setFirstLetter(word[0]);

            fileWriter.write("\t" + list.get(0).toString() + "\n");
            for (int i = 1; i < list.size(); i++) {
                temp = list.get(i).getWord();
                word = temp.toCharArray();
                list.get(i).setFirstLetter(word[0]);

                if(list.get(i).getFirstLetter() != list.get(i-1).getFirstLetter()){
                    fileWriter.write("\t" + list.get(i).toString() + "\n");
                }
                else
                    fileWriter.write(list.get(i).toString() + "\n");

            }
            fileWriter.close();
        }
        catch (FileNotFoundException e) {
            logger.info("can't find \"" + filename + "\" file");
            e.printStackTrace();
        } catch (IOException e){
            logger.fatal("error in writing words to file " + filename + " file");
        }
    }
}
