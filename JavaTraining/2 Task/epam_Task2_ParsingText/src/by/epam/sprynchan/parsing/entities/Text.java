package by.epam.sprynchan.parsing.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Кирилл on 06.04.2015.
 */
public class Text implements TextComponent{
    private List<TextComponent> listOfPargraphs;

    public Text(){
        listOfPargraphs = new ArrayList<TextComponent>();
    }

    @Override
    public void addComponent(TextComponent component) {
        listOfPargraphs.add(component);
    }

    @Override
    public TextComponent getComponent(int index) {
        return listOfPargraphs.get(index);
    }

    @Override
    public List<TextComponent> getComponents() {
        return listOfPargraphs;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(TextComponent elem : listOfPargraphs){
            sb.append(elem.toString()).append("\n");
        }
        return sb.toString();
    }


}
