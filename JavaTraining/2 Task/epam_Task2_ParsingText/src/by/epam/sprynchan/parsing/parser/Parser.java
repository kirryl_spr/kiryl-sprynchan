package by.epam.sprynchan.parsing.parser;

import by.epam.sprynchan.parsing.entities.*;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Кирилл on 07.04.2015.
 */
public class Parser {
    private static final Logger logger = Logger.getLogger(Parser.class);
    public static TextComponent parseText(String textToParsing) {
        logger.info("Parsing of text was started.");
        Text text = new Text();
        Pattern codePattern = Pattern.compile(Regulars.LISTING_REGEX);
        Pattern paragraphPattern = Pattern.compile(Regulars.PARAGRAPH_REGEX);
        Matcher codeMatcher = codePattern.matcher(textToParsing);
        int start = 0;
        int nextStart;
        int end = 0;
        while (end != textToParsing.length()) {
            if (codeMatcher.find(start)) {
                end = codeMatcher.start();
                nextStart = codeMatcher.end(0);
            } else {
                end = textToParsing.length();
                nextStart = -1;
            }
            String paragraphs = textToParsing.substring(start, end);
            Matcher paragraphMatcher = paragraphPattern.matcher(paragraphs);
            while (paragraphMatcher.find()) {
                text.addComponent(parseParagraph(paragraphMatcher.group(0) + "\n"));
            }
            if (end != textToParsing.length()) {
                text.addComponent(new CodeListing(codeMatcher.group(0)));
            }
            start = nextStart;
        }
        logger.info("Text was parsed.");
        return text;
    }

    public static TextComponent parseParagraph(String textForParsing) {
        Paragraph paragraph = new Paragraph();

        Pattern sentenceParser = Pattern.compile(Regulars.SENTENCE_REGEX);
        Matcher sentenceMatcher = sentenceParser.matcher(textForParsing);

        int start=0;
        int nextStart=0;
        int end=0;
        while(end!= textForParsing.length()) {
            if (sentenceMatcher.find(start)) {
                end = sentenceMatcher.start(0);
                nextStart = sentenceMatcher.end(0);
            } else {
                end = textForParsing.length();
                nextStart = -1;
            }
            String unknown = textForParsing.substring(start, end);
            if(!unknown.matches("\\s+")) {
                if(unknown.contains("\n"))
                {
                    unknown = unknown.replace("\n","");
                }
                if(unknown.contains("\t"))
                {
                    unknown = unknown.replace("\t","");
                }
                paragraph.addComponent(new UnknownPart(unknown));
            }
            start = nextStart;
            if(end!=textForParsing.length())
            {
                paragraph.addComponent(parseSentence(sentenceMatcher.group(0)));
            }
        }
        return paragraph;
    }

    public static TextComponent parseSentence(String textForParsing) {
        Sentence sentence= new Sentence();
        Pattern sentencePartPattern = Pattern.compile(Regulars.SENTENCE_PART_REGEX);
        Pattern wordPattern = Pattern.compile(Regulars.WORD_REGEX);
        Pattern punctuationPattern = Pattern.compile(Regulars.PUNCTUATION_REGEX);
        Matcher sentencePartMatcher = sentencePartPattern.matcher(textForParsing);
        while(sentencePartMatcher.find())
        {
            String sentencePart = sentencePartMatcher.group(0);
            Matcher wordMatcher = wordPattern.matcher(sentencePart);
            String word;
            if(wordMatcher.find()) {
                word = wordMatcher.group(0);
                sentence.addComponent(new Word(word));
                sentencePart = sentencePart.replace(word, "");
                char[] sentencePartArray = sentencePart.toCharArray();
                for (Character ch : sentencePartArray) {
                    if (ch.compareTo(' ') == 0) {
                        sentence.addComponent(new Space());
                    } else if (punctuationPattern.matcher(ch.toString()).matches()) {
                        sentence.addComponent(new Punctuation(ch.toString()));
                    } else {
                        sentence.addComponent(new UnknownPart(ch.toString()));
                    }
                }
            }
        }

        return sentence;
    }
}