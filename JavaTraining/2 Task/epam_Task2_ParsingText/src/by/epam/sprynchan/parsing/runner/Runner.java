package by.epam.sprynchan.parsing.runner;

import by.epam.sprynchan.parsing.entities.Text;
import by.epam.sprynchan.parsing.file_worker.WorkWithFiles;
import by.epam.sprynchan.parsing.manager.ChangerWords;
import by.epam.sprynchan.parsing.manager.SentenceSorter;
import by.epam.sprynchan.parsing.manager.WordSorter;
import by.epam.sprynchan.parsing.parser.Parser;
import org.apache.log4j.Logger;

import java.io.*;

/**
 * Created by Кирилл on 07.04.2015.
 */
public class Runner {
    private static final Logger logger = Logger.getLogger(Runner.class);

    public static void main(String[] args) {
        Text text = (Text) Parser.parseText(getTextFromFile());

        WorkWithFiles.writeTextToFile("file_out", text);
        WorkWithFiles.writeSentenciesToFile("file_with_sorted_sentencies", SentenceSorter.getSortedSentences(text));
        WorkWithFiles.writeTextToFile("file_with_changed_sentencies", ChangerWords.change(text));
        WorkWithFiles.writeWordsToFile("file_with_words", WordSorter.getSortedWords(text));

    }

    public static String getTextFromFile() {

        String fileName = "file_in";
        String result = "";

        StringBuilder sb = new StringBuilder();
        try {
            File file = new File(fileName);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            StringBuffer text = new StringBuffer();
            while(bufferedReader.ready()) {
                text.append(bufferedReader.readLine() + "\n");
            }
            result = text.toString();
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            logger.info("can't find \"" + fileName + "\" file");
            e.printStackTrace();
        } catch (IOException e) {
            logger.info("error in reading \"" + fileName + "\" file");
            e.printStackTrace();
        }
        return result;
    }
}
