package by.epam.sprynchan.parsing.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Кирилл on 07.04.2015.
 */
public class Paragraph implements TextComponent{
    private List<TextComponent> listOfSentencies;

    public Paragraph(){
        listOfSentencies = new ArrayList<TextComponent>();
    }

    @Override
    public void addComponent(TextComponent component) {
        listOfSentencies.add(component);
    }

    @Override
    public TextComponent getComponent(int index) {
        return listOfSentencies.get(index);
    }

    @Override
    public List<TextComponent> getComponents() {
        return listOfSentencies;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        for(TextComponent sent : listOfSentencies) {
            res.append(sent.toString());
        }
        return new String(res);
    }
}
