package by.epam.sprynchan.parsing.entities;

/**
 * Created by Кирилл on 09.04.2015.
 */
public class UnknownPart extends SentenceComponent {
    private String part;

    public UnknownPart(String part) {
        this.part = part;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){ return true;}
        if (o.getClass()!=UnknownPart.class){ return false;}

        UnknownPart that = (UnknownPart) o;

        if (!part.equals(that.part)){ return false;}

        return true;
    }

    @Override
    public int hashCode() {
        return part.hashCode();
    }

    @Override
    public String toString() {
        return part;
    }
}
