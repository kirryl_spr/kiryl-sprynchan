package by.epam.sprynchan.parsing.entities;

import java.util.List;

/**
 * Created by Кирилл on 09.04.2015.
 */
public abstract class SentenceComponent implements TextComponent{
    @Override
    public void addComponent(TextComponent component) {
        throw new UnsupportedOperationException();
    }

    @Override
    public TextComponent getComponent(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<TextComponent> getComponents() {
        throw new UnsupportedOperationException();
    }

}
