package by.epam.sprynchan.parsing.manager;

import by.epam.sprynchan.parsing.entities.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Кирилл on 09.04.2015.
 */
public class SentenceSorter {

    public static List<Sentence> getSortedSentences(Text text)
    {
        List<Sentence> result = getAllSentences(text);
        Comparator<Sentence> cp = new Comparator<Sentence>() {
            @Override
            public int compare(Sentence o1, Sentence o2) {
                return Integer.compare(getNumberOfWords(o1),getNumberOfWords(o2));
            }
        };

        Collections.sort(result, cp);
        return result;
    }

    public static List<Sentence> getAllSentences(Text text)
    {
        List<Sentence> result = new ArrayList<Sentence>();
        for(TextComponent partOfText : text.getComponents())
        {
            if(partOfText.getClass() == Paragraph.class)
            {
                for(TextComponent partOfParagraph : partOfText.getComponents())
                {
                    if(partOfParagraph.getClass() == Sentence.class)
                    {
                        result.add((Sentence)partOfParagraph);
                    }
                }
            }
        }
        return result;
    }

    private static int getNumberOfWords(Sentence sentence)
    {
        int numberOfwords=0;
        for(TextComponent partOfSentence : sentence.getComponents())
        {
            if(partOfSentence.getClass() == Word.class)
            {
                numberOfwords++;
            }
        }
        return numberOfwords;
    }
}
