<?xml version="1.0" encoding="UTF-8"?>
            <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <head>
               <title>Flowers</title>
            </head>
            <body>
                <h1 align="center">Flowers in greenhouse in order with growing temperature.</h1>
                <table border="1" align="center">
                    <tr>
                        <td rowspan="2">ID</td>
                        <td rowspan="2">Name</td>
                        <td rowspan="2">Temperature</td>
                        <td rowspan="2">Soil</td>
                        <td rowspan="2">Origin</td>
                        <td rowspan="2">StemColor</td>
                        <td rowspan="2">LeafColor</td>
                        <td rowspan="2">AverageSize</td>
                        <td rowspan="2">Lighting</td>
                        <td rowspan="2">Watering</td>
                        <td rowspan="2">Multiplying</td>
                    </tr>
                    <xsl:for-each select="Flower/flower">
                        <xsl:sort select="number(GrowingTips/Temperature)" order="ascending"/>
                    <tr>

                        <tr bgcolor="#F5F5F5">
                            <td>
                                <xsl:value-of select="@ID"/>
                            </td>
                            <td>
                                <xsl:value-of select="Name"/>
                            </td>
                            <td>
                                <xsl:value-of select="GrowingTips/Temperature"/>
                            </td>
                            <td>
                                <xsl:value-of select="Soil"/>
                            </td>
                            <td>
                                <xsl:value-of select="Origin"/>
                            </td>
                            <td>
                                <xsl:value-of select="VisualParametres/StemColor"/>
                            </td>
                            <td>
                                <xsl:value-of select="VisualParametres/LeafColor"/>
                            </td>
                            <td>
                                <xsl:value-of select="VisualParametres/AverageSize"/>
                            </td>
                            <td>
                                <xsl:value-of select="GrowingTips/Lighting"/>
                            </td>
                            <td>
                                <xsl:value-of select="GrowingTips/Watering"/>
                            </td>
                            <td>
                                <xsl:value-of select="Multiplying"/>
                            </td>
                        </tr>
                    </tr>

                </xsl:for-each>
                </table>

            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>