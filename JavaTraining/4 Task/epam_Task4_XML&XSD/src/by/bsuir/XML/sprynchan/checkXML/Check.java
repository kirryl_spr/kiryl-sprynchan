package by.bsuir.XML.sprynchan.checkXML;

import org.apache.xerces.parsers.DOMParser;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import java.io.IOException;


public class Check {
    public static void main(String[] args) {
        String filename = "Greenhouse.xml";
        DOMParser parser = new DOMParser();
        boolean ifChecked = true;

        try {
// установка обработчика ошибок
            parser.setErrorHandler(new MyErrorHandler("log.txt"));
// установка способов проверки с использованием XSD
            parser.setFeature(
                    "http://xml.org/sax/features/validation", true);
            parser.setFeature(
                    "http://apache.org/xml/features/validation/schema", true);
            parser.parse(filename);
        } catch (SAXNotRecognizedException e) {
            e.printStackTrace();
            System.out.print("идентификатор не распознан");
        } catch (SAXNotSupportedException e) {
            e.printStackTrace();
            System.out.print("неподдерживаемая операция");
        } catch (SAXException e) {
            e.printStackTrace();
            System.out.print("глобальная SAX ошибка ");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.print("ошибка I/O потока");
        }
        System.out.print("проверка " + filename + " завершена");
    }
}
