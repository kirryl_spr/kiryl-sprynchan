package by.bsuir.XML.sprynchan.checkXML;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;
/**
 * Created by Кирилл on 22.04.2015.
 */

import java.io.IOException;
public class MyErrorHandler implements ErrorHandler {
    private Logger logger;
    public MyErrorHandler(String log) throws IOException {
//создание регистратора ошибок chapt16.xsd
        logger = Logger.getLogger("Greenhouse-sort.xsl.xsd");
//установка файла и формата вывода ошибок
        logger.addAppender(new FileAppender(
                new SimpleLayout(), log));
    }
    public void warning(SAXParseException e) {
        logger.warn(getLineAddress(e) + "-" +
                e.getMessage());
    }
    public void error(SAXParseException e) {
        logger.error(getLineAddress(e) + " - "
                + e.getMessage());
    }
    public void fatalError(SAXParseException e) {
        logger.fatal(getLineAddress(e) + " - "
                + e.getMessage());
    }
    private String getLineAddress(SAXParseException e) {
//определение строки и столбца ошибки
        return e.getLineNumber() + " : "
                + e.getColumnNumber();
    }
}
