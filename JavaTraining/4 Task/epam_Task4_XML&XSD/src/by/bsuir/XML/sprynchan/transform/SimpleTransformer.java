package by.bsuir.XML.sprynchan.checkXML.transform;

/**
 * Created by Кирилл on 23.04.2015.
 */

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
public class SimpleTransformer {
    public static void main(String[] args) {
        try { TransformerFactory tf =  TransformerFactory.newInstance();
//установка используемого XSL-преобразования
         Transformer transformer =  tf.newTransformer(new StreamSource("Greenhouse-sort.xsl"));
//установка исходного XML-документа и конечного XML-файла
            transformer.transform( new StreamSource("GreenhouseCopy.xml"), new StreamResult("GreenhouseSorted.html"));
            System.out.print("complete");
        } catch(TransformerException e) { e.printStackTrace(); }
}
}
