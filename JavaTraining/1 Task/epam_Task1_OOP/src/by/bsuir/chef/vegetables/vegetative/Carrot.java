package by.bsuir.chef.vegetables.vegetative;

/**
 * Carrot entity.
 */
public class Carrot extends Vegetative{
    private String countryOfRigin;

    public Carrot(String countryOfRigin, String color, double calorificValue, int vegetablePrice) {
        super(color, calorificValue, vegetablePrice);
        this.countryOfRigin = countryOfRigin;
    }

    public String getCountryOfRigin() {
        return countryOfRigin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (getClass() == o.getClass()){
            Carrot temp = (Carrot) o;
            return this.countryOfRigin.equals(temp.countryOfRigin);
        } else
            return false;
    }

    @Override
    public int hashCode() {
        return (int)(31 * ((countryOfRigin == null) ? 0 : countryOfRigin.hashCode()));
    }

    @Override
    public String toString() {
        return "Carrot{" +
                "countryOfRigin='" + countryOfRigin + '\'' +" color:" + color + " colorificValue = " + calorificValue
                + " price for killogramm = " + priceForKillogramm +
                '}'+ '\n';
    }
}
