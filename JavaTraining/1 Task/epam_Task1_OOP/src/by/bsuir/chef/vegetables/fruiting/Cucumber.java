package by.bsuir.chef.vegetables.fruiting;

/**
 * Entity cucumber.
 */
public class Cucumber extends Fruiting{
    private int size;

    public Cucumber(int size, String pack, double calorificValue, int priceForKillogramm) {
        super(pack, calorificValue, priceForKillogramm);
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (getClass() == o.getClass()){
            Cucumber temp = (Cucumber) o;
            return this.size == (temp.size);
        } else
            return false;
    }

    @Override
    public int hashCode() {
        return (int)(31 * size);
    }

    @Override
    public String toString() {
        return "Cucumber{"+
                "size: '" + size + '\'' +" pack:" + pack + " colorificValue = " + calorificValue
                + " price for killogramm = " + priceForKillogramm +
                '}'+ '\n';
    }
}
