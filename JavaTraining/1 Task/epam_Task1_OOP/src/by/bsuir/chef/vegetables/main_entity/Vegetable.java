package by.bsuir.chef.vegetables.main_entity;

/**
 * Abstract class describing vegetable entity.
 */
public abstract class Vegetable {
    protected double calorificValue;
    protected int priceForKillogramm;

    public Vegetable(double calorificValue, int priceForKillogramm) {
        this.calorificValue = calorificValue;
        this.priceForKillogramm = priceForKillogramm;
    }

    public double getCalorificValue() {
        return calorificValue;
    }

    public int getPriceForKillogramm() {
        return priceForKillogramm;
    }
}
