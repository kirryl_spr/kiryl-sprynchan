package by.bsuir.chef.vegetables.vegetative;

/**
 *Pepper entity.
 */
public class Pepper extends Vegetative {
    private String freshness;

    public Pepper(String freshness, String color, double calorificValue, int vegetablePrice) {
        super(color, calorificValue, vegetablePrice);
        this.freshness = freshness;
    }

    public String getFreshness() {
        return freshness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (getClass() == o.getClass()){
            Pepper temp = (Pepper) o;
            return this.freshness.equals(temp.freshness);
        } else
            return false;
    }

    @Override
    public int hashCode() {
        return (int)(31 * ((freshness == null) ? 0 : freshness.hashCode()));
    }

    @Override
    public String toString() {
        return "Pepper{" +
                "freshness: '" + freshness + '\'' +" color:" + color + " colorificValue = " + calorificValue
                + " price for killogramm = " + priceForKillogramm +
                '}'+ '\n';
    }
}
