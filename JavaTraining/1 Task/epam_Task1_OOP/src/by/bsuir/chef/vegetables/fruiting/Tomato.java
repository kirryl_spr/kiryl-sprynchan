package by.bsuir.chef.vegetables.fruiting;

/**
 *Tomato entity.
 */
public class Tomato extends Fruiting{
    private String sort;

    public Tomato(String sort, String pack, double calorificValue, int priceForKillogramm) {
        super(pack, calorificValue, priceForKillogramm);
        this.sort = sort;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (getClass() == o.getClass()){
            Tomato temp = (Tomato) o;
            return this.sort.equals(temp.sort);
        } else
            return false;
    }

    @Override
    public int hashCode() {
        return (int)(31 * ((sort == null) ? 0 : sort.hashCode()));
    }
    @Override
    public String toString() {
        return "Tomato{"+
                "sort: '" + sort + '\'' +" pack:" + pack + " colorificValue = " + calorificValue
                + " price for killogramm = " + priceForKillogramm +
                '}'+ '\n';
    }

}
