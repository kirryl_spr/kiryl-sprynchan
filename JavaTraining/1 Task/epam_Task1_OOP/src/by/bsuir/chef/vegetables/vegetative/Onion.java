package by.bsuir.chef.vegetables.vegetative;

/**
 *Onion entity.
 */
public class Onion extends Vegetative {
    protected String fortress;

    public Onion(String fortress, String color, double calorificValue, int vegetablePrice) {
        super(color, calorificValue, vegetablePrice);
        this.fortress = fortress;
    }

    public String getFortress() {
        return fortress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (getClass() == o.getClass()){
            Onion temp = (Onion) o;
            return this.fortress.equals(temp.fortress);
        } else
            return false;
    }

    @Override
    public int hashCode() {
        return (int)(31 * ((fortress == null) ? 0 : fortress.hashCode()));
    }


    @Override
    public String toString() {
        return "Onion{" +
                "fortress: '" + fortress + '\'' +" color:" + color + " colorificValue = " + calorificValue
                + " price for killogramm = " + priceForKillogramm +
                '}'+ '\n';
    }
}
