package by.bsuir.chef.vegetables.vegetative;

import by.bsuir.chef.vegetables.main_entity.Vegetable;

/**
 * Class for vegetative vegetables.
 */
public abstract class Vegetative extends Vegetable{
    protected String color;

    public Vegetative(String color, double colorificValue, int priceForKillogramm) {
        super(colorificValue, priceForKillogramm);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Vegetative{" +
                "color='" + color + '\'' +
                '}'+ '\n';
    }
}
