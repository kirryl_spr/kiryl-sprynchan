package by.bsuir.chef.vegetables.fruiting;

import by.bsuir.chef.vegetables.main_entity.Vegetable;

/**
 * Class for Fruiting vegetables.
 */
public abstract class Fruiting extends Vegetable{
    protected String pack;

    public Fruiting(String pack, double calorificValue, int priceForKillogramm) {
        super(calorificValue, priceForKillogramm);
        this.pack = pack;
    }

    public String getPack() {
        return pack;
    }

    @Override
    public String toString() {
        return "Fruiting{" +
                "pack='" + pack + '\'' +
                '}';
    }
}
