package by.bsuir.chef.main;

import by.bsuir.chef.builder.DOMBuilder;
import by.bsuir.chef.builder.UsualBuilder;
import by.bsuir.chef.builder.base.BaseBuilder;
import by.bsuir.chef.builder.salad.Salad;
import by.bsuir.chef.operations.OperationsWithSalad;
import by.bsuir.chef.vegetables.main_entity.Vegetable;

import java.util.ArrayList;


/**
 * Main class for application.
 */
public class RunnerSalad {
    /**
     * Methods to build chef with define builder.
     *
     * @param builder Way to build chef.
     * @return chef Builded chef.
     */
    private static Salad build(BaseBuilder builder){
        return builder.buildSalad();
    }

    /**
     * Main method that runs the application.
     * @param args Arguments for main.
     */
    public static void main(String[] args) {
        Salad usualSalad = build(new UsualBuilder());
        Salad xmlSalad = build(new DOMBuilder());

        System.out.println("Salad built using UsualBuilder:\n" + usualSalad.toString());

        OperationsWithSalad.sortingByCalorificValue(usualSalad.getIngredients());

        System.out.println("Salad after sorting:\n" + usualSalad.toString());

        ArrayList<Vegetable> foundedVegetables = OperationsWithSalad.searchingByCalorificValue(usualSalad, 100, 200);

        System.out.println(foundedVegetables);

        System.out.println("Salad built using DOMBuilder:\n" + xmlSalad.toString());


    }
}
