package by.bsuir.chef.operations;

import by.bsuir.chef.builder.salad.Salad;
import by.bsuir.chef.vegetables.main_entity.Vegetable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Class created for making different operatins with chef.
 */

public class OperationsWithSalad {

    /**
     * Count calorific value for the chef and return amount of calories.
     *
     * @param salad Salad for counting.
     * @return saladCalorificValue Returns counted calories for the chef.
 */

    public static double countingCalorifieForSalad(Salad salad){
        double saladCalorificValue = 0;

        for(Vegetable vegetable : salad.getIngredients()){
            saladCalorificValue += vegetable.getCalorificValue();
        }

        return saladCalorificValue;
    }

    /**
     * Search vegetables in the chef whose calorific value between bottom and top.
     *
     * @param salad Salad that we get
     * @param bottom bottom of calorific value
     * @param top top of calorific value
     * @return saladCalorificValue Returns list of founded vegetables.
     */

    public static ArrayList<Vegetable> searchingByCalorificValue(Salad salad, double bottom, double top){
        ArrayList<Vegetable> searchedVegetables = new ArrayList<Vegetable>();
        for (Vegetable vegetable : salad.getIngredients()){
            if(vegetable.getCalorificValue() >= bottom && vegetable.getCalorificValue() <= top){
                searchedVegetables.add(vegetable);
            }
        }
        return searchedVegetables;
    }

    /**
     * Sorting chef by calorific value of each vegetable.
     *
     * @param ingredients list of ingredients in chef got.
     */

    public static void sortingByCalorificValue(List<Vegetable> ingredients){
        Comparator<Vegetable> comp = new Comparator<Vegetable>() {
            @Override
            public int compare(Vegetable o1, Vegetable o2) {
                return Double.compare(o1.getCalorificValue(), o2.getCalorificValue());
            }
        };
        Collections.sort(ingredients, comp);
    }

    /**
     * Sorting chef by price of each vegetable.
     *
     * @param ingredients list of ingredients in chef got.
     */

    public static void sortingByPrice(List<Vegetable> ingredients){
        Comparator<Vegetable> comp = new Comparator<Vegetable>() {
            @Override
            public int compare(Vegetable o1, Vegetable o2) {
                return Double.compare(o1.getPriceForKillogramm(), o2.getPriceForKillogramm());
            }
        };
        Collections.sort(ingredients, comp);
    }
}
