package by.bsuir.chef.builder.base;

import by.bsuir.chef.builder.salad.Salad;

/**
 * Abstract class for Builder.
 */
public abstract class BaseBuilder {
    protected Salad salad = new Salad();

    public Salad getSalad() {
        return salad;
    }

    /**
     * Method that will be overrided.
     *
     * @return salad Returns built salad with the chosen way.
     */
    public abstract Salad buildSalad();
}
