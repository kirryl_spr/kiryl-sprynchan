package by.bsuir.chef.builder;

import by.bsuir.chef.builder.base.BaseBuilder;
import by.bsuir.chef.builder.salad.Salad;
import by.bsuir.chef.vegetables.fruiting.Tomato;
import by.bsuir.chef.vegetables.vegetative.Carrot;
import by.bsuir.chef.vegetables.vegetative.Onion;
import by.bsuir.chef.vegetables.vegetative.Pepper;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;


/**
 * Class designed to build chef from XML-file InputXmLData.xml
 */
public class DOMBuilder extends BaseBuilder {

    private DocumentBuilder documentBuilder;
    private static final Logger logger = Logger.getLogger(DOMBuilder.class);

    public DOMBuilder() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException PCE) {
            System.out.println("Error parser configuration!!!" + PCE);
        }
    }

    /**
     * Method that builds chef parsing InputXMLData.xml
     * @return chef with vegetables parsed from xml-file.
     */
    @Override
    public Salad buildSalad() {

        Document doc = null;
        Salad salad = new Salad();

        try {
            doc = documentBuilder.parse("src/InputXMLData.xml");
            Element root = doc.getDocumentElement();
            NodeList vegetableList = root.getElementsByTagName("vegetable");

            for (int i = 0; i < vegetableList.getLength(); i++) {
                Element element = (Element) vegetableList.item(i);

                switch (element.getAttribute("type")) {
                    case "Carrot":
                        Carrot carrot = new Carrot(element.getElementsByTagName("countryOfRigin").item(0).getTextContent(),
                                element.getElementsByTagName("color").item(0).getTextContent(),
                                Double.parseDouble(element.getElementsByTagName("calorificValue").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent()));
                        salad.getIngredients().add(carrot);
                        break;
                    case "Onion":
                        Onion onion = new Onion(element.getElementsByTagName("fortress").item(0).getTextContent(),
                                element.getElementsByTagName("color").item(0).getTextContent(),
                                Double.parseDouble(element.getElementsByTagName("calorificValue").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent()));
                        salad.getIngredients().add(onion);
                        break;
                    case "Tomato":
                        Tomato tomato = new Tomato(element.getElementsByTagName("sort").item(0).getTextContent(),
                                element.getElementsByTagName("pack").item(0).getTextContent(),
                                Double.parseDouble(element.getElementsByTagName("calorificValue").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent()));
                        salad.getIngredients().add(tomato);
                        break;
                    case "Pepper":
                        Pepper pepper = new Pepper(element.getElementsByTagName("freshness").item(0).getTextContent(),
                                element.getElementsByTagName("color").item(0).getTextContent(),
                                Double.parseDouble(element.getElementsByTagName("calorificValue").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent()));
                        salad.getIngredients().add(pepper);
                        break;
                }
            }
        } catch (IOException e) {
            logger.info("IO Exception" + e);
        } catch (SAXException e) {
            logger.info("Parsing failure" + e);
        }
        return salad;
    }
}
