package by.bsuir.chef.builder;

import by.bsuir.chef.builder.base.BaseBuilder;
import by.bsuir.chef.vegetables.fruiting.Cucumber;
import by.bsuir.chef.vegetables.vegetative.Onion;
import by.bsuir.chef.vegetables.vegetative.Pepper;
import by.bsuir.chef.vegetables.fruiting.Tomato;
import by.bsuir.chef.builder.salad.Salad;

/**
 *Class for building chef with the usual constructors.
 *Create different vegetables and adds them into the chef.
 */
public class UsualBuilder extends BaseBuilder {
    @Override
    public Salad buildSalad() {

        Tomato firstTomato = new Tomato("First Sort", "Pack", 115.3, 20);
        Onion firstOnion = new Onion("Strong", "Green", 140.4, 30);
        Pepper firstPepper = new Pepper("Fresh", "Red",  154.23, 40);
        Cucumber firstCucumber = new Cucumber(2, "Packet", 65.4, 10);

        Salad salad = new Salad();
        salad.getIngredients().add(firstTomato);
        salad.getIngredients().add(firstOnion);
        salad.getIngredients().add(firstPepper);
        salad.getIngredients().add(firstCucumber);

        return salad;
    }
}
