package by.bsuir.chef.builder.salad;

import by.bsuir.chef.vegetables.main_entity.Vegetable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class describing entity of chef.
 */
public class Salad {
    private List<Vegetable> ingredients;

    public Salad(){
        ingredients = new ArrayList<Vegetable>();
    }

    public List<Vegetable> getIngredients() {
        return ingredients;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Vegetable vegetable : ingredients)
            sb.append(vegetable);
        return sb.toString();
    }
}
