package main;

import center.CallCenter;
import center.CallCenterException;
import client.Client;

/**
 * Created by Кирилл on 15.04.2015.
 */
public class Runner {
    public static void main(String[] args) throws InterruptedException, CallCenterException {
        CallCenter callCenter = new CallCenter(4);

        Client client1 = new Client(callCenter, 150, 1);
        Client client2 = new Client(callCenter, 100, 2);
        Client client3 = new Client(callCenter, 160, 3);
        Client client4 = new Client(callCenter, 120, 4);
        Client client5 = new Client(callCenter, 90, 5);


        new Thread(client1).start();
        new Thread(client2).start();
        new Thread(client3).start();
        new Thread(client4).start();
        new Thread(client5).start();

        Thread.sleep(300);

        client1.stopThread();
        client2.stopThread();
        client3.stopThread();
        client4.stopThread();
        client5.stopThread();
    }
}
