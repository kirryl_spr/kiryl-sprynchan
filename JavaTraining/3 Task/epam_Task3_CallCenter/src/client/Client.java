package client;

import center.CallCenter;
import center.CallCenterException;
import center.Operator;
import org.apache.log4j.Logger;

/**
 * Created by Кирилл on 15.04.2015.
 */
public class Client implements Runnable{
    private final static Logger logger = Logger.getRootLogger();
    private volatile boolean stopThread = false;

    private CallCenter callCenter;
    private int timeout;
    private int id;

    public Client(CallCenter callCenter, int timeout, int id) {
        this.callCenter = callCenter;
        this.timeout = timeout;
        this.id = id;
    }

    public void stopThread(){
        stopThread = true;
    }

    public int getId() {
        return id;
    }

    public void run() {
        try {
            while (!stopThread) {
                call();
                hangUp();
            }
        } catch (InterruptedException e) {
            logger.error("Error.", e);
        } catch (CallCenterException e) {
            logger.error("Ошибка в получении оператора.", e);
        }
    }

    public void hangUp() throws InterruptedException, CallCenterException {
        Thread.sleep(this.timeout);
        logger.info("Клиент " + this.getId() + "  не дождался ответа и положил трубку.");
        this.call();
    }
    // выкидвает CallCenterException из метода getOperator(), catch в методе run()
    private void  call() throws  CallCenterException{
        boolean isLockedOperator = false;
        boolean isRedirect = false;
        boolean needToRedirect = false;
        Operator operator = null;

        try{
            isLockedOperator = callCenter.lockOperator(this);
            if(isLockedOperator) {
                operator = callCenter.getOperator(this); // метод который выкидывает CallCenterException
                logger.info("Клиент " + this.getId() + " дозвонился в колл-центр оператору " + operator.getOperatorId());
                //блок который определяет перенаправлять ли клиента к другому оператру, если рандом возвращает true,
                //клиент перенаправляется(вызывается функция getAnotherOperator(). до тех пор пока не будет найден другой оператор.
                needToRedirect = Math.random() < 0.3;
                if (needToRedirect) {
                    callCenter.unlockOperator(this);
                        isRedirect = callCenter.getAnotherOperator(this, operator);
                        if (isRedirect) {
                            operator = callCenter.getOperator(this);
                            logger.info("Клиент " + this.getId() + " перенаправлен оператору " + operator.getOperatorId());
                        }

                }
            }else {
                logger.info("Клиент " + this.getId() + " не смог довзониться в колл-центр.");
            }
        }finally {
            if(isLockedOperator){
                callCenter.unlockOperator(this);
                logger.info("Клиент " + this.getId() + " был обслужен оператором " + operator.getOperatorId());
            }
        }
    }
}


