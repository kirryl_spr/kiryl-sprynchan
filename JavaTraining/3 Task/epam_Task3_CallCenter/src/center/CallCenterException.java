package center;

/**
 * Created by Кирилл on 15.04.2015.
 */
public class CallCenterException extends Exception{
    public CallCenterException(String message) {
        super(message);
    }
}
