package center;


import client.Client;
import org.apache.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Кирилл on 15.04.2015.
 */
public class CallCenter {
    private static final Logger logger = Logger.getRootLogger();
    private BlockingQueue<Operator> operatorList;
    private Map<Client, Operator> usedOperators;

    public CallCenter(int amountOfOperators){
        operatorList = new ArrayBlockingQueue<Operator>(amountOfOperators);
        for (int i = 0; i < amountOfOperators; i++) {
            operatorList.add(new Operator(i+1));
        }
        usedOperators = new LinkedHashMap<Client, Operator>();
    }

    public boolean lockOperator(Client client){
        Operator operator;
        try{
            operator = operatorList.take();
            usedOperators.put(client, operator);
        }catch (InterruptedException e){
            return false;
        }
        return true;
    }

    public boolean unlockOperator(Client client){
        Operator operator = usedOperators.get(client);

        try{
            operatorList.put(operator);
            usedOperators.remove(client);
        }catch (InterruptedException e){
            return false;
        }
        return true;
    }

    public Operator getOperator(Client client) throws CallCenterException{
        Operator operator = usedOperators.get(client);
        if(operator == null){
            throw new CallCenterException("Ошибка в получении оператора.");
        }
        return operator;
    }

    public  boolean getAnotherOperator(Client client, Operator operator){
        int operatorIdCheck;
        operatorIdCheck = operator.getOperatorId();
        while(operatorIdCheck == operator.getOperatorId()) {
            try {
                operator = operatorList.take();
                usedOperators.put(client, operator);
            } catch (InterruptedException e) {
                logger.error("Ошибка, InterruptedException");
            }
        }
        return true;
    }
}
