package com.epam.newsportal.dao.implementations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsportal.dao.AuthorDao;
import com.epam.newsportal.domain.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Class <code>AuthorDaoImplTest</code> content list of public Data Base
 * Unit tests, which check right work of Data Access layer class
 * <codes>AuthorDao</code> methods.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsportal.dao.AuthorDao
 * @see com.epam.newsportal.domain.Author
 * @since 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = { "classpath:/SpringTest.xml" })
@DatabaseSetup(value = "/data.before.operation.xml")
public class AuthorDaoImplTest {

	/**
	 * reference on AuthorDao class, to use it all public methods
	 */
	@Autowired
	private AuthorDao authorDao;	

	/**
	 * Data Base Unit test, that verify work create() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long authorId = null;
		String authorName = "Taylor";

		Author author = new Author();
		author.setAuthorName(authorName);

		authorId = authorDao.create(author);
		author.setAuthorId(authorId);
		
		Author createdAuthor = authorDao.read(authorId);
		
		assertEqualsAuthor(author, createdAuthor);
	}

	/**
	 * Data Base Unit test, that verify work read() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Author author = new Author();
		
		Long authorId = 1L;
		author.setAuthorId(authorId);
		String authorName = "Bob";
		author.setAuthorName(authorName);

		Author readedAuthor = new Author();
		readedAuthor = authorDao.read(authorId);

		assertEqualsAuthor(author, readedAuthor);
	}

	/**
	 * Data Base Unit test, that verify work readAll() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		Author author = new Author();
		
		Long authorId = 3L;
		author.setAuthorId(authorId);
		String authorName = "Robby";
		author.setAuthorName(authorName);
		
		int expectedSize = 4;
		
		List<Author> authorList = authorDao.readAll();
		
		Author readedAuthor = authorList.get(2);
		
		assertEquals(authorList.size(), expectedSize);

		assertEqualsAuthor(author, readedAuthor);
	}

	/**
	 * Data Base Unit test, that verify work update() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Long authorId = 1L;
		String authorName = "SuperNewBob";
		
		Author author = new Author();
		author.setAuthorId(authorId);
		author.setAuthorName(authorName);

		authorDao.update(author);
		
		Author updatedAuthor = authorDao.read(authorId);
		
		assertEqualsAuthor(author, updatedAuthor);
	}

	/**
	 * Data Base Unit test, that verify work delete() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long authorId = 1L;
		
		Author author = new Author();
		author = authorDao.read(authorId);
		assertNotNull(author);
		
		authorDao.delete(authorId);
		author = authorDao.read(authorId);
		assertNull(author);
	}

	/**
	 * Private method that is used for comparing two objects
	 * 
	 * @param firstAuthor
	 * @param secondAuthor
	 */
	private void assertEqualsAuthor(Author firstAuthor, Author secondAuthor){
		assertEquals(firstAuthor.getAuthorId(), secondAuthor.getAuthorId());
		assertEquals(firstAuthor.getAuthorName(), secondAuthor.getAuthorName());
	}
}
