package com.epam.newsportal.service.implementation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsportal.dao.AuthorDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;

/**
 * Public class <code>AuthorServiceImplTest</code> content list of public
 * Mockito Unit tests, which check right work of Service layer class
 * <code>AuthorServiceImpl</code> methods.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsportal.service.AuthorService
 * @see com.epam.newsportal.service.implementation.AuthorServiceImpl
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {

	/**
	 * mock object, which will be inject into AuthorServiceImpl class
	 */
	@Mock
	private AuthorDao authorDao;
	
	/**
	 * service layer object, which we will testing
	 */
	@InjectMocks
	private AuthorServiceImpl authorService;
	
	/**
	 * Public Mockito Unit test, that verify work create() method in the
	 * AuthorServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception{
		Long authorId = null;
		
		Author author = new Author();
		when(authorDao.create(author)).thenReturn(Long.valueOf(1L));
		
		authorId = authorService.create(author);
		verify(authorDao, times(1)).create(author);
		
		assertEquals(authorId, Long.valueOf(1L));
	}
	
	/**
	 * Public Mockito Unit test, that verify work read() method in the
	 * AuthorServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception{
		Long authorId = 1L;
		String authorName = "Mock";
		
		Author author = new Author();
		author.setAuthorId(authorId);
		author.setAuthorName(authorName);

		when(authorDao.read(authorId)).thenReturn(author);
		
		Author mockAuthor = authorService.read(authorId);
		verify(authorDao, times(1)).read(authorId);
		
		assertEquals(authorId, mockAuthor.getAuthorId());
		assertEquals(authorName, mockAuthor.getAuthorName());
	}
	
	/**
	 * Public Mockito Unit test, that verify work readAll() method in the
	 * AuthorServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception{
		List<Author> authorList = null;
		
		when(authorDao.readAll()).thenReturn(new ArrayList<Author>());
		
		authorList = authorService.readAll();
		verify(authorDao, times(1)).readAll();
		assertNotNull(authorList);
	}
	
	/**
	 * Public Mockito Unit test, that verify work update() method in the
	 * AuthorServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception{
		Author author = new Author();
		
		authorService.update(author);
		verify(authorDao,times(1)).update(author);
	}
	
	/**
	 * Public Mockito Unit test, that verify work delete() method in the
	 * AuthorServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception{
		Long authorId = 1L;
		
		authorService.delete(authorId);
		verify(authorDao,times(1)).delete(authorId);
	}
	
	/**
	 * Public Mockito Unit test, that verify work searchAuthorByNews() method in the
	 * AuthorServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchAuthorByNews() throws Exception{
		Long authorId = 1L;
		Author author = new Author();
		author.setAuthorId(authorId);
		
		News news = new News();
		
		when(authorDao.searchAuthorByNews(news)).thenReturn(author);
		
		Author mockAuthor = authorService.searchAuthorByNews(news);
		verify(authorDao, times(1)).searchAuthorByNews(news);
		
		assertEquals(authorId, mockAuthor.getAuthorId());
	}
}
