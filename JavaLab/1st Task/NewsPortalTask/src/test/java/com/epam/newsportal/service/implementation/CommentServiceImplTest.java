package com.epam.newsportal.service.implementation;


import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsportal.dao.CommentDao;
import com.epam.newsportal.domain.Comment;
import com.epam.newsportal.domain.News;

/**
 * Public class <code>CommentServiceImplTest</code> content list of public
 * Mockito Unit tests, which check right work of Service layer class
 * <code>CommentServiceImpl</code> methods.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.service.CommentService
 * @see com.epam.newsmanagement.service.implementation.CommentServiceImpl
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {

	/**
	 * mock object, which will be inject into CommentServiceImpl class
	 */
	@Mock
	private CommentDao commentDao;

	/**
	 * service layer object, which we will testing
	 */
	@InjectMocks
	private CommentServiceImpl commentService;
	
	/**
	 * Public Mockito Unit test, that verify work create() method in the
	 * CommentServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception{
		Long commentId = null;
		
		Comment comment = new Comment();
		when(commentDao.create(comment)).thenReturn(Long.valueOf(1L));
		
		commentId = commentService.create(comment);
		verify(commentDao, times(1)).create(comment);
		
		assertEquals(commentId, Long.valueOf(1L));
	}
	
	/**
	 * Public Mockito Unit test, that verify work read() method in the
	 * CommentServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception{
		Long commentId = 1L;
		Long newsId = 2L;
		String commentText = "Mockito comment";
		Date creationDate = Timestamp.valueOf("2015-08-20 01:59:24");
		
		
		Comment comment = new Comment();
		comment.setCommentId(commentId);
		comment.setNewsId(newsId);
		comment.setCommentText(commentText);
		comment.setCreationDate(creationDate);
		
		
		when(commentDao.read(commentId)).thenReturn(comment);
		
		Comment mockComment =new Comment();
		mockComment = commentService.read(commentId);
		verify(commentDao, times(1)).read(commentId);
		
		assertEquals(commentId, mockComment.getCommentId());
		assertEquals(newsId, mockComment.getNewsId());
		assertEquals(commentText, mockComment.getCommentText());
		assertEquals(creationDate, mockComment.getCreationDate());
	}
	
	/**
	 * Public Mockito Unit test, that verify work readAll() method in the
	 * CommentServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception{
		List<Comment> commentList = null;
		
		when(commentDao.readAll()).thenReturn(new ArrayList<Comment>());
		
		commentList = commentService.readAll();
		verify(commentDao, times(1)).readAll();
		
		assertNotNull(commentList);
	}
	
	/**
	 * Public Mockito Unit test, that verify work update() method in the
	 * CommentServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception{
		Long commentId = 1L;
		Comment comment = new Comment();
		comment.setCommentId(commentId);
		
		commentService.update(comment);
		verify(commentDao, times(1)).update(comment);
	}
	
	/**
	 * Public Mockito Unit test, that verify work delete() method in the
	 * CommentServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception{
		Long commentId = 1L;
		
		commentService.delete(commentId);
		verify(commentDao, times(1)).delete(commentId);
	}
	
	/**
	 * Public Mockito Unit test, that verify work searchCommentsByNews() method in the
	 * CommentServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchCommentsByNews() throws Exception{
		List<Comment> commentList = null;
		Long newsId = 1L;
		
		News news = new News();
		news.setNewsId(newsId);
		
		when(commentDao.searchCommentsByNews(news)).thenReturn(new ArrayList<Comment>());
		
		commentList = commentService.searchCommentsByNews(news);
		verify(commentDao, times(1)).searchCommentsByNews(news);
		
		assertNotNull(commentList);
		
	}
}
