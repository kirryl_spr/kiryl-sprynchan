package com.epam.newsportal.dao.implementations;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsportal.dao.NewsDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.SearchCriteria;
import com.epam.newsportal.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

/**
 * Public class <code>NewsDaoImplTest</code> content list of public Data Base
 * Unit tests, which check right work of Data Access layer class
 * <code>NewsDao</code> methods.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsportal.dao.NewsDao
 * @see com.epam.newsportal.domain.News
 * @since 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, 
	TransactionalTestExecutionListener.class,
	DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = { "classpath:/SpringTest.xml" })
@DatabaseSetup(value = "/data.before.operation.xml" )

public class NewsDaoImplTest {

	/**
	 * reference on NewsDao class, to use it all public methods
	 */
	@Autowired
	private NewsDao newsDao;

	/**
	 * Data Base Unit test, that verify work create() method in the NewsDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long newsId = null;
		String shortText = "SH4";
		String fullText = "FL4";
		String title = "T4";
		java.util.Date creationDate = Timestamp.valueOf("2015-03-30 03:45:24");
		java.util.Date modificationDate = Date.valueOf("2015-03-30");

		News news = new News();
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setTitle(title);
		news.setCreationDate(creationDate);
		news.setModificationDate(modificationDate);
		newsId = newsDao.create(news);
		news.setNewsId(newsId);

		News createdNews = newsDao.read(newsId);
		
		assertEqualsNews(news, createdNews);
	}
	
	/**
	 * Data Base Unit test, that verify work read() method in the NewsDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long newsId = 1L;
		String shortText = "SH1";
		String fullText = "FL1";
		String title = "T1";
		java.util.Date creationDate = Timestamp.valueOf("2015-03-30 03:45:24");
		java.util.Date modificationDate = Date.valueOf("2015-03-30");

		News news = new News();
		news.setNewsId(newsId);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setTitle(title);
		news.setCreationDate(creationDate);
		news.setModificationDate(modificationDate);
		
		
		News readedNews = newsDao.read(newsId);

		assertEqualsNews(news, readedNews);
	}
	
	/**
	 * Public Data Base Unit test, that verify work readAll() method in the
	 * NewsDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		Long newsId = 3L;
		String shortText = "SH3";
		String fullText = "FL3";
		String title = "T3";
		java.util.Date creationDate = Timestamp.valueOf("2015-03-30 03:45:24");
		java.util.Date modificationDate = Date.valueOf("2015-05-30");
		
		int excpectedSize = 3;

		News news = new News();
		news.setNewsId(newsId);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setTitle(title);
		news.setCreationDate(creationDate);
		news.setModificationDate(modificationDate);
		
		List<News> newsList = newsDao.readAll();

		News readedNews = newsList.get(2);

		assertEquals(newsList.size(), excpectedSize);
		
		assertEqualsNews(news, readedNews);
	}
	
	/**
	 * Data Base Unit test, that verify work update() method in the NewsDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Long newsId = 1L;
		String shortText = "NEW_SH1";
		String fullText = "NEW_FL1";
		String title = "NEW_T1";
		java.util.Date creationDate = Timestamp.valueOf("2015-03-29 03:45:24");
		java.util.Date modificationDate = Date.valueOf("2015-03-29");

		News mainNews = new News();
		mainNews.setNewsId(newsId);
		mainNews.setShortText(shortText);
		mainNews.setFullText(fullText);
		mainNews.setTitle(title);
		mainNews.setCreationDate(creationDate);
		mainNews.setModificationDate(modificationDate);
		newsDao.update(mainNews);

		News createdNews = newsDao.read(newsId);
		
		assertEqualsNews(createdNews, mainNews);
	}

	/**
	 * Data Base Unit test, that verify work delete() method in the NewsDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long newsId = 1L;

		News news = new News();
		news = newsDao.read(newsId);
		assertNotNull(news);

		newsDao.delete(newsId);
		news = newsDao.read(newsId);
		assertNull(news);
	}
	
	/**
	 * Public Data Base Unit test, that verify work searchNewsByAuthor() method
	 * in the NewsDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchNewsByAuthor() throws Exception {
		Long authorId = 1L;
		Long newsId = 2L;

		Author author = new Author();
		author.setAuthorId(authorId);

		List<News> newsList = null;
		newsList = newsDao.searchNewsByAuthor(author);

		News news = newsList.get(0);
		assertEquals(news.getNewsId(), newsId);
	}
	
	/**
	 * Data Base Unit test, that verify work searchNewsByTag() method in
	 * the NewsDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchNewsByTag() throws Exception {
		Long tagId = 1L;
		Long newsId = 1L;

		Tag tag = new Tag();
		tag.setTagId(tagId);

		List<News> newsList = null;
		newsList = newsDao.searchNewsByTag(tag);

		News news = newsList.get(0);
		assertEquals(news.getNewsId(), newsId);
	}
	
	/**
	 * Public Data Base Unit test, that verify work getSrtedNews()
	 * method in the NewsDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void getSortedNews() throws Exception {
		List<News> newsList = null;

		newsList = newsDao.getSortedNews();
		assertThat(newsList.get(0).getNewsId(), is(3L));
		assertThat(newsList.get(1).getNewsId(), is(2L));
		assertThat(newsList.get(2).getNewsId(), is(1L));
	}
	
	/**
	 * Data Base Unit test, that verify work saveNewsAuthor() method
	 * in the AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	@ExpectedDatabase( value = "classpath:data.after.operation.xml", 
	table = "NEWS_AUTHOR", assertionMode =  DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void saveNewsAuthor() throws Exception{
		Long newsId = 3L;
		Long authorId = 3L;
		
		newsDao.saveNewsAuthor(newsId, authorId);
	}
	
	/**
	 * Data Base Unit test, that checks work saveNewsTag() method in
	 * the TagDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	@ExpectedDatabase( value = "classpath:data.after.operation.xml", 
	table = "NEWS_TAG", assertionMode =  DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void saveNewsTag() throws Exception{
		Long newsId = 3L;
		Long firstTagId = 3L;
		Long secondTagId = 1L;
		
		Tag firstTag = new Tag();
		Tag secondTag = new Tag();
		
	
		firstTag.setTagId(firstTagId);
		secondTag.setTagId(secondTagId);
		
		List<Tag> testTagList = new ArrayList<Tag>();
		testTagList.add(firstTag);
		testTagList.add(secondTag);
		
		Iterator<Tag> it = testTagList.iterator();
		
		while(it.hasNext()){
			Tag obj = it.next();
			newsDao.saveNewsTag(newsId, obj.getTagId());
		}
	}
	
	/**
	 * Puvlic Data Base Unit test, that verify work readAllNews()
	 * method in the NewsDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAllnews() throws Exception{
		Long authorId = 1L;
		Long firstTagId = 1L;
		Long secondTagId = 2L;

		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(firstTagId);
		tagIdList.add(secondTagId);
		
		
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(authorId);
		searchCriteria.setIdTagsList(tagIdList);
		
		Long newsId = 2L;
		List<News> newsList = null;
		newsList = newsDao.readAllNews(searchCriteria);

		News news = newsList.get(0);
		assertEquals(news.getNewsId(), newsId);
	}
	
	/**
	 * Private method that is used for comparing two objects
	 * 
	 * @param firstNews
	 * @param secondNews
	 */
	private void assertEqualsNews(News firstNews, News secondNews){
		assertEquals(firstNews.getNewsId(), secondNews.getNewsId());
		assertEquals(firstNews.getShortText(), secondNews.getShortText());
		assertEquals(firstNews.getFullText(), secondNews.getFullText());
		assertEquals(firstNews.getTitle(), secondNews.getTitle());
		assertEquals(firstNews.getCreationDate(), secondNews.getCreationDate());
		assertEquals(firstNews.getModificationDate(), secondNews.getModificationDate());
	}
}
