package com.epam.newsportal.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsportal.dao.TagDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.DaoException;
import com.epam.newsportal.utils.DataBaseUtil;

/**
 * Public class <code>TagDaoImpl</code> is an element of Data Access layer and
 * working with Tag data base instance. This is a single ton realization of
 * <code>TagDao</code> interface and it performs all operations described in
 * this interface. Realized pattern Singleton.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsportal.dao.TagDao
 * @see com.epam.newsportal.domain.Tag
 * @since 1.0
 */
@Repository
public class TagDaoImpl implements TagDao{

	/**
	 * Object which used for connecting to the database
	 */
	@Autowired
	private DataBaseUtil dataBaseUtil;
	
	/**
	 * SQL query for create tag
	 */
	private static final String SQL_CREATE_QUERY = "INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES(TAG_ID_SEQ.NEXTVAL, ?)";

	/**
	 * SQL query for read tag by id
	 */
	private static final String SQL_READ_QUERY = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID = ?";

	/**
	 * SQL query for readAll tag records
	 */
	private static final String SQL_READ_ALL_QUERY = "SELECT TAG_ID, TAG_NAME FROM TAG ORDER BY TAG_ID";

	/**
	 * SQL query for update tag
	 */
	private static final String SQL_UPDATE_QUERY = "UPDATE TAG SET TAG_NAME = ? WHERE TAG_ID = ?";

	/**
	 * SQL query for delete tag
	 */
	private static final String SQL_DELETE_QUERY = "DELETE FROM TAG WHERE TAG_ID = ?";
	
	/**
	 * SQL query for delete association news with tags
	 */
	private static final String SQL_DELETE_NEWS_TAG_QUERY = "DELETE FROM NEWS_TAG WHERE NEWS_ID =?";

	/**
	 * SQL query for search tag by author
	 */
	private static final String SQL_SEARCH_BY_AUTHOR_QUERY = "SELECT DISTINCT TAG.TAG_ID, TAG.TAG_NAME from TAG inner join NEWS_TAG on TAG.TAG_ID = NEWS_TAG.TAG_ID inner join NEWS_AUTHOR on NEWS_TAG.NEWS_ID = NEWS_AUTHOR.NEWS_ID WHERE NEWS_AUTHOR.AUTHOR_ID = ? ORDER BY TAG_ID";
	
	/**
	 * Override public method used to add record to the table Tag.
	 *
	 * @param tag
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public Long create(Tag tag) throws DaoException {
		String columnTagId = "TAG_ID";
		Long tagId = null;

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		String generatedColumns[] = { columnTagId };
		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE_QUERY,
					generatedColumns);

			preparedStatement.setString(1, tag.getTagName());

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				tagId = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in create 'tag' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
		return tagId;
	}

	/**
	 * Override method used to read record from the table Tag.
	 *
	 * @param tagId
	 *            unique identifier of instance which will be found
	 * @return find Tag instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public Tag read(Long tagId) throws DaoException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Tag tag = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ_QUERY);

			preparedStatement.setLong(1, tagId);

			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				tag = new Tag();
				tag.setTagId(resultSet.getLong(1));
				tag.setTagName(resultSet.getString(2));
			}

			return tag;

		} catch (SQLException e) {
			throw new DaoException("SQL error in read 'tag' operation.");
		}
		 finally {
			 dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to read all records from the table Tag.
	 *
	 * @return list of find Tags instances
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public List<Tag> readAll() throws DaoException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		List<Tag> tagList = null;
		Tag tag = null;

		try {
			connection = dataBaseUtil.getConnection();
			statement = connection.createStatement();

			resultSet = statement.executeQuery(SQL_READ_ALL_QUERY);

			tagList = new ArrayList<Tag>();
			while (resultSet.next()) {
				tag = new Tag();

				tag.setTagId(resultSet.getLong(1));
				tag.setTagName(resultSet.getString(2));

				tagList.add(tag);
			}

			return tagList;

		} catch (SQLException e) {
			throw new DaoException("SQL error in readAll 'tag' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, statement, connection);
		}
	}


	/**
	 * Override method used to update record in the table Tag.
	 *
	 * @param tag
	 *            instance which will be update in the data base
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void update(Tag tag) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_QUERY);

			preparedStatement.setString(1, tag.getTagName());
			preparedStatement.setLong(2, tag.getTagId());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in update 'tag' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}


	/**
	 * Override method used to delete record from the table Tag.
	 *
	 * @param tagId
	 *            unique identifier of instance which will be delete
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void delete(Long tagId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_QUERY);

			preparedStatement.setLong(1, tagId);

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in delete 'tag' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to search list of Tags record from the table Tag by
	 * Author instance.
	 *
	 * @param author
	 *            element of Author instance to search for Tag list
	 * @return list of Tag which was find by the Author instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public List<Tag> searchTagsByAuthor(Author author) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Tag tag = null;
		List<Tag> tagList = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_SEARCH_BY_AUTHOR_QUERY);

			preparedStatement.setLong(1, author.getAuthorId());

			resultSet = preparedStatement.executeQuery();

			tagList = new ArrayList<Tag>();
			while (resultSet.next()) {
				tag = new Tag();

				tag.setTagId(resultSet.getLong(1));
				tag.setTagName(resultSet.getString(2));

				tagList.add(tag);
			}

			return tagList;

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in search 'tag' by author operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}
	
	/**
	 * Override method used to delete data from table NEWS_TAG 
	 * when news is deleting
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsportal.domain.News
	 */
	public void deleteNewsTag(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_TAG_QUERY);

			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in delete 'news_tag' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
		
	}

}
