package com.epam.newsportal.service;

import java.util.List;

import com.epam.newsportal.domain.PostedNews;
import com.epam.newsportal.exception.ServiceException;

/**
 * General interface <code>NewsManagementService</code> describing transactional
 * operations for which, you must use a few simple Service layer interfaces.
 *
 *
 * The main interface in all Service layer interfaces. This interface
 * <code>NewsManagementService</code> describing transactional operations for
 * which, you must use a few simple Service layer interfaces implementations.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @since 1.0
 */
public interface NewsManagementService {

	/**
	 * Save news with list of binding tags and author in one step. This is a
	 * transactional operation.
	 *
	 * @param postedNews
	 *            PostedNews instance for saving in database
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public void saveNews(PostedNews postedNews) throws ServiceException;
	
	/**
	 * Delete news with list of binding tags, author and comments in one step. This is a
	 * transactional operation.
	 *
	 * @param List of long instances which are ID of news
	 *            list of Tag instances for saving in database
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public void deleteNews(List<Long> newsIdList) throws ServiceException;
}
