package com.epam.newsportal.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Public class <code>SearchCriteria</code> is one of Entities classes. Is used for
 * searching news by users` criteria. Can be an
 * element <code>HashTable</code>
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @since 1.0
 */
public class SearchCriteria implements Serializable{
	
	private static final long serialVersionUID = 1L;

	/**
	 * Author instance for current search criteria object
	 */
	private Long authorId;
	
	/**
	 * List of tag instances for current search criteria object
	 */
	private List<Long> idTagsList;

	/**
	 * @return the authorId
	 */
	public Long getAuthorId() {
		return authorId;
	}

	/**
	 * @param authorId the authorId to set
	 */
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	/**
	 * @return the idTagsList
	 */
	public List<Long> getIdTagsList() {
		return idTagsList;
	}

	/**
	 * @param idTagsList the idTagsList to set
	 */
	public void setIdTagsList(List<Long> idTagsList) {
		this.idTagsList = idTagsList;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result
				+ ((idTagsList == null) ? 0 : idTagsList.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchCriteria other = (SearchCriteria) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (idTagsList == null) {
			if (other.idTagsList != null)
				return false;
		} else if (!idTagsList.equals(other.idTagsList))
			return false;
		return true;
	}

	
	@Override
	public String toString() {
		return "SearchCriteria [authorId=" + authorId + ", idTagsList="
				+ idTagsList + "]";
	}
	
	
}
