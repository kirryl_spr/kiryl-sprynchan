package com.epam.newsportal.domain;

import java.io.Serializable;
import java.util.List;

public class PostedNews implements Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * Current news instance
	 */
	private News news;
	
	/**
	 * Author instance for current news
	 */
	private Author author;
	
	/**
	 * List of tag instances for current news
	 */
	private List<Tag> tagList;
	
	/**
	 * List of comment instances for current news
	 */
	private List<Comment> commentList;
	
	/**
	 * Constructor without parameters for creating an object.
	 */
	public PostedNews(){
	}
	
	/**
	 * Constructor with parameters for creating an object.
	 */
	public PostedNews(News news, Author author, List<Tag> tagList, List<Comment> commentList){
		this.news = news;
		this.author = author;
		this.tagList = tagList;
		this.commentList = commentList;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTagList() {
		return tagList;
	}

	public void setTagList(List<Tag> tagList) {
		this.tagList = tagList;
	}

	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((commentList == null) ? 0 : commentList.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + ((tagList == null) ? 0 : tagList.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostedNews other = (PostedNews) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (commentList == null) {
			if (other.commentList != null)
				return false;
		} else if (!commentList.equals(other.commentList))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (tagList == null) {
			if (other.tagList != null)
				return false;
		} else if (!tagList.equals(other.tagList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PostedNews [news=" + news + ", author=" + author + ", tagList="
				+ tagList + ", commentList=" + commentList + "]";
	}
	
	
	
}
