package com.epam.newsportal.service;

import java.util.List;

import com.epam.newsportal.domain.Comment;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.exception.ServiceException;


/**
 * The second level of the hierarchy Service layer interfaces. This interface
 * <code>CommentService</code> describes the behavior of a particular service
 * layer which working with instance of <code>Comment</code> using Data Access
 * layer implementations of interface <code>CommentDao</code>.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsportal.service.GenericService
 * @see com.epam.newsportal.dao.CommentDao
 * @see com.epam.newsportal.domain.Comment
 * @since 1.0
 */
public interface CommentService extends GenericService<Comment>{

	/**
	 * Defining search operation from Comment table in data base by sent News
	 * instance, using the searchCommentsByNews() method in CommentDao interface.
	 *
	 * @param news
	 *            element of News instance to search for Comment list
	 * @return list of Comment which was find by the News instance
	 * @@throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public List<Comment> searchCommentsByNews(News news) throws ServiceException;
	
	/**
	 * Defining delete operation from Comments table in data base by sent Long
	 * instance, using the deleteAllCommentsForNews(Long newsId) method in CommentDao interface.
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @@throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public void deleteAllCommentsForNews(Long newsId) throws ServiceException;
}
