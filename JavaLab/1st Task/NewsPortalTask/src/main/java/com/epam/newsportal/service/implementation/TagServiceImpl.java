package com.epam.newsportal.service.implementation;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsportal.dao.TagDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.DaoException;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.TagService;

/**
 * Public class <code>TagServiceImpl</code> is an element of Service layer and
 * working with Tag data base instance. This is a realization of
 * <code>TagService</code> interface and it performs all operations described in
 * this interface.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsportal.service.TagService
 * @see com.epam.newsportal.domain.Tag
 * @since 1.0
 */
@Service
public class TagServiceImpl implements TagService {

	/**
	 * object to conduct logging
	 */
	private static final Logger logger = Logger.getLogger(TagServiceImpl.class.getName());
	
	/**
	 * link to the interface part, providing access to basic operations
	 */
	@Autowired
	private TagDao tagDao;

	/**
	 * Override method used to add record to the table Tag, which calls create()
	 * method from TagDao interface.
	 *
	 * @param tag
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */
	public Long create(Tag tag) throws ServiceException {
		Long tagId = null;

		isObjectNull(tag, "Tag is empty.");
		
		try {
			tagId = tagDao.create(tag);
			return tagId;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot create tag.", e);
		}
	}

	/**
	 * Override method used to add record to the table Tag, which calls create()
	 * method from TagDao interface.
	 *
	 * @param tag
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */
	public Tag read(Long tagID) throws ServiceException {
		Tag tag = null;

		try {
			tag = tagDao.read(tagID);
			return tag;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot read tag.", e);
		}
	}

	/**
	 * Override method used to read all records from the table Tag, which calls
	 * readAll() method from TagDao interface.
	 *
	 * @return list of find Tags instances
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */
	public List<Tag> readAll() throws ServiceException {
		List<Tag> tagList = null;
		
		try {
			
			tagList = tagDao.readAll();
			return tagList;
			
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot read all tags.", e);
		}

	}

	/**
	 * Override method used to delete record from the table Tag, which calls
	 * update() method from TagDao interface.
	 *
	 * @param tag
	 *            instance which will be update in the data base
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */
	public void update(Tag tag) throws ServiceException {
		
		isObjectNull(tag, "Tag is empty.");

		try {
			tagDao.update(tag);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot update tag.", e);
		}

	}

	/**
	 * Override method used to delete record from the table Tag, which calls
	 * delete() method from TagDao interface.
	 *
	 * @param tagId
	 *            unique identifier of instance which will be delete
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */
	@Transactional
	public void delete(Long tagID) throws ServiceException {
		try {
			
			deleteNewsTag(tagID);
			tagDao.delete(tagID);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot delete tag.", e);
		}
	}

	/**
	 * Override method used to search list of Tags record from the table Tag by
	 * Author instance. which calls searchTagsByAuthor() method from TagDao
	 * interface.
	 *
	 * @param author
	 *            element of Author instance to search for Tag list
	 * @return list of Tag which was find by the Author instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */
	public List<Tag> searchTagsByAuthor(Author author) throws ServiceException {
		List<Tag> tagList = null;

		isObjectNull(author, "Author is empty.");

		try {
			tagList = tagDao.searchTagsByAuthor(author);
			return tagList;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot search tag by author.", e);
		}
		
	}
	
	/**
	 * Override method used to delete operation from NEWS_TAG table in data base by sent Long
	 * instance, using the deleteNewsTag(Long newsId) method in TagDao interface.
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @@throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public void deleteNewsTag(Long newsId) throws ServiceException {
		try {

			tagDao.deleteNewsTag(newsId);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot delete news tags.", e);
		}
	}
	
	/**
	 *Override method for checking whether the object is null.
	 *If it is true throws exception with message
	 *
	 * @param obj Object
	 * @param String message
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public void isObjectNull(Object obj, String message)
			throws ServiceException {
		if (obj == null) {
			logger.error("Object is null");
			throw new ServiceException(message);
		}
	}
}
