package com.epam.newsportal.dao.implementation;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsportal.dao.NewsDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.SearchCriteria;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.DaoException;
import com.epam.newsportal.utils.DataBaseUtil;
import com.epam.newsportal.utils.QueryBuilder;

/**
 * Public class <code>NewsDaoImpl</code> is an element of Data Access layer and
 * working with News data base instance. This is a single ton realization of
 * <code>NewsDao</code> interface and it performs all operations described in
 * this interface. Realized pattern Singleton.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsportal.dao.NewsDao
 * @see com.epam.newsportal.domain.News
 * @since 1.0
 */
@Repository
public class NewsDaoImpl implements NewsDao {

	/**
	 * Object which used for connecting to the database
	 */
	@Autowired
	private DataBaseUtil dataBaseUtil;

	/**
	 * SQL query for create news
	 */
	private static final String SQL_CREATE_QUERY = "INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) "
			+ "VALUES (NEWS_ID_SEQ.NEXTVAL, ?, ?, ?,  ?,  ?)";

	/**
	 * SQL query for read news by id
	 */
	private static final String SQL_READ_QUERY = "SELECT NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE FROM NEWS "
			+ "WHERE NEWS_ID = ?";

	/**
	 * SQL query for readAll news records
	 */
	private static final String SQL_READ_ALL_QUERY = "SELECT NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE FROM NEWS "
			+ "ORDER BY NEWS_ID";

	/**
	 * SQL query for update news
	 */
	private static final String SQL_UPDATE_QUERY = "UPDATE NEWS SET SHORT_TEXT = ?, FULL_TEXT = ?, TITLE = ?, CREATION_DATE = ?, MODIFICATION_DATE = ? "
			+ "WHERE NEWS_ID = ?";

	/**
	 * SQL query for delete news
	 */
	private static final String SQL_DELETE_QUERY = "DELETE FROM NEWS WHERE NEWS_ID =?";
	
	
	/**
	 * SQL query for search news by author
	 */
	private static final String SQL_SEARCH_BY_AUTHOR_QUERY = "SELECT DISTINCT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS "
			+ "JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID "
			+ "JOIN (SELECT NEWS_ID AS N_ID FROM COMMENTS GROUP BY NEWS_ID"
			+ " ORDER BY COUNT(*) DESC) ON NEWS.NEWS_ID = N_ID WHERE (AUTHOR_ID = ?) ORDER BY MODIFICATION_DATE";

	/**
	 * SQL query for search news by tag
	 */
	private static final String SQL_SEARCH_BY_TAG_QUERY = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, "
			+ "NEWS.MODIFICATION_DATE FROM NEWS INNER JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID WHERE NEWS_TAG.TAG_ID = ?";

	/**
	 * SQL query for sort news by comments count
	 */
	private static final String SQL_GET_SORTED_NEWS_QUERY = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS "
			+ "LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY (NEWS.NEWS_ID, NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE) "
			+ "ORDER BY COUNT(COMMENTS.COMMENT_ID)DESC, NEWS.MODIFICATION_DATE DESC";

	/**
	 * SQL query for saving news with author
	 */
	private static final String SQL_SAVE_NEWS_AUTHOR_QUERY = "INSERT INTO NEWS_AUTHOR (NEWS_AUTHOR.NEWS_ID, NEWS_AUTHOR.AUTHOR_ID) VALUES(?, ?)";

	/**
	 * SQL query for saving news with tag
	 */
	private static final String SQL_SAVE_NEWS_TAG_QUERY = "INSERT INTO NEWS_TAG (NEWS_TAG.NEWS_ID, NEWS_TAG.TAG_ID) VALUES(?, ?)";


	/**
	 * Override method used to add record to the table News.
	 *
	 * @param news
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public Long create(News news) throws DaoException {
		String columnNewsId = "NEWS_ID";
		Long newsId = null;

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		String generatedColumns[] = { columnNewsId };
		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE_QUERY,
					generatedColumns);

			preparedStatement.setString(1, news.getShortText());
			preparedStatement.setString(2, news.getFullText());
			preparedStatement.setString(3, news.getTitle());
			preparedStatement.setTimestamp(4, new Timestamp(news
					.getCreationDate().getTime()));
			preparedStatement.setDate(5, new Date(news.getModificationDate()
					.getTime()));

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				newsId = resultSet.getLong(1);
			}

		} catch (SQLException e) {
			throw new DaoException("SQL error in create 'news' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
		return newsId;

	}

	/**
	 * Override method used to read record from the table News.
	 *
	 * @param newsId
	 *            unique identifier of instance which will be found
	 * @return find News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public News read(Long newsId) throws DaoException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		News news = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ_QUERY);

			preparedStatement.setLong(1, newsId);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				news = new News();
				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));
			}

			return news;

		} catch (SQLException e) {
			throw new DaoException("SQL error in read 'news' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to read all records from the table News.
	 *
	 * @return list of find News instances
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public List<News> readAll() throws DaoException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		List<News> newsList = null;
		News news = null;

		try {
			connection = dataBaseUtil.getConnection();
			statement = connection.createStatement();

			resultSet = statement.executeQuery(SQL_READ_ALL_QUERY);

			newsList = new ArrayList<News>();
			while (resultSet.next()) {
				news = new News();

				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));

				newsList.add(news);
			}

			return newsList;

		} catch (SQLException e) {
			throw new DaoException("SQL error in readAll 'news' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, statement, connection);
		}
	}

	/**
	 * Override method used to update record in the table News.
	 *
	 * @param news
	 *            instance which will be update in the data base
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void update(News news) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_QUERY);

			preparedStatement.setString(1, news.getShortText());
			preparedStatement.setString(2, news.getFullText());
			preparedStatement.setString(3, news.getTitle());
			preparedStatement.setTimestamp(4, new Timestamp(news
					.getCreationDate().getTime()));
			preparedStatement.setDate(5, new Date(news.getModificationDate()
					.getTime()));
			preparedStatement.setLong(6, news.getNewsId());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in update 'news' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to delete record from the table News.
	 *
	 * @param newsId
	 *            unique identifier of instance which will be delete
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void delete(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_QUERY);

			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in delete 'news' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to get sorted, by comments count and modification date,
	 *  list of records from the table News.
	 *
	 * @return sorted list of News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public List<News> getSortedNews() throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		List<News> newsList = null;
		News news = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_GET_SORTED_NEWS_QUERY);

			resultSet = preparedStatement.executeQuery();

			newsList = new ArrayList<News>();
			while (resultSet.next()) {
				news = new News();

				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));

				newsList.add(news);
			}

			return newsList;

		} catch (SQLException e) {
			throw new DaoException("SQL error in sort 'news' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to search list of News records from the table News
	 * by Tag instance.
	 *
	 * @param tag
	 *            element of Tag instance to search for news
	 * @return list of News which was found by the Tag instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public List<News> searchNewsByTag(Tag tag) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		List<News> newsList = null;
		News news = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_SEARCH_BY_TAG_QUERY);

			preparedStatement.setLong(1, tag.getTagId());

			resultSet = preparedStatement.executeQuery();

			newsList = new ArrayList<News>();
			while (resultSet.next()) {
				news = new News();

				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));

				newsList.add(news);
			}

			return newsList;

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in search 'news' by tag operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to search list of News records from the table News
	 * by Author instance.
	 *
	 * @param author
	 *            element of Author instance to search for News list
	 * @return list of News which was find by the Author instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public List<News> searchNewsByAuthor(Author author) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		List<News> newsList = null;
		News news = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_SEARCH_BY_AUTHOR_QUERY);

			preparedStatement.setLong(1, author.getAuthorId());

			resultSet = preparedStatement.executeQuery();

			newsList = new ArrayList<News>();
			while (resultSet.next()) {
				news = new News();

				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));

				newsList.add(news);
			}

			return newsList;

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in search 'news' by author operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to save Author instance with instance of News, and
	 * add same record to the NewsAuthor table in data base.
	 *
	 * @param news
	 *            News instance for binding with Author instance
	 * @param author
	 *            Author instance for binding with News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void saveNewsAuthor(Long newsId, Long authorId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_SAVE_NEWS_AUTHOR_QUERY);

			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in saving news-author operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to save list of Tag instances with instance of News,
	 * and add same record to the NewsTag table in data base.
	 *
	 * @param news
	 *            News instance for binding with Tag instance
	 * @param tagList
	 *            List of tag instance for binding with News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void saveNewsTag(Long newsId, Long tagId)
			throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
				connection = dataBaseUtil.getConnection();

				preparedStatement = connection
						.prepareStatement(SQL_SAVE_NEWS_TAG_QUERY);
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tagId);

				preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			throw new DaoException("SQL error in saving news-tag operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}

	}

	/**
	 * Override method used to search list of NEws instances with instance of SearchCriteria,
	 * and add same record to the NewsTag table in data base.
	 *
	 * @param searchCriteria
	 *            element of SearchCriteria instance to search for users` criteria
	 * @return list of News which was found by the SearchCriteria instance
	 * @throws DaoLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsportal.domain.SearchCriteria
	 */
	public List<News> readAllNews(SearchCriteria searchCriteria)
			throws DaoException {
		
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		List<News> newsList = new ArrayList<News>();

		try {
			connection = dataBaseUtil.getConnection();
			statement = connection.createStatement();

			if(searchCriteria.getAuthorId() == null && searchCriteria.getIdTagsList() != null){
				newsList = searchByListOfTags(searchCriteria);
			}
			
			else if(searchCriteria.getAuthorId() != null && searchCriteria.getIdTagsList() == null){
				Author author = new Author();
				author.setAuthorId(searchCriteria.getAuthorId());
				newsList = searchNewsByAuthor(author);
			}
			
			else if (searchCriteria.getAuthorId() == null && searchCriteria.getIdTagsList() == null){
				newsList = getSortedNews();
			}
			
			else {
				newsList = searchByListOfTagsAndAuthor(searchCriteria);
			}
			
			return newsList;
		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in searching news with Search criteria.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, statement, connection);
		}
	}
	
	

	/**
	 * Private method that search news by list of tags, when search criteria contains only 
	 * list of tags.
	 *
	 * @param searchCriteria
	 *            element of SearchCriteria instance to search for users` criteria
	 * @return list of News which was found by the SearchCriteria instance
	 * @throws DaoLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsportal.domain.SearchCriteria
	 */
	private List<News> searchByListOfTags(SearchCriteria searchCriteria) throws DaoException{

		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		
		List<News> newsList = new ArrayList<News>();
		try {
			connection = dataBaseUtil.getConnection();
			statement = connection.createStatement();

			resultSet =statement.executeQuery(QueryBuilder.buildQueryForTagList(searchCriteria.getIdTagsList()));

				while (resultSet.next()) {
					News news = new News();

					news.setNewsId(resultSet.getLong(1));
					news.setTitle(resultSet.getString(2));
					news.setShortText(resultSet.getString(3));
					news.setFullText(resultSet.getString(4));
					news.setCreationDate(resultSet.getTimestamp(5));
					news.setModificationDate(resultSet.getDate(6));

					newsList.add(news);
				}
			return newsList;
		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in searching news with Search criteria.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, statement, connection);
		}
	}
	
	/**
	 * Private method that search news by list of tags, when search criteria contains  
	 * list of tags and author instance.
	 *
	 * @param searchCriteria
	 *            element of SearchCriteria instance to search for users` criteria
	 * @return list of News which was found by the SearchCriteria instance
	 * @throws DaoLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsportal.domain.SearchCriteria
	 */
	private List<News> searchByListOfTagsAndAuthor(SearchCriteria searchCriteria) throws DaoException{
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		
		List<News> newsList = new ArrayList<News>();
		try {
			connection = dataBaseUtil.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(QueryBuilder.buildQueryForTagsAndAuthor(searchCriteria.getAuthorId(),
						searchCriteria.getIdTagsList()));

				while (resultSet.next()) {
					News news = new News();

					news.setNewsId(resultSet.getLong(1));
					news.setTitle(resultSet.getString(2));
					news.setShortText(resultSet.getString(3));
					news.setFullText(resultSet.getString(4));
					news.setCreationDate(resultSet.getTimestamp(5));
					news.setModificationDate(resultSet.getDate(6));

					newsList.add(news);
				}

			return newsList;
		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in searching news with Search criteria.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, statement, connection);
		}
	}
	
}
