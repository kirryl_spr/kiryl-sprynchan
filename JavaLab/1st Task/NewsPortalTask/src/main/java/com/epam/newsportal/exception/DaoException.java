package com.epam.newsportal.exception;

/**
 * Public class <code>DaoException</code> user-define exception class and can be
 * used when any public methods in the Data Access layer throw up SQL-errors.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @since 1.0
 */
public class DaoException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructs an {@code DaoException} with the specified
	 * detail message and error object that throw up when exception generate.
	 *
	 * @param message
	 *            some string which describe exception cause
	 * @param cause
	 *            exception object that throw up
	 */
	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs an {@code DaoException} with the specified
	 * detail message.
	 *
	 * @param message
	 *            some string which describe exception cause
	 */
	public DaoException(String message) {
		super(message);
	}

	/**
	 * Constructs an {@code DaoLayerTechnicalException} with the specified
	 * object exception message.
	 *
	 * @param cause
	 *            exception object that throw up
	 */
	public DaoException(Exception cause) {
		super(cause);
	}
}
