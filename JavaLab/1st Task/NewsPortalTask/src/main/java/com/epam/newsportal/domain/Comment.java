package com.epam.newsportal.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Public class <code>Comment</code> is one of Entities classes. Its content is
 * fully consistent with Table Comment in data base, which we use for. The main
 * role is to store associated with the table information(data). Can be an
 * element <code>HashTable</code>
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @since 1.0
 */
public class Comment implements Serializable{
	
	private static final long serialVersionUID = 1L;

	/**
	 * unique identifier of instance
	 */
	private Long commentId;
	
	/**
	 * unique identifier of News with which comment bind for
	 */
	private Long newsId;
	
	/**
	 * simple comment text
	 */
	private String commentText;
	
	/**
	 * date of comment creation
	 */
	private Date creationDate;
	
	/**
	 * Constructor without parameters for creating an object.
	 */
	public Comment(){
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(getClass() == obj.getClass()))
			return false;

		Comment comment = (Comment) obj;
		if (commentId != null ? !commentId.equals(comment.commentId)
				: comment.commentId != null)
			return false;
		if (commentText != null ? !commentText.equals(comment.commentText)
				: comment.commentText != null)
			return false;
		if (creationDate != null ? !creationDate.equals(comment.creationDate)
				: comment.creationDate != null)
			return false;
		if (newsId != null ? !newsId.equals(comment.newsId)
				: comment.newsId != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = (commentId != null ? commentId.hashCode() : 0);
		result = 31 * result
				+ (commentText != null ? commentText.hashCode() : 0);
		result = 31 * result
				+ (creationDate != null ? creationDate.hashCode() : 0);
		result = 31 * result + newsId.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return "Comment{" + "commentID=" + commentId + ", commentText='"
				+ commentText + '\'' + ", creationDate=" + creationDate
				+ ", newsId=" + newsId + '}';
	}
	
}
