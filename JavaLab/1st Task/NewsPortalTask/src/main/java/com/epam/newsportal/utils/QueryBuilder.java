package com.epam.newsportal.utils;

import java.util.List;

/**
 * Public class which is used for constructing SQL-queries.
 * 
 * @author Kiryl_Sprynchan
 * @version 1.0
 * @since 1.0
 */
public class QueryBuilder {
	
	/**
	 * SQL query for searching news with search criteria object, that contains
	 * only list of tags
	 */
	private static final String SQL_SEARCH_BY_SEARCH_CRITERIA = "SELECT DISTINCT NEWS.NEWS_ID,"
			+ " NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, "
			+ "NEWS.MODIFICATION_DATE FROM NEWS";

	/**
	 * Public static method that is used for constructing SQL-queries, 
	 * when news are searched by list of tags only. 
	 * @param tagList
	 * @return String which is the SQL query
	 */
	public static String buildQueryForTagList(List<Long> tagList) {
		StringBuilder scriptBuilder = new StringBuilder();

		scriptBuilder.append(SQL_SEARCH_BY_SEARCH_CRITERIA
				+ " JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID ");

		scriptBuilder
				.append(" JOIN (SELECT NEWS_ID AS N_ID FROM COMMENTS "
						+ "GROUP BY NEWS_ID ORDER BY COUNT(*) DESC) ON NEWS.NEWS_ID = N_ID ");

		scriptBuilder.append("WHERE ");

		for (Long tagId : tagList) {
			scriptBuilder.append("NEWS_TAG.TAG_ID=" + tagId + " OR ");
		}

		scriptBuilder
				.delete(scriptBuilder.length() - 3, scriptBuilder.length());

		scriptBuilder.append(" ORDER BY MODIFICATION_DATE");

		return new String(scriptBuilder);
	}

	/**
	 * Public static method that is used for constructing SQL-queries, 
	 * when news are searched by list of tags and Author. 
	 * @param tagList
	 * @param authorId
	 * @return String which is the SQL query
	 */
	public static String buildQueryForTagsAndAuthor(Long authorId,
			List<Long> tagList) {
		StringBuilder scriptBuilder = new StringBuilder();

		scriptBuilder.append(SQL_SEARCH_BY_SEARCH_CRITERIA
				+ " JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = "
				+ "NEWS_AUTHOR.NEWS_ID");

		scriptBuilder.append(" JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID ");

		scriptBuilder
				.append(" JOIN (SELECT NEWS_ID AS N_ID FROM COMMENTS "
						+ "GROUP BY NEWS_ID ORDER BY COUNT(*) DESC) ON NEWS.NEWS_ID = N_ID ");

		scriptBuilder.append("WHERE (AUTHOR_ID = " + authorId + ")" + " OR (");

		for (Long tagId : tagList) {
			scriptBuilder.append("NEWS_TAG.TAG_ID=" + tagId + " OR ");
		}

		scriptBuilder
				.delete(scriptBuilder.length() - 3, scriptBuilder.length());
		scriptBuilder.append(")");

		scriptBuilder.append("ORDER BY MODIFICATION_DATE");
		
		return new String(scriptBuilder);
	}
}
