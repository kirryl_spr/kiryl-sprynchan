package com.epam.newsportal.service.implementation;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsportal.domain.PostedNews;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.AuthorService;
import com.epam.newsportal.service.CommentService;
import com.epam.newsportal.service.NewsManagementService;
import com.epam.newsportal.service.NewsService;
import com.epam.newsportal.service.TagService;

/**
 * Public class <code>NewsManagementServiceImpl<code/> is an
 * implementation of interface <code>NewsManagementService</code> and overrides
 * all methods describing transactional operations for which, you must use a few
 * simple Service layer interfaces.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsportal.service.NewsManagementService
 * @since 1.0
 */
@Service
public class NewsManagementServiceImpl implements NewsManagementService{
	
	/**
	 * author instance for using all service methods
	 */
	@Autowired
	private AuthorService authorService;
	
	/**
	 * comment instance for using all service methods
	 */
	@Autowired
	private CommentService commentService;
	
	/**
	 * news instance for using all service methods
	 */
	@Autowired
	private NewsService newsService;

	/**
	 * news instance for using all service methods
	 */
	@Autowired
	private TagService tagService;
	
	/**
	 * Override method used to save news with list of binding tags and author in one step. 
	 * This is a transactional operation.
	 *
	 * @param postedNews
	 *            PostedNews instance for saving in database
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	@Transactional
	public void saveNews(PostedNews postedNews) throws ServiceException {
		newsService.create(postedNews.getNews());
		
		newsService.saveNewsAuthor(postedNews.getNews(), postedNews.getAuthor());
		newsService.saveNewsTagList(postedNews.getNews(), postedNews.getTagList());
	}
	
	/**
	 * Override method used to delete news in one step. 
	 * This is a transactional operation.
	 *
	 * @param postedNews
	 *            PostedNews instance for saving in database
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	@Transactional
	public void deleteNews(List<Long> newsIdList) throws ServiceException{
		Iterator<Long> it = newsIdList.iterator();
	
		while(it.hasNext()){
			Long newsId = (Long) it.next();
				
			commentService.deleteAllCommentsForNews(newsId);
			authorService.deleteNewsAuthor(newsId);
			tagService.deleteNewsTag(newsId);
		}
	}
}
