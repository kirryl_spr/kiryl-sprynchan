package com.epam.newsportal.dao;

import java.util.List;

import com.epam.newsportal.domain.Comment;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.exception.DaoException;

/**
 * The second level of the hierarchy Data Access layer interfaces. This
 * interface <code>CommentDao</code> describes the behavior of a particular dao
 * layer which working with instance of <code>Comment</code>.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsportal.dao.GenericDao
 * @see com.epam.newsportal.domain.Comment
 * @since 1.0
 */
public interface CommentDao extends GenericDao<Comment>{
	
	/**
	 * Search list of Comments instance from Comment table in data base by sent
	 * News instance.
	 *
	 * @param news
	 *            element of News instance to search for Comments list
	 * @return list of Comments which was find by the news instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsportal.domain.News
	 * @see com.epam.newsportal.domain.Comment
	 */
	public List<Comment> searchCommentsByNews(News news) throws DaoException;

	/**
	 * Delete data from table Comments when news is deleting
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsportal.domain.News
	 */
	public void deleteAllCommentsForNews(Long newsId) throws DaoException;
	
}
