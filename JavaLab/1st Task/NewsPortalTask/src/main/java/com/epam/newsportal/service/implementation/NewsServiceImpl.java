package com.epam.newsportal.service.implementation;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsportal.dao.NewsDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.SearchCriteria;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.DaoException;
import com.epam.newsportal.exception.ServiceException;
import com.epam.newsportal.service.NewsService;

/**
 * Public class <code>NewsServiceImpl</code> is an element of Service layer and
 * working with News data base instance. This is a realization of
 * <code>NewsService</code> interface and it performs all operations described
 * in this interface.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsportal.service.NewsService
 * @see com.epam.newsportal.domain.News
 * @since 1.0
 */
@Service
public class NewsServiceImpl implements NewsService {

	/**
	 * object to conduct logging
	 */
	private static final Logger logger = Logger.getLogger(NewsServiceImpl.class
			.getName());

	/**
	 * link to the interface part, providing access to basic operations
	 */
	@Autowired
	private NewsDao newsDao;

	/**
	 * Override public method used to add record to the table News, which calls
	 * create() method from NewsDao interface.
	 *
	 * @param news
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 */
	public Long create(News news) throws ServiceException {
		Long newsId = null;

		isObjectNull(news, "News is epmty.");

		try {
			newsId = newsDao.create(news);
			return newsId;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot create news.", e);
		}
	}

	/**
	 * Override public method used to read record from the table News, which
	 * calls read() method from NewsDao interface.
	 *
	 * @param newsId
	 *            unique identifier of instance which will be found
	 * @return find News instance
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 */
	public News read(Long newsId) throws ServiceException {
		News news = null;

		try {
			news = newsDao.read(newsId);
			return news;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot read news.", e);
		}
	}

	/**
	 * Override public method used to read all records from the table News,
	 * which calls readAll() method from NewsDao interface.
	 *
	 * @return list of find News instances
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 */
	public List<News> readAll() throws ServiceException {
		List<News> newsList = null;

		try {
			newsList = newsDao.readAll();
			return newsList;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot read all news.", e);
		}
	}

	/**
	 * Override public method used to update record in the table News, which
	 * calls update() method from NewsDao interface.
	 *
	 * @param news
	 *            instance which will be update in the data base
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 */
	public void update(News news) throws ServiceException {

		isObjectNull(news, "News is epmty.");

		try {
			newsDao.update(news);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot update news.", e);
		}
	}

	/**
	 * Override public method used to delete record from the table News, which
	 * calls delete() method from NewsDao interface.
	 *
	 * @param newsId
	 *            unique identifier of instance which will be delete
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 */
	public void delete(Long newsId) throws ServiceException {
		try {

			newsDao.delete(newsId);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot delete news.", e);
		}

	}

	/**
	 * Override public method used to search list of News records from the table
	 * News by Author instance, which calls searchNewsByAuthor() method from
	 * NewsDao interface.
	 *
	 * @param author
	 *            element of Author instance to search for News list
	 * @return list of News which was find by the Author instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 */
	public List<News> searchNewsByAuthor(Author author) throws ServiceException {
		List<News> newsList = null;

		isObjectNull(author, "Author is empty.");

		try {
			newsList = newsDao.searchNewsByAuthor(author);
			return newsList;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot search news by author.", e);
		}
	}

	/**
	 * Override public method used to search list of News records from the table
	 * News by Tag instance, which calls searchNewsByTag() method from NewsDao
	 * interface.
	 *
	 * @param tag
	 *            element of Tag instance to search for news
	 * @return list of News which was found by the Tag instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 */
	public List<News> searchNewsByTag(Tag tag) throws ServiceException {
		List<News> newsList = null;

		isObjectNull(tag, "Tag is empty.");

		try {
			newsList = newsDao.searchNewsByTag(tag);
			return newsList;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot search news by tag.", e);
		}
	}

	public List<News> getSortedNews() throws ServiceException {
		List<News> newsList = null;

		try {
			newsList = newsDao.getSortedNews();
			return newsList;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot sort news by comments count.", e);
		}
	}

	/**
	 * Override public method used to bind Author instance with instance of
	 * News, and add same record to the NewsAuthor table in data base, calls
	 * createAuthorForNews() method from AuthorDao interface.
	 *
	 * @param news
	 *            News instance for binding with Author instance
	 * @param author
	 *            Author instance for binding with News instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */
	public void saveNewsAuthor(News news, Author author)
			throws ServiceException {

		isObjectNull(news, "News is null.");

		isObjectNull(author, "Author is null.");

		try {
			newsDao.saveNewsAuthor(news.getNewsId(), author.getAuthorId());
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot save news-author.", e);
		}
	}

	/**
	 * Override public method used to bind Tag instance with instance of News,
	 * and add same record to the NewsTag table in data base, which calls
	 * createTagForNews() method from TagDao interface.
	 *
	 * @param news
	 *            News instance for binding with Tag instance
	 * @param tagList
	 *            list<Tag> instance for binding with News instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */
	public void saveNewsTagList(News news, List<Tag> tagList)
			throws ServiceException {

		isObjectNull(news, "News is null.");

		isObjectNull(tagList, "TagList is null.");

		try {

			Iterator<Tag> it = tagList.iterator();

			while (it.hasNext()) {
				Tag obj = (Tag) it.next();
				newsDao.saveNewsTag(news.getNewsId(), obj.getTagId());
			}
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot save news-taglist.", e);
		}

	}

	/**
	 * Override public method used to get list News instances from News table in data base , 
	 * using the getSortedNews() method in NewsDao interface.
	 *
	 * @return list of News which are sorted by number of comments and modification date
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public List<News> readAllNews(SearchCriteria searchCriteria)
			throws ServiceException {

		isObjectNull(searchCriteria, "SearchCriteria is null.");

		List<News> newsList = null;

		try {
			newsList = newsDao.readAllNews(searchCriteria);
			return newsList;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException(
					"Error in reading all news by searchCriteria.", e);
		}
	}

	/**
	 *Override method for checking whether the object is null.
	 *If it is true throws exception with message
	 *
	 * @param obj Object
	 * @param String message
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public void isObjectNull(Object obj, String message)
			throws ServiceException {
		if (obj == null) {
			logger.error("Object is null");
			throw new ServiceException(message);
		}
	}

}
