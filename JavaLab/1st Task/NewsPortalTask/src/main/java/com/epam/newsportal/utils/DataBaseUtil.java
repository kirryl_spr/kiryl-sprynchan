package com.epam.newsportal.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * Public class <code>DataBaseUtil</code>. Used for getting connections to
 * database, and controlling transaction using one connection
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see org.springframework.jdbc.datasource.DataSourceUtils;
 * @since 1.0
 */
public class DataBaseUtil {
	
	/**
	 * object to conduct logging
	 */
	private Logger logger = Logger.getLogger(DataBaseUtil.class.getName());

	/**
	 * Object for getting connection with the help of Datasource
	 */
	private DataSource dataSource;
	
	/**
	 * Constructor
	 * @param dataSource
	 */
	public DataBaseUtil(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * provide {@code Connection} instance from pool using
	 * {@code DataSourceUtils} class.
	 * 
	 * @return {@code Connection} instance from pool.
	 * @throws SQLException
	 */
	public synchronized Connection getConnection() throws SQLException {
		return DataSourceUtils.getConnection(dataSource);
	}
	/**
	 * Method that execute operation of closing all resources, after using.
	 *
	 * @param resultSet
	 *            ResultSet object to close after using
	 * @param statement
	 *            Statement object to close after using
	 * @param connection
	 *            Connection object to close after using
	 * @throws DaoLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void closeResources(ResultSet resultSet, Statement statement,
			Connection connection) {
		try {
			if (resultSet != null && !resultSet.isClosed()) {
				resultSet.close();
			}

			if (statement != null && !statement.isClosed()) {
				statement.close();
			}

			if (connection != null && !connection.isClosed()) {
				DataSourceUtils.doReleaseConnection(connection, dataSource);
			}
		} catch (SQLException ex) {
			logger.error(ex.getMessage());
		}
	}
}
