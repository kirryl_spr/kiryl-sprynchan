package com.epam.newsportal.service;

import java.util.List;

import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.ServiceException;

/**
 * The second level of the hierarchy Service layer interfaces. This interface
 * <code>TagService</code> describes the behavior of a particular service layer
 * which working with instance of <code>Tag</code> using Data Access layer
 * implementations of interface <code>TagDao</code>.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsportal.service.GenericService
 * @see com.epam.newsportal.dao.TagDao
 * @see com.epam.newsportal.domain.Tag
 * @since 1.0
 */
public interface TagService extends GenericService<Tag>{
	
	/**
	 * Defining search operation from Tag table in data base by sent Author
	 * instance, using the searchTagByAuthor() method in TagDao interface.
	 *
	 * @param author
	 *            element of Author instance to search for Tag list
	 * @return list of Tag which was find by the Author instance
	 * @@throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public List<Tag> searchTagsByAuthor(Author author) throws ServiceException;
	
	/**
	 * Defining delete operation from NEWS_TAG table in data base by sent Long
	 * instance, using the deleteNewsTag(Long newsId) method in TagDao interface.
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @@throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public void deleteNewsTag(Long newsId) throws ServiceException;
}
