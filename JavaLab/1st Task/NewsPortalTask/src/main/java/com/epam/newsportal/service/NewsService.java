package com.epam.newsportal.service;

import java.util.List;

import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.SearchCriteria;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.ServiceException;

/**
 * The second level of the hierarchy Service layer interfaces. This interface
 * <code>NewsService</code> describes the behavior of a particular service layer
 * which working with instance of <code>News</code> using Data Access layer
 * implementations of interface <code>NewsDao</code>.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsportal.service.GenericService
 * @see com.epam.newsportal.dao.NewsDao
 * @see com.epam.newsportal.domain.News
 * @since 1.0
 */
public interface NewsService extends GenericService<News>{
	
	/**
	 * Search list of News instances from News table in data base by sent Author
	 * instance, using the searchNewsByAuthor() method in NewsDao interface.
	 *
	 * @param author
	 *            element of Author instance to search for News list
	 * @return list of News which was find by the Author instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public List<News> searchNewsByAuthor(Author author) throws ServiceException;
	
	/**
	 * Search list News instances from News table in data base by sent Tag
	 * instance, using the searchNewsByTag() method in NewsDao interface.
	 *
	 * @param tag
	 *            element of Tag instance to search for news
	 * @return list of News which was found by the Tag instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public List<News> searchNewsByTag(Tag tag) throws ServiceException;
	
	/**
	 * Get list News instances from News table in data base , 
	 * using the getSortedNews() method in NewsDao interface.
	 *
	 * @return list of News which are sorted by number of comments and modification date
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public List<News> getSortedNews() throws ServiceException;
	
	/**
	 * Binding the News instance with the Author instance in the table
	 * NewsAuthor in data base, using the bindingNewsAuthor() method in NewsDao
	 * interface.
	 *
	 * @param news
	 *            News instance for binding with Author instance
	 * @param author
	 *            Author instance for binding with News instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public void saveNewsAuthor(News news, Author author) throws ServiceException;
	
	/**
	 * Binding the News instance with the list of Tag instances in the table
	 * NewsTag in data base, using the createTagForNews() method in NewsDao
	 * interface.
	 *
	 * @param news
	 *            News instance for binding with Tag instance
	 * @param tag
	 *            Tag instance for binding with News instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public void saveNewsTagList(News news, List<Tag> tagList) throws ServiceException;
	
	/**
	 * Get list News instances from News table in data base by sent SearchCriteria instance, 
	 * using the readAllNews(SearchCriteria searchCriteria) method in NewsDao interface.
	 *
	 * @return list of News which are sorted by number of comments and modification date
	 * @param searchCriteria
	 *            SearchCriteria instance for binding with other instances
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsportal.exception.DaoException
	 */
	public List<News> readAllNews(SearchCriteria searchCriteria) throws ServiceException;
	
}
