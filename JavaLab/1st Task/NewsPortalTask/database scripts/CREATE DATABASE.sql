--------------------------------------------------------
--  DDL for deleting Tables if exists
--------------------------------------------------------
DROP TABLE "COMMENTS";
DROP TABLE "NEWS_AUTHOR";
DROP TABLE "NEWS_TAG";
DROP TABLE "ROLES";
DROP TABLE "TAG";
DROP TABLE "USER";
DROP TABLE "AUTHOR";
DROP TABLE "NEWS";
DROP SEQUENCE "AUTHOR_ID_SEQ";
DROP SEQUENCE "COMMENT_ID_SEQ";
DROP SEQUENCE "NEWS_ID_SEQ";
DROP SEQUENCE "TAG_ID_SEQ";
DROP SEQUENCE "USER_ID_SEQ";

--------------------------------------------------------
--  DDL for Table NEWS
--------------------------------------------------------

CREATE TABLE "NEWS"
(
  "NEWS_ID"           NUMBER(20, 0),
  "SHORT_TEXT"        NVARCHAR2(100)  NOT NULL,
  "FULL_TEXT"         NVARCHAR2(2000) NOT NULL,
  "TITLE"             NVARCHAR2(30)   NOT NULL,
  "CREATION_DATE"     TIMESTAMP(6)    NOT NULL,
  "MODIFICATION_DATE" DATE            NOT NULL,
  CONSTRAINT NEWS_PK PRIMARY KEY ("NEWS_ID")

);
--------------------------------------------------------
--  DDL for Table AUTHOR
--------------------------------------------------------

CREATE TABLE "AUTHOR"
(
  "AUTHOR_ID" NUMBER(20, 0),
  "AUTHOR_NAME"      NVARCHAR2(30) NOT NULL,
  CONSTRAINT AUTHOR_PK PRIMARY KEY ("AUTHOR_ID")
);
--------------------------------------------------------
--  DDL for Table NEWS_AUTHOR
--------------------------------------------------------

CREATE TABLE "NEWS_AUTHOR"
(
  "NEWS_ID"   NUMBER(20, 0) NOT NULL,
  "AUTHOR_ID" NUMBER(20, 0) NOT NULL
);
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

CREATE TABLE "COMMENTS"
(
  "COMMENT_ID"    NUMBER(20, 0),
  "COMMENT_TEXT"  NVARCHAR2(100) 	 NOT NULL,
  "CREATION_DATE" TIMESTAMP(6)       NOT NULL,
  "NEWS_ID"       NUMBER(20, 0)      NOT NULL,
  CONSTRAINT COMMENTS_PK PRIMARY KEY ("COMMENT_ID")
);
--------------------------------------------------------
--  DDL for Table TAG
--------------------------------------------------------

CREATE TABLE "TAG"
(
  "TAG_ID"   NUMBER(20, 0),
  "TAG_NAME" NVARCHAR2(30) NOT NULL,
  CONSTRAINT TAG_PK PRIMARY KEY ("TAG_ID")
);
--------------------------------------------------------
--  DDL for Table NEWS_TAG
--------------------------------------------------------

CREATE TABLE "NEWS_TAG"
(
  "NEWS_ID" NUMBER(20, 0),
  "TAG_ID"  NUMBER(20, 0)
);

--------------------------------------------------------
--  DDL for Table USER
--------------------------------------------------------
CREATE TABLE "USER"
(
  "USER_ID"           NUMBER(20, 0),
  "USER_NAME"         NVARCHAR2(50)  NOT NULL,
  "LOGIN"             VARCHAR2(30)   NOT NULL,
  "PASSWORD"          VARCHAR2(30)   NOT NULL,
  CONSTRAINT USER_PK PRIMARY KEY ("USER_ID")

);

--------------------------------------------------------
--  DDL for Table ROLES
--------------------------------------------------------
CREATE TABLE "ROLES"
(
  "USER_ID"           NUMBER(20, 0) NOT NULL,
  "ROLE_NAME"         VARCHAR2(50)   NOT NULL
);

--------------------------------------------------------
--  Ref Constraints for Table NEWS_AUTHOR
--------------------------------------------------------

ALTER TABLE "NEWS_AUTHOR" ADD CONSTRAINT "NEWS_AUTHOR_AUTHOR" FOREIGN KEY ("AUTHOR_ID")
REFERENCES "AUTHOR" ("AUTHOR_ID");
ALTER TABLE "NEWS_AUTHOR" ADD CONSTRAINT "NEWS_AUTHOR_NEWS" FOREIGN KEY ("NEWS_ID")
REFERENCES "NEWS" ("NEWS_ID");
--------------------------------------------------------
--  Ref Constraints for Table COMMENTS
--------------------------------------------------------

ALTER TABLE "COMMENTS" ADD CONSTRAINT "COMMENTS_NEWS" FOREIGN KEY ("NEWS_ID")
REFERENCES "NEWS" ("NEWS_ID");
--------------------------------------------------------
--  Ref Constraints for Table NEWS_TAG
--------------------------------------------------------

ALTER TABLE "NEWS_TAG" ADD CONSTRAINT "NEWS_TAG_NEWS" FOREIGN KEY ("NEWS_ID")
REFERENCES "NEWS" ("NEWS_ID");
ALTER TABLE "NEWS_TAG" ADD CONSTRAINT "NEWS_TAG_TAG" FOREIGN KEY ("TAG_ID")
REFERENCES "TAG" ("TAG_ID");

--------------------------------------------------------
--  Ref Constraints for Table ROLES
--------------------------------------------------------

ALTER TABLE "ROLES" ADD CONSTRAINT "USER_ROLES" FOREIGN KEY ("USER_ID")
REFERENCES "USER" ("USER_ID");

--------------------------------------------------------
--  SEQUENCE for Data Base
--------------------------------------------------------
CREATE SEQUENCE AUTHOR_ID_SEQ
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE NEWS_ID_SEQ
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE COMMENT_ID_SEQ
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE TAG_ID_SEQ
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE USER_ID_SEQ
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;