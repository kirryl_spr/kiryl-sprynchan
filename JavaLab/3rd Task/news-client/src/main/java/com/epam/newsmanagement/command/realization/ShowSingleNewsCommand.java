package com.epam.newsmanagement.command.realization;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.domain.PostedNews;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;

import static com.epam.newsmanagement.util.ConstantPool.SEARCH_CRITERIA;
import static com.epam.newsmanagement.util.ConstantPool.NEWS_ID;
import static com.epam.newsmanagement.util.ConstantPool.SINGLE_NEWS;
import static com.epam.newsmanagement.util.ConstantPool.NEXT_NEWS;
import static com.epam.newsmanagement.util.ConstantPool.PREVIOUS_NEWS;
import static com.epam.newsmanagement.util.ConstantPool.TIME_ZONE;

/**
 * This class implements a pattern command
 * Realization of showing subglt news action
 * @author Kirill
 */

@Component
public class ShowSingleNewsCommand extends Command {
	
	/**
	 * Required service
	 */
	@Autowired
	private NewsManagementService newsManagerService;
	
	/**
	 * Required service
	 */
	@Autowired
	private NewsService newsService;

	/**
	 * URL of page after action.
	 */
	private static String FORWARD_PAGE = "WEB-INF/views/pages/single-news.jsp";

	
	/**
     * This is abstract method which causes methods a business-logic and sends results on jsp
     *
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws com.epam.sprynchan.web_project.command.exception.CommandException a ServletException
     */
	@Override
	public void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws CommandException {
		try {
			String newsId = (String) request.getParameter(NEWS_ID);
			Long neededId = new Long(newsId);

			String timeZone = request.getParameter(TIME_ZONE);
			SearchCriteria searchCriteria = (SearchCriteria) request
					.getSession().getAttribute(SEARCH_CRITERIA);

			Long nextNewsId = newsService.isNext(neededId, searchCriteria);
			Long previousNewsId = newsService
					.isPrevious(neededId, searchCriteria);

			PostedNews singleNews = newsManagerService.readSingleNews(neededId);
			setForward(FORWARD_PAGE);

			request.setAttribute(TIME_ZONE, timeZone);
			request.setAttribute(SINGLE_NEWS, singleNews);
			request.setAttribute(NEXT_NEWS, nextNewsId);
			request.setAttribute(PREVIOUS_NEWS, previousNewsId);
		} catch (NumberFormatException e) {
			throw new CommandException(e);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}

	}

}
