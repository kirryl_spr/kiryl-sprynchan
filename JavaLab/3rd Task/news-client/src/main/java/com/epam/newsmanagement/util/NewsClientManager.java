package com.epam.newsmanagement.util;

import static com.epam.newsmanagement.util.ConstantPool.AUTHOR_LIST;
import static com.epam.newsmanagement.util.ConstantPool.FULL_NEWS_LIST;
import static com.epam.newsmanagement.util.ConstantPool.NEWS_COUNT_ON_PAGE;
import static com.epam.newsmanagement.util.ConstantPool.PAGES_COUNT;
import static com.epam.newsmanagement.util.ConstantPool.PAGE_NUMBER;
import static com.epam.newsmanagement.util.ConstantPool.TAG_LIST;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.PostedNews;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

/**
 * NewsManager class, contain all additional methods for news-admin application
 * 
 * @author Kiryl Sprynchan
 *
 */
@Component
public class NewsClientManager {
	/**
	 * Required service
	 */
	@Autowired
	private AuthorService authorService;

	/**
	 * Required service
	 */
	@Autowired
	private NewsManagementService newsManagerService;
	
	/**
	 * Required service
	 */
	@Autowired
	private NewsService newsService;

	/**
	 * Required service
	 */
	@Autowired
	private TagService tagService;

	/**
	 * Method returned count of news page, for pagination on news-list page
	 * 
	 * @param newsCount
	 *            all news count from database
	 * @param newsOnPageCount
	 *            news count on one page
	 * @return count of pages
	 */
	public static int getPagesCount(List<News> newsList, int newsOnPageCount) {
		int pagesCount = (newsList.size() + (newsOnPageCount - 1))
				/ newsOnPageCount;
		return pagesCount;
	}
	
	public void setPageInformation(HttpServletRequest request, SearchCriteria searchCriteria) throws ServiceException{

		int pageNumber = 1;
		if (request.getParameter(PAGE_NUMBER) != null) {
			pageNumber = Integer.parseInt(request
					.getParameter(PAGE_NUMBER));
		}

		List<News> newsList = newsService.getSortedNews(searchCriteria);
		int pagesCount = NewsClientManager.getPagesCount(newsList,
				NEWS_COUNT_ON_PAGE);

		List<PostedNews> fullNewsList = new ArrayList<PostedNews>();
		fullNewsList = newsManagerService.readNewsOnPage(pageNumber, NEWS_COUNT_ON_PAGE, searchCriteria);
		
		List<Author> authorList = authorService.readAll();
		List<Tag> tagList = tagService.readAll();
		
		request.setAttribute(PAGES_COUNT, pagesCount);
		request.setAttribute(AUTHOR_LIST, authorList);
		request.setAttribute(TAG_LIST, tagList);
		request.setAttribute(FULL_NEWS_LIST, fullNewsList);
		
		request.getSession().setAttribute(PAGE_NUMBER, pageNumber);
	}

}
