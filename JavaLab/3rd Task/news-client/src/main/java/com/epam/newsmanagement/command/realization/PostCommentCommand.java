package com.epam.newsmanagement.command.realization;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.PostedNews;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;

import static com.epam.newsmanagement.util.ConstantPool.NEWS_ID;
import static com.epam.newsmanagement.util.ConstantPool.COMMENT_TEXT;
import static com.epam.newsmanagement.util.ConstantPool.NEXT_NEWS;
import static com.epam.newsmanagement.util.ConstantPool.PREVIOUS_NEWS;
import static com.epam.newsmanagement.util.ConstantPool.SEARCH_CRITERIA;
import static com.epam.newsmanagement.util.ConstantPool.SINGLE_NEWS;


/**
 * This class implements a pattern command
 * Realization of posting comment action
 * @author Kirill
 */
@Component
public class PostCommentCommand extends Command {

	/**
	 * Required service
	 */
	@Autowired
	private NewsManagementService newsManagerService;

	/**
	 * Required service
	 */
	@Autowired
	private NewsService newsService;
	
	/**
	 * Required service
	 */
	@Autowired
	private CommentService commentService;

	/**
	 * URL of page after action.
	 */
	private final static String FORWARD_PAGE = "controller?command=SHOW_SINGLE_NEWS&newsId=";

	/**
     * This is abstract method which causes methods a business-logic and sends results on jsp
     *
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws com.epam.sprynchan.web_project.command.exception.CommandException a ServletException
     */
	@Override
	public void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws CommandException {
		try {
			String newsIdInString = (String) request.getParameter(NEWS_ID);
			Long newsId = new Long(newsIdInString);

			String commentText = (String) request.getParameter(COMMENT_TEXT);

			Comment comment = new Comment();
			comment.setNewsId(newsId);
			comment.setCommentText(commentText);
			comment.setCreationDate(new Date());

			commentService.create(comment);

			PostedNews singleNews = newsManagerService.readSingleNews(newsId);
			SearchCriteria searchCriteria = (SearchCriteria) request
					.getSession().getAttribute(SEARCH_CRITERIA);
			
			Long nextNewsId = newsService.isNext(newsId, searchCriteria);
			Long previousNewsId = newsService
					.isPrevious(newsId, searchCriteria);

			setForward(FORWARD_PAGE + newsId);

			request.setAttribute(NEXT_NEWS, nextNewsId);
			request.setAttribute(PREVIOUS_NEWS, previousNewsId);
			request.setAttribute(SINGLE_NEWS, singleNews);
			

		} catch (ServiceException e) {
			throw new CommandException(e);
		}

	}

}
