package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.command.realization.ChangeLocaleCommand;
import com.epam.newsmanagement.command.realization.FilterNewsCommand;
import com.epam.newsmanagement.command.realization.PostCommentCommand;
import com.epam.newsmanagement.command.realization.ResetSearchCriteria;
import com.epam.newsmanagement.command.realization.ShowNewsCommand;
import com.epam.newsmanagement.command.realization.ShowSingleNewsCommand;
import com.epam.newsmanagement.exception.CommandException;

import static com.epam.newsmanagement.util.ConstantPool.COMMAND;

public class CommandFactory {
	
	private static ApplicationContext context = new ClassPathXmlApplicationContext(
			"servlet-context.xml");
	/**
     * String for command parameter parsing
     */

    /**
     * Strings contains keys for errors in resource bundle
     */
    private static final String ERROR_NO_SUCH_COMMAND = "error.no.such.command";
    private static final String ERROR_IN_GETTING_COMMAND = "error.in.getting.command";
    private static final String ERROR_IN_CREATING_COMMAND = "error.in.creating.command";

    /**
     * This method get a commandName from request, checks this name and return
     * a instance of class <code>Command</code>
     *
     * @param request a HttpServletRequest
     * @return a new <code>Command</code>, this a instance of class which implements
     * a pattern command
     */
    public static Command getCommand(HttpServletRequest request) throws CommandException {
        Command command;
        CommandEnum commandType = getCommandEnum(request.getParameter(COMMAND));

        switch (commandType) {
            case SHOW_NEWS:
            	command = context.getBean(ShowNewsCommand.class);
    			break;
            case FILTER_NEWS:
            	command = context.getBean(FilterNewsCommand.class);
    			break;
            case RESET_SEARCH_CRITERIA:
            	command = context.getBean(ResetSearchCriteria.class);
    			break;
            case SHOW_SINGLE_NEWS:
            	command = context.getBean(ShowSingleNewsCommand.class);
            	break;
            case POST_COMMENT:
            	command = context.getBean(PostCommentCommand.class);
            	break;
            case CHANGE_LOCALE:
            	command = context.getBean(ChangeLocaleCommand.class);
            	break;
            default:
                throw new CommandException(ERROR_NO_SUCH_COMMAND);
        }
        return command;
    }

    /**
     * This method creats a instance of <code>CommandEnum</code> by a command name
     * If command name is incorrect then <code>CommandEnum.UNKNOWN_COMMAND</code> will return
     *
     * @param command a name of command
     * @return a instance of <code>CommandEnum</code>
     */
    private static CommandEnum getCommandEnum(String command) throws CommandException {
        CommandEnum commandEnum;

        try {
            commandEnum = CommandEnum.valueOf(command);
        } catch (IllegalArgumentException ex) {
            throw new CommandException(ERROR_IN_GETTING_COMMAND, ex);
        } catch (NullPointerException ex) {
            throw new CommandException(ERROR_IN_CREATING_COMMAND, ex);
        }
        return commandEnum;
    }
}
