package com.epam.newsmanagement.command;

/**
 * This enum contains enumeration of all commands
 *
 * @author Kirill
 */
public enum CommandEnum {
	SHOW_NEWS, SHOW_SINGLE_NEWS, POST_COMMENT, FILTER_NEWS, RESET_SEARCH_CRITERIA, CHANGE_LOCALE;
}
