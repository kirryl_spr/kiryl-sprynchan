function selectedAuthor(authorId) {
	var authorSelect = document.getElementById('authorSelect');
	if (authorId != "") {
		authorSelect[authorId].selected = true;
	}

}

function selectedTags(str) {
	var tagOptions = document.getElementById("tagSelect").options;

	var trimedString = str.substring(1, str.length - 1);

	var regex = /\s*,\s*/;
	var tagsIdList = trimedString.split(regex);

	for (var i = 0; i < tagOptions.length; i++) {
		if (tagsIdList.indexOf(tagOptions[i].value) != -1) {
			tagOptions[i].selected = true;
		}
	}
}