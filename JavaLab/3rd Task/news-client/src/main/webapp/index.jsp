<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="locale" value="en" scope="session"/>

<jsp:forward page="/controller">
	<jsp:param name="command" value="SHOW_NEWS" />
</jsp:forward>