<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${singleNews.news.title}</title>
</head>
<body>
	<fmt:setLocale value="${sessionScope.locale}" />
	<fmt:setBundle basename="Resources" />


	<div class="show-single-news">
		<a
			href="controller?command=SHOW_NEWS&pageNumber=${sessionScope.pageNumber}"
			id="back_button"><fmt:message key="back" /></a>

		<div class="head-information-news">
			<div class="news-title">
				<strong><c:out value="${singleNews.news.title}"/></strong>
			</div>

			<div class="author-date">
				<a id="author">(<fmt:message key="by_label" />
					<c:out value="${singleNews.author.authorName}"/>)
				</a> <a id="date">
						<fmt:formatDate timeZone="GMT+${timeZone}" type="both" 
            					dateStyle="short" pattern="YYYY-MM-dd HH:mm"
						value="${singleNews.news.creationDate}"/>
				</a>
			</div>
		</div>

		<div class="full-text"><c:out value="${singleNews.news.fullText}"/></div>

		<div class="comment-content">
			<c:forEach items="${singleNews.commentList}" var="comment">
				<div class="single-comment-admin">
					<a id="comment-date">${comment.creationDate}</a>
					<div class="comment-text">
						<div class="comment-text-value"><c:out value="${comment.commentText}"/></div>
					</div>
				</div>
			</c:forEach>
		</div>

		<div class="adding-comment-hidden" style="display: none;"><fmt:message key="empty_comment"/></div>
		<div class="add-comment">
			<form action="controller" id="postComment"  method="POST">
				<textarea name="commentText" id="new-comment" required
					maxlength="100"></textarea>
				<input type="hidden" name="newsId" value="${singleNews.news.newsId}">
				<input type="hidden" name="command" value="POST_COMMENT"> <input
					type="button" id="post-comment"
					value="<fmt:message key="post_comment"/>">
			</form>
		</div>


		<div class="change-news">
			<c:if test="${previousNews ne 0}">
				<a onclick="changeNews('${previousNews}')"id="prev-link"> <fmt:message key="previous" />
				</a>
			</c:if>

			<c:if test="${nextNews ne 0}">
				<a
					onclick="changeNews('${nextNews}')"
					id="next-link"> <fmt:message key="next" />
				</a>
			</c:if>
		</div>

	</div>

	<script>
		$("#post-comment").on("click", function() {
			var commentText = $("#new-comment").val();
			var x = new Date();
			if (commentText.length > 100 || commentText.length == 0) {
						$(".adding-comment-hidden:hidden").show();
				
			} else {
				var postTo = "${pageContext.request.contextPath}/controller?";
			 
				$.ajax({
				        type: "POST",
				        url: postTo,
				        dataType: "html",
				        data: {
				        	command: $('[name=command]').val(), 
				        	commentText: $('[name=commentText]').val(), 
							newsId: $('[name=newsId]').val()
						},
						success: function(){
							window.location.href = "${pageContext.request.contextPath}/controller?command=SHOW_SINGLE_NEWS&newsId="+$('[name=newsId]').val()+"&timeZone="+-x.getTimezoneOffset()/60;
						}
				});
				
			}
		});
		
		function changeNews(newsId){
			var x = new Date();
			document.location.href = "${pageContext.request.contextPath}/controller?command=SHOW_SINGLE_NEWS&newsId="+newsId+"&timeZone="+-x.getTimezoneOffset()/60;
		}
	</script>

</body>
</html>