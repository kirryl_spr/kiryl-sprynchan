<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>News-Portal</title>
</head>
<body>
	<fmt:setLocale value="${sessionScope.locale}" />
	<fmt:setBundle basename="Resources" />

	<div class="show-news-content">
		<div class="search-criteria">
			<div class="unchosen">
				<div id="unchosen-hidden" style="display: none">
					<fmt:message key="choose_something" />
					!!!
				</div>
			</div>

			<form action="controller" id="filter-news" name="filterForm">

				<select name="author" id="authorSelect">
					<option value="" disabled selected><fmt:message
							key="choose_author" /></option>
					<c:forEach items="${authorList}" var="author">
						<option value="${author.authorId}"><c:out value="${author.authorName}"/></option>
					</c:forEach>
				</select> <select id="tagSelect" class="multi-select" multiple name="tagList"
					data-placeholder="<fmt:message key="choose_tags"/>">
					<c:forEach items="${tagList}" var="tag">
						<option value="${tag.tagId }"><c:out value="${tag.tagName}"/></option>
					</c:forEach>
				</select> <input type="button" id="filter-button"
					value="<fmt:message key="filter"/>" name="filterButton"> <input
					type="hidden" name="command" value="FILTER_NEWS" />
			</form>

			<form action="controller" id="reset-reset-searchCriteria">
				<input type="submit" value="<fmt:message key="reset"/>"> <input
					type="hidden" name="command" value="RESET_SEARCH_CRITERIA" />
			</form>
		</div>

		<c:forEach items="${fullNewsList}" var="fullNews">

			<div class="news">
				<div class="news-title-link">
					<strong><c:out value="${fullNews.news.title}"/></strong>
				</div>

				<div class="by-author-label">
					(
					<fmt:message key="by_label" />
					<c:out value="${fullNews.author.authorName }"/>
					 )
				</div>

				<div class="creation-date-label">${fullNews.news.modificationDate}</div>

				<div class="short-text"><c:out value="${fullNews.news.shortText}"/></div>

				<div class="data-about-news">
					<div class="news-tags">
						<c:forEach items="${fullNews.tagList}" var="tag">
						<c:out value="${tag.tagName}"/>,
					</c:forEach>
					</div>

					<div class="news-comments">
						<fmt:message key="comments_label" />
						: (${fn:length(fullNews.commentList)})
					</div>

					<div class="edit-news-checkbox">
						<a
							onclick="goToShowSingleNews('${fullNews.news.newsId}')">
							<fmt:message key="view" />
						</a>
					</div>
				</div>

			</div>

		</c:forEach>

		<div class="paginal-nav-bar">
			<form action="controller" id="paginal-nav-bar">
				<c:forEach var="i" begin="1" end="${pagesCount}">
					<c:choose>
						<c:when test="${pageNumber eq i}">
							<input type="submit" id="current-page" value="${i}"
								name="pageNumber" />
						</c:when>

						<c:otherwise>
							<input type="submit" value="${i}" name="pageNumber" />
						</c:otherwise>
					</c:choose>
				</c:forEach>
				<input type="hidden" name="command" value="SHOW_NEWS" />
			</form>
		</div>

	</div>

	<script>
		$(document).ready(selectedTags('${searchCriteria.idTagsList}'));
		$(document).ready(selectedAuthor('${searchCriteria.authorId}'));

		"use strict";
		$(".multi-select").chosen();

		$("#filter-button").on("click", function() {

			var tags = $(".search-choice");

			if ($("#authorSelect").val() || tags.length != 0) {
				$("#filter-news").submit();
			} else {
				$("#unchosen-hidden").show(1500);
			}
		});
		
		function goToShowSingleNews(newsId){
			var x = new Date();
			document.location.href = "${pageContext.request.contextPath}/controller?command=SHOW_SINGLE_NEWS&newsId="+newsId+"&timeZone="+-x.getTimezoneOffset()/60;
		}
	</script>

</body>
</html>

