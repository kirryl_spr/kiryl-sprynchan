package com.epam.newsmanagement.dao.implementations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Class <code>CommentDaoImplTest</code> content list of public Data Base Unit
 * tests, which check right work of Data Access layer class
 * <code>CommentDao</code> methods.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.dao.CommentDao
 * @see com.epam.newsmanagement.domain.Comment
 * @since 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, 
	TransactionalTestExecutionListener.class,
	DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = { "classpath:/SpringTest.xml" })
@DatabaseSetup(value = "classpath:/data.before.operation.xml" )

public class CommentDaoImplTest {

	/**
	 * reference on CommentDao class, to use it all public methods
	 */
	@Autowired
	private CommentDao commentDao;
	
	/**
	 * Data Base Unit test, that verify work create() method in the
	 * CommentDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws DaoException{
		Long commentId = null;
		String commentText = "FirstText";
		Date creationDate = Timestamp.valueOf("2015-03-30 03:45:24");
		Long newsId = 2L;

		Comment comment = new Comment();
		comment.setCommentText(commentText);
		comment.setCreationDate(creationDate);
		comment.setNewsId(newsId);
		commentId = commentDao.create(comment);
		comment.setCommentId(commentId);
		

		Comment createdComment = commentDao.read(commentId);
		
		assertEqualsComment(comment, createdComment);
	}
	
	/**
	 * Data Base Unit test, that verify work read() method in the CommentDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws DaoException {
		Long commentId = 1L;
		String commentText = "FirstText";
		Date creationDate = Timestamp.valueOf("2015-03-30 03:45:24");
		Long newsId = 2L;

		Comment comment = new Comment();
		comment.setCommentId(commentId);
		comment.setCommentText(commentText);
		comment.setCreationDate(creationDate);
		comment.setNewsId(newsId);

		Comment readedComment = commentDao.read(commentId);

		assertEqualsComment(comment, readedComment);
	}
	
	/**
	 * Data Base Unit test, that verify work readAll() method in the
	 * CommentDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws DaoException {
		Long commentId = 2L;
		String commentText = "SecondText";
		Date creationDate = Timestamp.valueOf("2015-03-30 03:45:24");
		Long newsId = 2L;
		
		int expectedSize = 4;

		Comment comment = new Comment();
		comment.setCommentId(commentId);
		comment.setCommentText(commentText);
		comment.setCreationDate(creationDate);
		comment.setNewsId(newsId);
		
		List<Comment> commentList = commentDao.readAll();

		Comment readedComment = commentList.get(1);

		assertEquals(commentList.size(), expectedSize);
		
		assertEqualsComment(comment, readedComment);
	}
	
	/**
	 * Data Base Unit test, that verify work update() method in the
	 * CommentDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws DaoException {
		Long commentId = 1L;
		String commentText = "SuperNewText";
		Date creationDate = Timestamp.valueOf("2015-03-29 03:45:24");
		Long newsId = 2L;

		Comment comment = new Comment();
		comment.setCommentId(commentId);
		comment.setCommentText(commentText);
		comment.setCreationDate(creationDate);
		comment.setNewsId(newsId);

		commentDao.update(comment);

		Comment updatedComment = commentDao.read(commentId);
		
		assertEqualsComment(comment, updatedComment);
	}
	
	/**
	 * Public Data Base Unit test, that verify work delete() method in the
	 * CommentDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws DaoException {
		Long commentId = 1L;

		Comment comment = new Comment();
		comment = commentDao.read(commentId);
		assertNotNull(comment);

		commentDao.delete(commentId);
		comment = commentDao.read(commentId);
		assertNull(comment);
	}
	
	/**
	 * Private method that is used for comparing two objects
	 * 
	 * @param firstComment
	 * @param secondComment
	 */
	private void assertEqualsComment(Comment firstComment, Comment secondComment){
		assertEquals(firstComment.getCommentId(), secondComment.getCommentId());
		assertEquals(firstComment.getCommentText(), secondComment.getCommentText());
		assertEquals(firstComment.getCreationDate(), secondComment.getCreationDate());
		assertEquals(firstComment.getNewsId(), secondComment.getNewsId());
	}
}
