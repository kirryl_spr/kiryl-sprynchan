package com.epam.newsmanagement.service.implementation;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

import static com.epam.newsmanagement.utils.ServiceUtil.isObjectNull;

/**
 * Public class <code>NewsServiceImpl</code> is an element of Service layer and
 * working with News data base instance. This is a realization of
 * <code>NewsService</code> interface and it performs all operations described
 * in this interface.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.service.NewsService
 * @see com.epam.newsmanagement.domain.News
 * @since 1.0
 */
@Service
public class NewsServiceImpl implements NewsService {

	/**
	 * object to conduct logging
	 */
	private static final Logger logger = Logger.getLogger(NewsServiceImpl.class
			.getName());

	/**
	 * link to the interface part, providing access to basic operations
	 */
	@Autowired
	private NewsDao newsDao;

	/**
	 * Override public method used to add record to the table News, which calls
	 * create() method from NewsDao interface.
	 *
	 * @param news
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws ServiceException
	 *             user-defined exception occurs when when any
	 *             <code>DaoException</code> throw up
	 * @throws ServiceException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 */
	public Long create(News news) throws ServiceException {
		Long newsId = null;

		isObjectNull(news, "News is epmty.");

		try {
			newsId = newsDao.create(news);
			return newsId;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot create news.", e);
		}
	}

	/**
	 * Override public method used to read record from the table News, which
	 * calls read() method from NewsDao interface.
	 *
	 * @param newsId
	 *            unique identifier of instance which will be found
	 * @return find News instance
	 * @throws ServiceException
	 *             user-defined exception occurs when when any
	 *             <code>DaoException</code> throw up
	 */
	public News read(Long newsId) throws ServiceException {
		News news = null;

		try {
			news = newsDao.read(newsId);
			return news;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot read news.", e);
		}
	}

	/**
	 * Override public method used to read all records from the table News,
	 * which calls readAll() method from NewsDao interface.
	 *
	 * @return list of find News instances
	 * @throws ServiceException
	 *             user-defined exception occurs when when any
	 *             <code>DaoException</code> throw up
	 */
	public List<News> readAll() throws ServiceException {
		List<News> newsList = null;

		try {
			newsList = newsDao.readAll();
			return newsList;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot read all news.", e);
		}
	}

	/**
	 * Override public method used to update record in the table News, which
	 * calls update() method from NewsDao interface.
	 *
	 * @param news
	 *            instance which will be update in the data base
	 * @throws ServiceException
	 *             user-defined exception occurs when when any
	 *             <code>DaoException</code> throw up
	 * @throws ServiceException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 */
	public void update(News news) throws ServiceException {

		isObjectNull(news, "News is epmty.");

		try {
			newsDao.update(news);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot update news.", e);
		}
	}

	/**
	 * Override public method used to delete record from the table News, which
	 * calls delete() method from NewsDao interface.
	 *
	 * @param newsId
	 *            unique identifier of instance which will be delete
	 * @throws ServiceException
	 *             user-defined exception occurs when when any
	 *             <code>DaoException</code> throw up
	 */
	public void delete(Long newsId) throws ServiceException {
		try {

			newsDao.delete(newsId);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot delete news.", e);
		}

	}


	public List<News> getSortedNews(SearchCriteria searchCriteria) throws ServiceException {
		List<News> newsList = null;

		try {
			newsList = newsDao.getSortedNews(searchCriteria);
			return newsList;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot sort news by comments count.", e);
		}
	}

	/**
	 * Override public method used to bind Author instance with instance of
	 * News, and add same record to the NewsAuthor table in data base, calls
	 * createAuthorForNews() method from AuthorDao interface.
	 *
	 * @param news
	 *            News instance for binding with Author instance
	 * @param author
	 *            Author instance for binding with News instance
	 * @throws ServiceException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 */
	public void saveNewsAuthor(News news, Author author)
			throws ServiceException {

		isObjectNull(news, "News is null.");

		isObjectNull(author, "Author is null.");

		try {
			newsDao.saveNewsAuthor(news.getNewsId(), author.getAuthorId());
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot save news-author.", e);
		}
	}

	/**
	 * Override public method used to bind Tag instance with instance of News,
	 * and add same record to the NewsTag table in data base, which calls
	 * createTagForNews() method from TagDao interface.
	 *
	 * @param news
	 *            News instance for binding with Tag instance
	 * @param tagList
	 *            list<Tag> instance for binding with News instance
	 * @throws ServiceException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 */
	public void saveNewsTagList(News news, List<Tag> tagList)
			throws ServiceException {

		isObjectNull(news, "News is null.");

		isObjectNull(tagList, "TagList is null.");

		try {

			Iterator<Tag> it = tagList.iterator();

			while (it.hasNext()) {
				Tag obj = (Tag) it.next();
				newsDao.saveNewsTag(news.getNewsId(), obj.getTagId());
			}
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot save news-taglist.", e);
		}

	}
	
	/**
	 * Get list News instances from News table in data base by sent SearchCriteria instance, 
	 * using the readAllNews(SearchCriteria searchCriteria) method in NewsDao interface.
	 *
	 * @return list of News which are sorted by number of comments and modification date
	 * @param searchCriteria
	 *            SearchCriteria instance for binding with other instances
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public List<News> readNewsByPageNumber(int pageNumber, int newsOnPageCount, SearchCriteria searchCriteria)
			throws ServiceException {
		List<News> newsList = null;
		int lowBorder = (pageNumber - 1) * newsOnPageCount;
		int heightBorder = pageNumber * newsOnPageCount;

		try {
			newsList = newsDao.readNewsByPageNumber(lowBorder, heightBorder, searchCriteria);
		} catch (DaoException e) {
			throw new ServiceException("Cannot read news with by page number.",
					e);
		}
		return newsList;
	}

	/**
	 * Checking is there next news after current
	 * according to searching criteria
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @param searchCriteria
	 *            element of SearchCriteria instance            
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	@Override
	public Long isNext(Long newsId, SearchCriteria searchCriteria) throws ServiceException {
		isObjectNull(newsId, "NewsId is null.");
		Long nextNewsId = null;
		
		try {
			nextNewsId = newsDao.isNext(newsId, searchCriteria);
		} catch (DaoException e) {
			throw new ServiceException("Cannot read next news",
					e);
		} 
		return nextNewsId;
	}

	/**
	 * Checking is there prevoius news after current
	 * according to searching criteria
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @param searchCriteria
	 *            element of SearchCriteria instance            
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	@Override
	public Long isPrevious(Long newsId, SearchCriteria searchCriteria) throws ServiceException {
		isObjectNull(newsId, "NewsId is null.");
		Long previousNewsId = null;
		
		try {
			previousNewsId = newsDao.isPrevious(newsId, searchCriteria);
		} catch (DaoException e) {
			throw new ServiceException("Cannot read previous news",
					e);
		} 
		return previousNewsId;
	}

	/**
	 * Updating NEWS_AUTHOR table after editing news action.
	 *
	 * @param news
	 *            element of News instance to delete necessary rows
	 * @param author
	 *            element of Author instance to delete necessary rows           
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	@Override
	public void updateNewsAuthor(News news,Author author)
			throws ServiceException {
		isObjectNull(news, "News is null.");

		isObjectNull(author, "Author is null.");

		try {
			newsDao.saveNewsAuthor(news.getNewsId(), author.getAuthorId());
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot save news-author.", e);
		}
		
	}

	/**
	 * Override method used to delete operation from NEWS_TAG table in data base by sent Long
	 * instance, using the deleteNewsTag(Long newsId) method in TagDao interface.
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @@throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public void deleteNewsTag(Long newsId) throws ServiceException {
		try {

			newsDao.deleteNewsTag(newsId);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot delete news tags.", e);
		}
	}
}
