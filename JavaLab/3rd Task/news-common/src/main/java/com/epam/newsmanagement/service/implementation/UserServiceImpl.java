package com.epam.newsmanagement.service.implementation;

import com.epam.newsmanagement.dao.RoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.UserDao;
import com.epam.newsmanagement.domain.User;
import com.epam.newsmanagement.exception.DaoException;

/**
 * Public class <code>UserServiceImpl</code> is an element of Service layer and
 * working with User and Role data base instance. This is a realization of
 * <code>UserDetailsService</code> interface and it performs all operations described in
 * this interface.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.domain.User
 * @see com.epam.newsmanagement.domain.Role
 * @since 1.0
 */
@Service
public class UserServiceImpl implements UserDetailsService{
	
	/**
	 * Required dao instance
	 */
	@Autowired
	private UserDao userDao;

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private Environment environment;
	
	/**
	 * Override method used to read information about user and its roles.
	 *
	 * @param login
	 *            String instance describing login attribute
	 * @return unique identifier of user instance
	 * @throws UsernameNotFoundException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 */
	@Override
	public UserDetails loadUserByUsername(final String login)
			throws UsernameNotFoundException {
		try {
			User user = userDao.readUserByLogin(login);
			if(environment.acceptsProfiles("SQL")){
				user.setAuthorities(roleDao.readRolesByLogin(login));
			}
			return user;
		} catch (DaoException e) {
			throw new UsernameNotFoundException("Cannot read user", e);
		}
		
	}

}
