package com.epam.newsmanagement.dao.hibernate_implementation;


import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.UserDao;
import com.epam.newsmanagement.domain.User;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.utils.HibernateSessionUtil;

/**
 * Realization of using daoMethods for working with USERS table in database
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.domain.User
 * @since 1.0
 */
public class UserDaoImpl implements UserDao{

	/**
	 * Object which used for connecting to the database
	 */
	@Autowired
	private HibernateSessionUtil sessionUtil;
	
	private static final String LOGIN_PARAMETER = "login";
	
	/**
	 * Sql query for reading users from database by login.
	 */
	private static final String HQL_READ_USER = "select user from User user where user.login = :login";
	

	/**
	 * Reading user from database by login value.
	 *
	 * @return list of entity records to read
	 * @throws com.epam.newsmanagement.exception.DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	@Override
	public User readUserByLogin(String login) throws DaoException {
		Session session = null;
		
		try{
			session = sessionUtil.getSessionFactory().openSession(); 
			Query query = session.createQuery(HQL_READ_USER);
			query.setParameter(LOGIN_PARAMETER, login);
			User user = (User) query.uniqueResult();
			return user;
		}catch(HibernateException ex){
			throw new DaoException("HibernateException caught during reading user. See details: ", ex);
		}finally{
			sessionUtil.closeSession(session);
		}
	}

}
