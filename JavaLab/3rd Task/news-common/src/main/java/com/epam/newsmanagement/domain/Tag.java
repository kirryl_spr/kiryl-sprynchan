package com.epam.newsmanagement.domain;

import java.io.Serializable;
import java.util.Set;

/**
 * Public class <code>Tag</code> is one of Entities classes. Its content is
 * fully consistent with Table Author in data base, which we use for. The main
 * role is to store associated with the table information(data). Can be an
 * element <code>HashTable</code>
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @since 1.0
 */
public class Tag implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3065775582675082943L;

	/**
	 * unique identifier of instance
	 */
	private Long tagId;
	
	/**
	 * parameter describe tag title Name
	 */
	private String tagName;
	
	/**
	 * parameter describing News that author has
	 */
	private Set<News> newsSet;

	
	/**
	 * Constructor without parameters for creating an object.
	 */
	public Tag() {
	}

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	
	
	public Set<News> getNewsSet() {
		return newsSet;
	}

	public void setNewsSet(Set<News> newsSet) {
		this.newsSet = newsSet;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(getClass() == o.getClass()))
			return false;

		Tag tag = (Tag) o;

		if (tagId != null ? !tagId.equals(tag.tagId) : tag.tagId != null)
			return false;
		if (tagName != null ? !tagName.equals(tag.tagName)
				: tag.tagName != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = (tagId != null ? tagId.hashCode() : 0);
		result = 31 * result + (tagName != null ? tagName.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Tag{" + "tagId=" + tagId + ", tagName='" + tagName + '\'' + '}';
	}
}
