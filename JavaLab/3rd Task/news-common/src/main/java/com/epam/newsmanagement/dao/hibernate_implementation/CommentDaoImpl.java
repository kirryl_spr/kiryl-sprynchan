package com.epam.newsmanagement.dao.hibernate_implementation;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.utils.HibernateSessionUtil;

/**
 * Public class <code>CommentDaoImpl</code> is an element of Data Access layer
 * and working with Comment data base instance. This is a single ton realization
 * of <code>CommentDao</code> interface and it performs all operations described
 * in this interface. Realized pattern Singleton.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsprotal.dao.CommentDao
 * @see com.epam.newsprotal.domain.Comment
 * @since 1.0
 */
public class CommentDaoImpl implements CommentDao{
	
	/**
	 * Object which used for connecting to the database
	 */
	@Autowired
	private HibernateSessionUtil sessionUtil;
	
	/**
	 * Parameter for query commentId
	 */
	private static final String COMMENT_ID_PARAMETER = "commentId";
	
	/**
	 * Parameter for query newsId
	 */
	private static final String NEWS_ID_PARAMETER = "newsId";
	
	/**
	 * HQL query for delete comment
	 */
	private static final String HQL_DELETE_QUERY = "delete from Comment "  + 
            "WHERE commentId = :commentId";
	
	/**
	 * HQL query for searching comments by news
	 */
	private static final String HQL_SEARCH_COMMENTS_BY_NEWS_QUERY = "select comment from Comment comment "
			+ "where comment.newsId = :newsId order by creationDate DESC";

	/**
	 * Override public method used to add record to the table Comment.
	 *
	 * @param comment
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public Long create(Comment comment) throws DaoException {
		Session session = null;
		
		try{
			session = sessionUtil.getSessionFactory().openSession();
			
			session.beginTransaction();
			Long commentId = (Long) session.save(comment);
			session.getTransaction().commit();
			
			return commentId;
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during creating comment. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override method used to read record from the table Comment.
	 *
	 * @param commentId
	 *            unique identifier of instance which will be found
	 * @return find Comment instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public Comment read(Long commentId) throws DaoException {
		Session session = null;
		
		try{
			session = sessionUtil.getSessionFactory().openSession();
			Comment comment = (Comment) session.get(Comment.class, commentId);
			
			return comment;
		}catch (HibernateException ex) {	
			throw new DaoException("HibernateException caught during reading comment by id. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override method used to read all records from the table Comment.
	 *
	 * @return list of find Comment instances
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	@SuppressWarnings("unchecked")
	public List<Comment> readAll() throws DaoException {
		Session session = null;
		List<Comment> commentList = null;

		try {
			session = sessionUtil.getSessionFactory().openSession();
			commentList = session.createCriteria(Comment.class).list();
			
			return commentList;
		} catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during reading all comments. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override method used to update record in the table Comment.
	 *
	 * @param comment
	 *            instance which will be update in the data base
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void update(Comment comment) throws DaoException {
		Session session = null;
		
		try{
			session = sessionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(comment);
			session.getTransaction().commit();
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during updating comment. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override method used to delete record from the table Comment.
	 *
	 * @param commentId
	 *            unique identifier of instance which will be delete
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void delete(Long commentId) throws DaoException {
		Session session = null;
		
		try{
			session = sessionUtil.getSessionFactory().openSession();	
			
			session.beginTransaction();
			
			Query query = session.createQuery(HQL_DELETE_QUERY);
			query.setParameter(COMMENT_ID_PARAMETER, commentId);
			query.executeUpdate();
			
			session.getTransaction().commit();
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during deleting comment. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override method used to search List of comments from the table Comments by
	 * News instance.
	 *
	 * @param author
	 *            element of News instance to search for Author instance
	 * @return List of comments which was found by the News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 * @see java.sql.SQLException
	 */
	@SuppressWarnings("unchecked")
	public List<Comment> searchCommentsByNews(Long newsId) throws DaoException {
		Session session = null;
		List<Comment> commentList;
		
		try {
			session = sessionUtil.getSessionFactory().openSession();	
			
			session.beginTransaction();	
			Query query = session.createQuery(HQL_SEARCH_COMMENTS_BY_NEWS_QUERY);
			query.setParameter(NEWS_ID_PARAMETER, newsId);
			commentList = query.list();
			session.getTransaction().commit();
			
			return commentList;
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during searching comments by news. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override method used to delete data from table COMMENTS 
	 * when news is deleting
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	public void deleteAllCommentsForNews(Long newsId) throws DaoException {
		Session session = null;
		List<Comment> commentList;
		
		try {
			session = sessionUtil.getSessionFactory().openSession();	
			
			commentList = searchCommentsByNews(newsId);
			
			session.beginTransaction();	
			Query query = session.createQuery(HQL_DELETE_QUERY);
			for(Comment comment : commentList){
				query.setParameter(COMMENT_ID_PARAMETER, comment.getCommentId());
				query.executeUpdate();
			}
			session.getTransaction().commit();
			
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during searching comments by news. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

}
