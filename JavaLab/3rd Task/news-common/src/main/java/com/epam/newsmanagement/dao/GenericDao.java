package com.epam.newsmanagement.dao;


import java.io.Serializable;
import java.util.List;

import com.epam.newsmanagement.exception.DaoException;

/**
 * Generic interface <code>GenericDao</code> <br>
 * The top of the inheritance hierarchy of interfaces Data Access layer contains
 * a set of four standard operations for working with data base common to all
 * DAO implementations.
 *
 * @param <Entity>
 *            the type of elements in this interface
 * @author Kiryl Sprynchan
 * @version 1.0
 * @since 1.0
 */
public interface GenericDao<Entity extends Serializable> {

	/**
	 * Adding record in a database.
	 *
	 * @param entity
	 *            record to be added
	 * @return inserted entityID
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	Long create(Entity entity) throws DaoException;
	
	
	/**
	 * Reading record in a database.
	 *
	 * @param entityId
	 *            unique identifier of a record to read
	 * @return entity record to be read
	 * @throws com.epam.newsmanagement.exception.DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	Entity read(Long entityId) throws DaoException;
	
	/**
	 * Reading all records in a database.
	 *
	 * @return list of entity records to read
	 * @throws com.epam.newsmanagement.exception.DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	List<Entity> readAll() throws DaoException;
	
	/**
	 * Updating record in a database.
	 *
	 * @param entity
	 *            record to updated
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	void update(Entity entity) throws DaoException;
	
	/**
	 * Defining the operation of deleting record from a database.
	 *
	 * @param entityId
	 *            unique identifier of a record to deleted
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	void delete(Long entityId) throws DaoException;
}
