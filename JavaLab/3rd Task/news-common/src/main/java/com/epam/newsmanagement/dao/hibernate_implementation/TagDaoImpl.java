package com.epam.newsmanagement.dao.hibernate_implementation;

import java.util.List;

import com.epam.newsmanagement.domain.Author;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.utils.HibernateSessionUtil;

/**
 * Public class <code>TagDaoImpl</code> is an element of Data Access layer and
 * working with Tag data base instance. This is a single ton realization of
 * <code>TagDao</code> interface and it performs all operations described in
 * this interface. Realized pattern Singleton.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.dao.TagDao
 * @see com.epam.newsmanagement.domain.Tag
 * @since 1.0
 */
public class TagDaoImpl implements TagDao {

	/**
	 * Object which used for connecting to the database
	 */
	@Autowired
	private HibernateSessionUtil sessionUtil;
	
	/**
	 * Parameter for query newsId
	 */
	private static final String NEWS_ID_PARAMETER = "newsId";
	
	
	/**
	 * Parameter for query tagId
	 */
	private static final String TAG_ID_PARAMETER = "tagId";
	
	/**
	 * HQL query for searching author by news
	 */
	private static final String HQL_SEARCH_TAGS_BY_NEWS_QUERY = "select tag from Tag tag "
			+ "join tag.newsSet news where news.newsId = :newsId";
	
	
	/**
	 * HQL query for searching news by tag
	 */
	private static final String HQL_SEARCH_NEWS_BY_TAG_QUERY = "select news from News news "
			+ "join news.tagSet tag where tag.tagId = :tagId";
	
	/**
	 * Override public method used to add record to the table Tag.
	 *
	 * @param tag
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see org.hibernate.HibernateException
	 */
	public Long create(Tag tag) throws DaoException {
		Session session = null;
		
		try{
			session = sessionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			Long tagId = (Long) session.save(tag);
			session.getTransaction().commit();
			
			return tagId;
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during creating tag. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override method used to read record from the table Tag.
	 *
	 * @param tagId
	 *            unique identifier of instance which will be found
	 * @return find Tag instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see org.hibernate.HibernateException
	 */
	public Tag read(Long tagId) throws DaoException {
		Session session = null;
		
		try{
			session = sessionUtil.getSessionFactory().openSession();
			Tag tag = (Tag) session.get(Tag.class, tagId);
			
			return tag;
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during reading tag by id. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override method used to read all records from the table Tag.
	 *
	 * @return list of find Tags instances
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see org.hibernate.HibernateException
	 */
	@SuppressWarnings("unchecked")
	public List<Tag> readAll() throws DaoException {
		Session session = null;
		List<Tag> tagList = null;

		try {
			session = sessionUtil.getSessionFactory().openSession();
			tagList = session.createCriteria(Tag.class).list();
			
			return tagList;
		} catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during reading all tags. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override method used to update record in the table Tag.
	 *
	 * @param tag
	 *            instance which will be update in the data base
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see org.hibernate.HibernateException
	 */
	public void update(Tag tag) throws DaoException {
		Session session = null;
		
		try{
			session = sessionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(tag);
			session.getTransaction().commit();
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during updating tag. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override method used to delete record from the table Tag.
	 *
	 * @param tagId
	 *            unique identifier of instance which will be delete
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see org.hibernate.HibernateException
	 */
	public void delete(Long tagId) throws DaoException {
		Session session = null;

		try {
			session = sessionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			Tag tag = (Tag) session.get(Tag.class, tagId);
			session.delete(tag);
			session.getTransaction().commit();
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during deleting author. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		} 
	}

	@Override
	public List<Tag> searchTagsByAuthor(Author author) throws DaoException {
		return null;
//		TODO
	}

	/**
	 * Override method used to delete data from table NEWS_TAG when news is
	 * deleting
	 *
	 * @param tagId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	@SuppressWarnings("unchecked")
	public void deleteNewsTagByTag(Long tagId) throws DaoException {
		Session session = null;

		try {
			session = sessionUtil.getSessionFactory().openSession();	
			
			session.beginTransaction();	
			Query query = session.createQuery(HQL_SEARCH_NEWS_BY_TAG_QUERY);
			query.setParameter(TAG_ID_PARAMETER, tagId);
			List<News> newsList = query.list();
			Tag tag = (Tag) session.get(Tag.class, tagId);
			for(News news : newsList){		
				news.getTagSet().remove(tag);
			}
			
			session.getTransaction().commit();
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during expiring author. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}
	
	/**
	 * Override method used to delete data from table NEWS_TAG when news is
	 * deleting
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	public void deleteNewsTagByNews(Long newsId) throws DaoException {
		Session session = null;

		try {		
			session = sessionUtil.getSessionFactory().openSession();	
			
			session.beginTransaction();	
			List<Tag> tagList = searchTagsByNews(newsId);
			News news = (News) session.get(News.class, newsId);
			for(Tag tag : tagList){		
				tag.getNewsSet().remove(news);
			}
			
			session.getTransaction().commit();
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during expiring author. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override method used to search list of Tags record from the table Tag by
	 * News instance.
	 *
	 * @param newsId
	 *            element of Long instance to search for Tag list
	 * @return list of Tag which was find by the Author instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>HibernateException</code> throw up
	 * @see org.hibernate.HibernateException
	 */
	@SuppressWarnings("unchecked")
	public List<Tag> searchTagsByNews(Long newsId) throws DaoException {
		Session session = null;
		List<Tag> tagList;
		
		try {
			session = sessionUtil.getSessionFactory().openSession();	
			
			session.beginTransaction();	
			Query query = session.createQuery(HQL_SEARCH_TAGS_BY_NEWS_QUERY);
			query.setParameter(NEWS_ID_PARAMETER, newsId);
			tagList =  query.list();
			session.getTransaction().commit();
			
			return tagList;
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during searching Tags by news. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		} 
	}

}
