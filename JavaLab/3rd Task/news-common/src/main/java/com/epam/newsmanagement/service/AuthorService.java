package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * The second level of the hierarchy Service layer interfaces. This interface
 * <code>AuthorService</code> describes the behavior of a particular service
 * layer which working with instance of <code>Author</code> using Data Access
 * layer implementations of interface <code>AuthorDao</code>.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.service.GenericService
 * @see com.epam.newsmanagement.dao.AuthorDao
 * @see com.epam.newsmanagement.domain.Author
 * @since 1.0
 */
public interface AuthorService extends GenericService<Author> {
	
	/**
	 * Defining search operation from Author table in data base by sent News
	 * instance, using the searchTagsByNews() method in TagDao interface.
	 *
	 * @param news
	 *            element of News instance to search for Tag list
	 * @return list of Tag which was find by the Author instance
	 * @@throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.ServiceException
	 */
	public Author searchAuthorByNews(News news) throws ServiceException;
	
	/**
	 * Defining delete operation from Author table in data base by sent Long
	 * instance, using the deleteNewsAuthor(Long newsId) method in TagDao interface.
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @@throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.ServiceException
	 */
	public void deleteNewsAuthor(Long newsId) throws ServiceException;
	
	/**
	 * Defining making author unexpired operation
	 *
	 * @param authorId
	 *            element of Long instance to delete necessary rows
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.domain.Author
	 */
	public void unexpireAuthor(Long authorId) throws ServiceException;

}
