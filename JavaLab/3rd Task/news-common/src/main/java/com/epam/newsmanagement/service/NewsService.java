package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * The second level of the hierarchy Service layer interfaces. This interface
 * <code>NewsService</code> describes the behavior of a particular service layer
 * which working with instance of <code>News</code> using Data Access layer
 * implementations of interface <code>NewsDao</code>.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.service.GenericService
 * @see com.epam.newsmanagement.dao.NewsDao
 * @see com.epam.newsmanagement.domain.News
 * @since 1.0
 */
public interface NewsService extends GenericService<News>{

	/**
	 * Get list News instances from News table in data base , 
	 * using the getSortedNews() method in NewsDao interface.
	 *
	 * @return list of News which are sorted by number of comments and modification date
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public List<News> getSortedNews(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * Binding the News instance with the Author instance in the table
	 * NewsAuthor in data base, using the bindingNewsAuthor() method in NewsDao
	 * interface.
	 *
	 * @param news
	 *            News instance for binding with Author instance
	 * @param author
	 *            Author instance for binding with News instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public void saveNewsAuthor(News news, Author author) throws ServiceException;
	
	/**
	 * Binding the News instance with the list of Tag instances in the table
	 * NewsTag in data base, using the createTagForNews() method in NewsDao
	 * interface.
	 *
	 * @param news
	 *            News instance for binding with Tag instance
	 * @param tag
	 *            Tag instance for binding with News instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public void saveNewsTagList(News news, List<Tag> tagList) throws ServiceException;
	
	/**
	 * Get list News instances from News table in data base by sent SearchCriteria instance, 
	 * using the readAllNews(SearchCriteria searchCriteria) method in NewsDao interface.
	 *
	 * @return list of News which are sorted by number of comments and modification date
	 * @param searchCriteria
	 *            SearchCriteria instance for binding with other instances
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public List<News> readNewsByPageNumber(int pageNumber, int newsOnPageCount, SearchCriteria searchCriteria)
			throws ServiceException;
	
	/**
	 * Checking is there next news after current
	 * according to searching criteria
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @param searchCriteria
	 *            element of SearchCriteria instance            
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	public Long isNext(Long newsId, SearchCriteria searchCriteria)throws ServiceException;
	
	/**
	 * Checking is there prevoius news after current
	 * according to searching criteria
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @param searchCriteria
	 *            element of SearchCriteria instance            
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	public Long isPrevious(Long newsId, SearchCriteria searchCriteria)throws ServiceException;
	
	/**
	 * Updating NEWS_AUTHOR table after editing news action.
	 *
	 * @param news
	 *            element of News instance to delete necessary rows
	 * @param author
	 *            element of Author instance to delete necessary rows           
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	public void updateNewsAuthor(News news,Author author) throws ServiceException;
	
	/**
	 * Defining delete operation from NEWS_TAG table in data base by sent Long
	 * instance, using the deleteNewsTag(Long newsId) method in TagDao interface.
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @@throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public void deleteNewsTag(Long newsId) throws ServiceException;
}
