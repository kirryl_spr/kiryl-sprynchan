package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.domain.PostedNews;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * General interface <code>NewsManagementService</code> describing transactional
 * operations for which, you must use a few simple Service layer interfaces.
 *
 *
 * The main interface in all Service layer interfaces. This interface
 * <code>NewsManagementService</code> describing transactional operations for
 * which, you must use a few simple Service layer interfaces implementations.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @since 1.0
 */
public interface NewsManagementService {

	/**
	 * Save news with list of binding tags and author in one step. This is a
	 * transactional operation.
	 *
	 * @param postedNews
	 *            PostedNews instance for saving in database
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public Long saveNews(PostedNews postedNews) throws ServiceException;
	
	/**
	 * Update news with list of binding tags and author in one step. This is a
	 * transactional operation.
	 *
	 * @param postedNews
	 *            PostedNews instance for saving in database
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public void updateNews(PostedNews postedNews) throws ServiceException;
	
	/**
	 * Delete news with list of binding tags, author and comments in one step. This is a
	 * transactional operation.
	 *
	 * @param List of long instances which are ID of news
	 *            list of Tag instances for saving in database
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public void deleteNews(Long newsId) throws ServiceException;
	
	/**
	 * Reading news on current page according to search criteria
	 *
	 * @param pageNumber of int what is describing number of a current page
	 * 
	 * @param newsOnPageCount of int describing quantity of news in all
	 * @param searchCriteria of SearchCriteria instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public List<PostedNews> readNewsOnPage(int pageNumber, int newsOnPageCount, SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * Reading news single news
	 *
	 * @param newsId of Long instance what is describing ID of news
	 * 
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public PostedNews readSingleNews(Long newsId) throws ServiceException;
	
}
