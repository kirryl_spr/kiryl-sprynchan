package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DaoException;

/**
 * The second level of the hierarchy Data Access layer interfaces. This
 * interface <code>AuthorDao</code> describes the behavior of a particular dao
 * layer which working with instance of <code>Author</code>.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.dao.GenericDao
 * @see com.epam.newsmanagement.domain.Author
 * @since 1.0
 */
public interface AuthorDao extends GenericDao<Author>{

	/**
	 * Search Author instance from Author table in data base by sent News
	 * instance.
	 *
	 * @param news
	 *            element of News instance to search for Author instance
	 * @return Author which was find by the News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	Author searchAuthorByNews(News news) throws DaoException;

	/**
	 * Delete data from table NEWS_AUTHOR when news is deleting
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	void deleteNewsAuthor(Long newsId) throws DaoException;
	
	/**
	 * Making author unexpired
	 *
	 * @param authorId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.Author
	 */
	void unexpireAuthor(Long authorId) throws DaoException;
}
