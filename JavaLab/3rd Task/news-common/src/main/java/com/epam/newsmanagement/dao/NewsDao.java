package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * The second level of the hierarchy Data Access layer interfaces. This
 * interface <code>NewsDao</code> describes the behavior of a particular dao
 * layer which working with instance of <code>News</code>.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.dao.GenericDao
 * @see com.epam.newsmanagement.domain.News
 * @since 1.0
 */
public interface NewsDao extends GenericDao<News>{

	/**
	 * Searching News instance from table News in data base by sent Author
	 * instance.
	 *
	 * @param author
	 *            element of Author instance to search for News list
	 * @return list of News which was find by the Author instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsmanagement.domain.Author
	 */
	List<News> searchNewsByAuthor(Author author) throws DaoException;

	/**
	 * Searching News instance from table News in data base by sent Tag
	 * instance.
	 *
	 * @param tag
	 *            element of Tag instance to search for news
	 * @return list of News which was found by the Tag instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsmanagement.domain.Tag
	 */
	List<News> searchNewsByTag(Tag tag) throws DaoException;

	/**
	 * Sort News instances from table News from database by count of comments.
	 *
	 * @return sorted list of News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsmanagement.domain.News
	 */
	List<News> getSortedNews(SearchCriteria searchCriteria) throws DaoException;

	/**
	 * Saving News instance with the Author instance in the table NewsAuthor
	 * in data base.
	 *
	 * @param newsId
	 *            News instance for binding with Author instance
	 * @param authorId
	 *            Author instance for binding with News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsmanagement.domain.News
	 * @see com.epam.newsmanagement.domain.Author
	 */
	void saveNewsAuthor(Long newsId, Long authorId) throws DaoException;

	/**
	 * Saving News instance with the list of Tag instances in the table NewsTag
	 * in data base.
	 *
	 * @param newsId
	 *            News instance for binding with Author instance
	 * @param tagId
	 *            Author instance for binding with News instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsmanagement.domain.News
	 * @see com.epam.newsmanagement.domain.Author
	 */
	void saveNewsTag(Long newsId, Long tagId) throws DaoException;

	/**
	 * Reading News instance from table News in data base by sent SeaerchCriteria
	 * instance.
	 *
	 * @param searchCriteria
	 *            element of SearchCriteria instance to search for users` criteria
	 * @return list of News which was found by the SearchCriteria instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsmanagement.domain.SearchCriteria
	 */
	List<News> readNewsByPageNumber(int pageNumber, int newsOnPageCount, SearchCriteria searchCriteria) throws DaoException;

	/**
	 * Checking is there next news after current
	 * according to searching criteria
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @param searchCriteria
	 *            element of SearchCriteria instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	Long isNext(Long newsId, SearchCriteria searchCriteria)throws DaoException;

	/**
	 * Checking is there previous news after current
	 * according to searching criteria
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @param searchCriteria
	 *            element of SearchCriteria instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	Long isPrevious(Long newsId, SearchCriteria searchCriteria)throws DaoException;

	/**
	 * Updating NEWS_AUTHOR table after editing news action.
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @param authorId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	void updateNewsAuthor(Long newsId, Long authorId) throws DaoException;

	/**
	 * Delete data from table NEWS_TAG when news is deleting
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	void deleteNewsTag(Long newsId) throws DaoException;
}
