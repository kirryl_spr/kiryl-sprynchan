package com.epam.newsmanagement.dao.hibernate_implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Tag;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.utils.DataBaseUtil;
import com.epam.newsmanagement.utils.HibernateSessionUtil;
import com.epam.newsmanagement.utils.QueryBuilderUtil;

/**
 * Public class <code>NewsDaoImpl</code> is an element of Data Access layer and
 * working with News data base instance. This is a single ton realization of
 * <code>NewsDao</code> interface and it performs all operations described in
 * this interface. Realized pattern Singleton.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.dao.NewsDao
 * @see com.epam.newsmanagement.domain.News
 * @since 1.0
 */
public class NewsDaoImpl implements NewsDao {

	/**
	 * Object which used for connecting to the database
	 */
	@Autowired
	private DataBaseUtil dataBaseUtil;

	/**
	 * Object which used for connecting to the database
	 */
	@Autowired
	private HibernateSessionUtil sessionUtil;

	private static final String TAG_ID_PARAMETER = "tagId";




	/**
	 * SQL query for search news by author
	 */
	private static final String SQL_SEARCH_BY_AUTHOR_QUERY = "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS "
			+ "JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID "
			+ "JOIN (SELECT NEWS_ID AS N_ID FROM COMMENTS GROUP BY NEWS_ID"
			+ " ORDER BY COUNT(*) DESC) ON NEWS.NEWS_ID = N_ID WHERE (AUTHOR_ID = ?) ORDER BY MODIFICATION_DATE";

	/**
	 * SQL query for search news by tag
	 */
	private static final String HQL_SEARCH_NEWS_BY_TAG_QUERY = "select news from News news "
			+ "join news.tagSet tag where tag.tagId = :tagId";

	/**
	 * SQL query for delete association news with tags
	 */
	private static final String SQL_DELETE_NEWS_TAG_QUERY = "DELETE FROM NEWS_TAG WHERE NEWS_ID =?";
	
	/**
	 * SQL query for saving news with author
	 */
	private static final String SQL_SAVE_NEWS_AUTHOR_QUERY = "INSERT INTO NEWS_AUTHOR (NEWS_AUTHOR.NEWS_ID, NEWS_AUTHOR.AUTHOR_ID) VALUES(?, ?)";

	
	/**
	 * SQL query for updating news with author
	 */
	private static final String SQL_UPDATE_NEWS_AUTHOR_QUERY = "UPDATE NEWS_AUTHOR SET AUTHOR_ID = ? WHERE NEWS_ID = ?";

	/**
	 * SQL query for saving news with tag
	 */
	private static final String SQL_SAVE_NEWS_TAG_QUERY = "INSERT INTO NEWS_TAG (NEWS_TAG.NEWS_ID, NEWS_TAG.TAG_ID) VALUES(?, ?)";

	/**
	 * Variable for setting format of current date and time
	 */
	private static final String GMT_FORMAT = "GMT";
	/**
	 * Override method used to add record to the table News.
	 *
	 * @param news
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public Long create(News news) throws DaoException {
		Session session = null;
		
		try{
			session = sessionUtil.getSessionFactory().openSession();
			
			session.beginTransaction();
			Long newsId = (Long) session.save(news);
			session.getTransaction().commit();
			
			return newsId;
		} catch(HibernateException ex){
			throw new DaoException("HibernateException caught during creating news. See details: ", ex);
		}finally{
			sessionUtil.closeSession(session);
		}

	}

	/**
	 * Override method used to read record from the table News.
	 *
	 * @param newsId
	 *            unique identifier of instance which will be found
	 * @return find News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public News read(Long newsId) throws DaoException {
		Session session = null;
		
		try{
			session = sessionUtil.getSessionFactory().openSession();
			News news = (News) session.get(News.class, newsId);
			
			return news;
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during reading news by id. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override method used to read all records from the table News.
	 *
	 * @return list of find News instances
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	@SuppressWarnings("unchecked")
	public List<News> readAll() throws DaoException {
		Session session = null;
		List<News> newsList = null;

		try {
			session = sessionUtil.getSessionFactory().openSession();
			newsList = session.createCriteria(News.class).list();
			
			return newsList;
		} catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during reading all news. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override method used to update record in the table News.
	 *
	 * @param news
	 *            instance which will be update in the data base
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void update(News news) throws DaoException {
		Session session = null;
		
		try{
			session = sessionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(news);
			session.getTransaction().commit();
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during updating news. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override method used to delete record from the table News.
	 *
	 * @param newsId
	 *            unique identifier of instance which will be delete
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void delete(Long newsId) throws DaoException {
		Session session = null;

		try {
			session = sessionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			News news = (News) session.get(News.class, newsId);
			session.delete(news);
			session.getTransaction().commit();
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during deleting news. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		} 
	}

	@Override
	public List<News> searchNewsByAuthor(Author author) throws DaoException {

		//TODO
		List<News> newsList = null;
		return newsList;
	}

	@Override
	public List<News> searchNewsByTag(Tag tag) throws DaoException {
		//TODO
		List<News> newsList = null;
		return newsList;
	}

	/**
	 * Override method used to get sorted, by comments count and modification
	 * date, list of records from the table News.
	 *
	 * @return sorted list of News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public List<News> getSortedNews(SearchCriteria searchCriteria)
			throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		List<News> newsList = null;
		News news = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(QueryBuilderUtil.BuilderWithoutPageOrder
					.buildQuery(searchCriteria));

			resultSet = preparedStatement.executeQuery();

			newsList = new ArrayList<News>();
			while (resultSet.next()) {
				news = new News();

				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5, Calendar.getInstance(TimeZone.getTimeZone(GMT_FORMAT))));
				news.setModificationDate(resultSet.getDate(6));

				newsList.add(news);
			}

			return newsList;

		} catch (SQLException e) {
			throw new DaoException("SQL error in sort 'news' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}


	/**
	 * Override method used to save Author instance with instance of News, and
	 * add same record to the NewsAuthor table in data base.
	 *
	 * @param newsId
	 *            News instance for binding with Author instance
	 * @param authorId
	 *            Author instance for binding with News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void saveNewsAuthor(Long newsId, Long authorId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_SAVE_NEWS_AUTHOR_QUERY);

			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in saving news-author operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to save list of Tag instances with instance of News,
	 * and add same record to the NewsTag table in data base.
	 *
	 * @param newsId
	 *            News instance for binding with Tag instance
	 * @param tagId
	 *            List of tag instance for binding with News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void saveNewsTag(Long newsId, Long tagId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();

			preparedStatement = connection
					.prepareStatement(SQL_SAVE_NEWS_TAG_QUERY);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, tagId);

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in saving news-tag operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}

	}

	/**
	 * Reading News instance from table News in data base by sent SeaerchCriteria
	 * instance.
	 *
	 * @param searchCriteria
	 *            element of SearchCriteria instance to search for users` criteria
	 * @return list of News which was found by the SearchCriteria instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsmanagement.domain.SearchCriteria
	 */
	public List<News> readNewsByPageNumber(int lowBorder, int heightBorder,
			SearchCriteria searchCriteria) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		List<News> newsList = new ArrayList<News>();

		try {
			connection = dataBaseUtil.getConnection();

			preparedStatement = connection.prepareStatement(QueryBuilderUtil.BuilderByPageOrder
					.buildQuery(searchCriteria));
			preparedStatement.setLong(1, lowBorder);
			preparedStatement.setLong(2, heightBorder);
			resultSet = preparedStatement.executeQuery();

			News news = null;
			while (resultSet.next()) {
				news = new News();

				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5, Calendar.getInstance(TimeZone.getTimeZone(GMT_FORMAT))));
				news.setModificationDate(resultSet.getDate(6));

				newsList.add(news);

			}
		} catch (SQLException e) {
			throw new DaoException(
					"SQL error while reading news by page number!?!?!?!?", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}

		return newsList;
	}

	/**
	 * Checking is there next news after current
	 * according to searching criteria
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @param searchCriteria
	 *            element of SearchCriteria instance            
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	@Override
	public Long isNext(Long newsId, SearchCriteria searchCriteria) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Long nextNewsId = null;
		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection
					.prepareStatement(QueryBuilderUtil.BuilderForCheckingNextNews.buildQuery(searchCriteria));
			preparedStatement.setLong(1, newsId);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				nextNewsId = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error while reading next news", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
		return nextNewsId;
	}

	/**
	 * Checking is there previous news after current
	 * according to searching criteria
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @param searchCriteria
	 *            element of SearchCriteria instance            
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	@Override
	public Long isPrevious(Long newsId, SearchCriteria searchCriteria) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Long previousNewsId = null;
		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection
					.prepareStatement(QueryBuilderUtil.BuilderForCheckingPreviousNews.buildQuery(searchCriteria));
			preparedStatement.setLong(1, newsId);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				previousNewsId = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error while reading next news", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
		return previousNewsId;
	}

	/**
	 * Updating NEWS_AUTHOR table after editing news action.
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @param authorId
	 *            element of Long instance to delete necessary rows           
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	@Override
	public void updateNewsAuthor(Long newsId, Long authorId)
			throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_UPDATE_NEWS_AUTHOR_QUERY);

			preparedStatement.setLong(1,authorId );
			preparedStatement.setLong(2, newsId);

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in updating news-author operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
		
	}

	/**
	 * Override method used to delete data from table NEWS_TAG when news is
	 * deleting
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	public void deleteNewsTag(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_NEWS_TAG_QUERY);

			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in delete 'news_tag' operation.",
					e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}

	}
}
