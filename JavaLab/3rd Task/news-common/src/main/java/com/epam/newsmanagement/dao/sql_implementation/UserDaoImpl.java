package com.epam.newsmanagement.dao.sql_implementation;

import com.epam.newsmanagement.dao.UserDao;
import com.epam.newsmanagement.domain.User;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.utils.DataBaseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Realization of using daoMethods for working with USERS table in database
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.domain.User
 * @since 1.0
 */
public class UserDaoImpl implements UserDao {

	/**
	 * Object which used for connecting to the database
	 */
	@Autowired
	private DataBaseUtil dataBaseUtil;

	/**
	 * Sql query for reading users from database by login.
	 */
	private String SQL_READ_USER = "SELECT USER_ID, USER_NAME, LOGIN, PASSWORD FROM USERS WHERE LOGIN = ?";


	/**
	 * Reading user from database by login value.
	 *
	 * @return list of entity records to read
	 * @throws com.epam.newsmanagement.exception.DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see SQLException
	 */
	@Override
	public User readUserByLogin(String login) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		User user = new User();
		try{
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ_USER);
			
			preparedStatement.setString(1, login);
			
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				user.setUserId(resultSet.getLong(1));
				user.setUserName(resultSet.getString(2));
				user.setLogin(resultSet.getString(3));
				user.setPassword(resultSet.getString(4));
			}
			return user;
		}catch(SQLException e){
			throw new DaoException("Cannot read user", e);
		}
	}

}
