package com.epam.newsmanagement.dao.hibernate_implementation;


import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.utils.HibernateSessionUtil;

/**
 * Public class <code>AuthorDaoImpl</code> is an element of Data Access layer
 * and working with Author data base instance. This is a singleton realization
 * of <code>AuthorDao</code> interface and it performs all operations described
 * in this interface. Realized pattern Singleton.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.dao.CommentDao
 * @see com.epam.newsmanagement.domain.Author
 * @since 1.0
 */
public class AuthorDaoImpl implements AuthorDao{
	
	/**
	 * Object which used for connecting to the database
	 */
	@Autowired
	private HibernateSessionUtil sessionUtil;
	
	/**
	 * Parameter for query expiration date
	 */
	private static final String EXPIRATION_DATE_PARAMETER = "expirationDate";
	
	/**
	 * Parameter for query authorId
	 */
	private static final String AUTHOR_ID_PARAMETER = "authorId";
	
	/**
	 * Parameter for query newsId
	 */
	private static final String NEWS_ID_PARAMETER = "newsId";
	/**
	 * HQL query for expiration author
	 */
	private static final String HQL_EXPIRATION_QUERY = "UPDATE Author set expired = :expirationDate WHERE authorId = :authorId";

	/**
	 * HQL query for searching author by news
	 */
	private static final String HQL_SEARCH_AUTHOR_BY_NEWS_QUERY = "select author from Author author "
			+ "join author.newsSet news where news.newsId = :newsId";

	/**
	 * Override public method used to add record to the table Author.
	 *
	 * @param author
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see org.hibernate.HibernateException
	 */
	public Long create(Author author) throws DaoException {
		Session session = null;
		
		try{
			session = sessionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			Long authorId = (Long) session.save(author);
			session.getTransaction().commit();
			
			return authorId;
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during creating author. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	
	/**
	 * Override public method used to read a record from the table Author.
	 *
	 * @param authorId
	 *            unique identifier of instance which will be found
	 * @return find Author instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see org.hibernate.HibernateException
	 */
	public Author read(Long authorId) throws DaoException{
		Session session = null;
		
		try{
			session = sessionUtil.getSessionFactory().openSession();
			Author author = (Author) session.get(Author.class, authorId);
			
			return author;
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during reading author by id. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override public method used to read all records from the table Author.
	 *
	 * @return list of find Author instances
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see org.hibernate.HibernateException
	 */
	@SuppressWarnings("unchecked")
	public List<Author> readAll() throws DaoException {
		Session session = null;
		List<Author> authorList = null;

		try {
			session = sessionUtil.getSessionFactory().openSession();
			authorList = session.createCriteria(Author.class).list();
			
			return authorList;
		} catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during reading all authors. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override public method used to update a record in the table Author.
	 *
	 * @param author
	 *            instance which will be update in the data base
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see org.hibernate.HibernateException
	 */
	public void update(Author author) throws DaoException {
		Session session = null;
		
		try{
			session = sessionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(author);
			session.getTransaction().commit();
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during updating author. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Override public method used to delete a record from the table Author.
	 *
	 * @param authorId
	 *            unique identifier of instance which will be delete
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see org.hibernate.HibernateException
	 */
	public void delete(Long authorId) throws DaoException {
		Session session = null;
		
		
		try {
			session = sessionUtil.getSessionFactory().openSession();	
			
			session.beginTransaction();
			
			Query query = session.createQuery(HQL_EXPIRATION_QUERY);
			query.setParameter(EXPIRATION_DATE_PARAMETER, new Date());
			query.setParameter(AUTHOR_ID_PARAMETER, authorId);
			query.executeUpdate();
			
			session.getTransaction().commit();
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during expiring author. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		} 
	}

	/**
	 * Override method used to search Author record from the table Author by
	 * News instance.
	 *
	 * @param news
	 *            element of News instance to search for Author instance
	 * @return Author which was find by the News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 * @see org.hibernate.HibernateException
	 */
	public Author searchAuthorByNews(News news) throws DaoException {
		Session session = null;
		
		try {
			session = sessionUtil.getSessionFactory().openSession();	
			
			session.beginTransaction();	
			Query query = session.createQuery(HQL_SEARCH_AUTHOR_BY_NEWS_QUERY);
			query.setParameter(NEWS_ID_PARAMETER, news.getNewsId());
			Author author = (Author) query.uniqueResult();
			session.getTransaction().commit();
			
			return author;
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during searching author by news. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		} 
	}
	
	/**
	 * Override method used to delete data from table NEWS_AUTHOR 
	 * when news is deleting
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	@SuppressWarnings("unchecked")
	public void deleteNewsAuthor(Long newsId) throws DaoException {
		Session session = null;
		
		try {
			session = sessionUtil.getSessionFactory().openSession();	
			
			session.beginTransaction();	
			Query query = session.createQuery(HQL_SEARCH_AUTHOR_BY_NEWS_QUERY);
			query.setParameter(NEWS_ID_PARAMETER, newsId);
			List<Author> authorList = query.list();
			News news = (News) session.get(News.class, newsId);
			for(Author author : authorList){		
				author.getNewsSet().remove(news);
			}
			
			session.getTransaction().commit();
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught deleting news_author record. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		}
	}

	/**
	 * Making author unexpired
	 *
	 * @param authorId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>HibernateException</code> throw up
	 * @see com.epam.newsmanagement.domain.Author
	 */
	@Override
	public void unexpireAuthor(Long authorId) throws DaoException {
		Session session = null;
		
		try {
			session = sessionUtil.getSessionFactory().openSession();	
			
			session.beginTransaction();
			
			Query query = session.createQuery(HQL_EXPIRATION_QUERY);
			query.setParameter(EXPIRATION_DATE_PARAMETER, null);
			query.setParameter(AUTHOR_ID_PARAMETER, authorId);	
			query.executeUpdate();
			
			session.getTransaction().commit();
		}catch (HibernateException ex) {
			throw new DaoException("HibernateException caught during unexpireing author. See details: ", ex);
		} finally {
			sessionUtil.closeSession(session);
		} 
		
	}

}
