package com.epam.newsmanagement.dao.sql_implementation;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.utils.DataBaseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Public class <code>CommentDaoImpl</code> is an element of Data Access layer
 * and working with Comment data base instance. This is a single ton realization
 * of <code>CommentDao</code> interface and it performs all operations described
 * in this interface. Realized pattern Singleton.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsprotal.dao.CommentDao
 * @see com.epam.newsprotal.domain.Comment
 * @since 1.0
 */
public class CommentDaoImpl implements CommentDao {
	
	/**
	 * Object which used for connecting to the database
	 */
	@Autowired
	 private DataBaseUtil dataBaseUtil;

	/**
	 * SQL query for create comment
	 */
	private static final String SQL_CREATE_QUERY = "INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) "
			+ "VALUES(COMMENT_ID_SEQ.NEXTVAL, ?, ?, ?)";
	
	/**
	 * SQL query for read comment by id
	 */
	private static final String SQL_READ_QUERY = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID FROM COMMENTS WHERE "
			+ "COMMENT_ID = ?";
	
	/**
	 * SQL query for readAll comment records
	 */
	private static final String SQL_READ_ALL_QUERY = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID FROM COMMENTS "
			+ "ORDER BY COMMENT_ID";
	
	/**
	 * SQL query for update comment
	 */
	private static final String SQL_UPDATE_QUERY = "UPDATE COMMENTS SET COMMENT_TEXT = ?, CREATION_DATE = ?, NEWS_ID = ? "
			+ "WHERE COMMENT_ID = ?";
	
	/**
	 * SQL query for delete comment
	 */
	private static final String SQL_DELETE_QUERY = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
	
	/**
	 * SQL query for delete all comments for news
	 */
	private static final String SQL_DELETE_ALL_COMMENTS_FOR_NEWS_QUERY = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
	
	/**
	 * SQL query for searching comment by news
	 */
	private static final String SQL_SEARCH_BY_NEWS_QUERY = "SELECT  COMMENT_ID, COMMENT_TEXT, "
			+ "CREATION_DATE, NEWS_ID FROM COMMENTS WHERE NEWS_ID = ?";
	
	
	/**
	 * Override public method used to add record to the table Comment.
	 *
	 * @param comment
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see SQLException
	 */
	public Long create(Comment comment) throws DaoException {
		String columnCommentId = "COMMENT_ID";
		Long commentId = null;

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		String generatedColumns[] = { columnCommentId };
		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE_QUERY,
					generatedColumns);

			preparedStatement.setLong(1, comment.getNewsId());
			preparedStatement.setString(2, comment.getCommentText());
			preparedStatement.setTimestamp(3, new Timestamp(comment
					.getCreationDate().getTime()));


			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				commentId = resultSet.getLong(1);
			}

		} catch (SQLException e) {
			throw new DaoException("SQL error in create 'comment' operation.",
					e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}

		return commentId;
	}

	/**
	 * Override method used to read record from the table Comment.
	 *
	 * @param commentId
	 *            unique identifier of instance which will be found
	 * @return find Comment instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see SQLException
	 */
	public Comment read(Long commentId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Comment comment = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ_QUERY);

			preparedStatement.setLong(1, commentId);

			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				comment = new Comment();
				comment.setCommentId(resultSet.getLong(1));
				comment.setCommentText(resultSet.getString(2));
				comment.setCreationDate(resultSet.getTimestamp(3));
				comment.setNewsId(resultSet.getLong(4));
			}

			return comment;

		} catch (SQLException e) {
			throw new DaoException("SQL error in read 'comment' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to read all records from the table Comment.
	 *
	 * @return list of find Comment instances
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see SQLException
	 */
	public List<Comment> readAll() throws DaoException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		List<Comment> commentList = null;
		Comment comment = null;

		try {
			connection = dataBaseUtil.getConnection();
			statement = connection.createStatement();

			resultSet = statement.executeQuery(SQL_READ_ALL_QUERY);

			commentList = new ArrayList<Comment>();
			while (resultSet.next()) {
				comment = new Comment();

				comment.setCommentId(resultSet.getLong(1));
				comment.setCommentText(resultSet.getString(2));
				comment.setCreationDate(resultSet.getTimestamp(3));
				comment.setNewsId(resultSet.getLong(4));

				commentList.add(comment);
			}

			return commentList;

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in readlALl 'comments' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, statement, connection);
		}
	}

	/**
	 * Override method used to update record in the table Comment.
	 *
	 * @param comment
	 *            instance which will be update in the data base
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see SQLException
	 */
	public void update(Comment comment) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_QUERY);

			preparedStatement.setString(1, comment.getCommentText());
			preparedStatement.setTimestamp(2, new Timestamp(comment
					.getCreationDate().getTime()));
			preparedStatement.setLong(3, comment.getNewsId());
			preparedStatement.setLong(4, comment.getCommentId());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in update 'comment' operation.",
					e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to delete record from the table Comment.
	 *
	 * @param commentId
	 *            unique identifier of instance which will be delete
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see SQLException
	 */
	public void delete(Long commentId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_QUERY);

			preparedStatement.setLong(1, commentId);

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in delete 'comment' operation.",
					e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}

	}

	/**
	 * Override method used to search List of comments from the table Comments by
	 * News instance.
	 *
	 * @param author
	 *            element of News instance to search for Author instance
	 * @return List of comments which was found by the News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 * @see SQLException
	 */
	public List<Comment> searchCommentsByNews(News news) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Comment comment = null;
		List<Comment> commentList = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_SEARCH_BY_NEWS_QUERY);

			preparedStatement.setLong(1, news.getNewsId());

			resultSet = preparedStatement.executeQuery();

			commentList = new ArrayList<Comment>();
			while (resultSet.next()) {
				comment = new Comment();

				comment.setCommentId(resultSet.getLong(1));
				comment.setCommentText(resultSet.getString(2));
				comment.setCreationDate(resultSet.getDate(3));
				comment.setNewsId(resultSet.getLong(4));

				commentList.add(comment);
			}
			return commentList;

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in search 'comments' by news operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	@Override
	public List<Comment> searchCommentsByNews(Long newsId) throws DaoException {
		return null;
//		TODO
	}

	/**
	 * Override method used to delete data from table COMMENTS
	 * when news is deleting
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	public void deleteAllCommentsForNews(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_ALL_COMMENTS_FOR_NEWS_QUERY);

			preparedStatement.setLong(1, newsId);

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in delete 'comment' operation.",
					e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

}
