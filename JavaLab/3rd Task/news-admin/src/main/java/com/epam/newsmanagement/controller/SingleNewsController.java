package com.epam.newsmanagement.controller;


import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.PostedNews;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

import static com.epam.newsmanagement.util.ConstantPool.SEARCH_CRITERIA;
import static com.epam.newsmanagement.util.ConstantPool.CURRENT_NEWS;
import static com.epam.newsmanagement.util.ConstantPool.SINGLE_NEWS;
import static com.epam.newsmanagement.util.ConstantPool.NEXT_NEWS;
import static com.epam.newsmanagement.util.ConstantPool.PREVIOUS_NEWS;
import static com.epam.newsmanagement.util.ConstantPool.TIME_ZONE;


/**
 * Controller for show-single-news page. The controller is in charge of showing
 * selected news, saving new news comments, deleting news comments and moving
 * between news in the list.
 * 
 * @author Kiryl Sprynchan
 *
 */
@Controller
public class SingleNewsController {


	@Autowired
	private NewsManagementService newsManagerService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private TagService tagService;
	
	@Autowired
	private CommentService commentService;
	
	/**
	 * Show current news by id. Also set previous news and next news on the
	 * show-single-news page, if they exist.
	 * 
	 * @param newsId
	 *            current news id
	 * @return views-news page
	 * @throws ServiceException
	 */
	@RequestMapping("/show-single-news")
	public String showSingleNews(@RequestParam(required = false) Long newsId, @RequestParam(required = false) String timeZone, Model model,
			HttpSession session) throws ServiceException {
		
		if(newsId != null){
			session.setAttribute(CURRENT_NEWS, newsId);
		}
		else{
			newsId = (Long) session.getAttribute(CURRENT_NEWS);
		}
		
		
		SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(SEARCH_CRITERIA);
		
		Long nextNewsId = newsService.isNext(newsId, searchCriteria);
		Long previousNewsId = newsService.isPrevious(newsId, searchCriteria);
		
		PostedNews singleNews = newsManagerService.readSingleNews(newsId);	
		
		model.addAttribute(TIME_ZONE, timeZone);
		model.addAttribute(SINGLE_NEWS, singleNews);
		model.addAttribute(NEXT_NEWS, nextNewsId);
		model.addAttribute(PREVIOUS_NEWS, previousNewsId);
		return "show-single-news";
	}
	
	/**
	 * Add new news comment to the news entity.
	 * 
	 * @param commentText
	 *            new news comment text
	 * @param newsId
	 *            current news id
	 * @return show-single-news page
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/add-comment", method = RequestMethod.POST)
	public String addComment(@RequestParam Long newsId, @RequestParam String commentText, Model model,
			HttpSession session) throws ServiceException {
		Comment comment = new Comment();
		comment.setNewsId(newsId);
		comment.setCommentText(commentText);
		comment.setCreationDate(new Date());
		
		commentService.create(comment);
		
		PostedNews singleNews = newsManagerService.readSingleNews(newsId);
		model.addAttribute(SINGLE_NEWS, singleNews);
		return "redirect:/show-single-news";
	}
	
	/**
	 * Delete news comment, by comment id.
	 * 
	 * @param commentId
	 *            selected comment id
	 * @param newsId
	 *            current news id
	 * @return show-single-news page
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/delete-comment", method = RequestMethod.POST)
	public String deleteComment(@RequestParam Long newsId, @RequestParam Long commentId,  Model model,
			HttpSession session)  throws ServiceException{
		commentService.delete(commentId);
			
		PostedNews singleNews = newsManagerService.readSingleNews(newsId);
		model.addAttribute(SINGLE_NEWS, singleNews);
		return "redirect:/show-single-news";
	}
	
}
