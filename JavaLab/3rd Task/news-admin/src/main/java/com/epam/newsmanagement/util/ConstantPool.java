package com.epam.newsmanagement.util;
/**
 *	Class that contains all specified constants.
 * 
 * @author Kiryl Sprynchan
 */
public class ConstantPool {

    /**
	 * Quantity of news on page parameter.
	 */
    public static final int NEWS_COUNT_ON_PAGE = 3;
    
	/**
	 * Locale parameter.
	 */
    public static final String LOCALE = "locale";
    
    /**
     * Search Criteria parameter.
     */
    public static final String SEARCH_CRITERIA = "searchCriteria";
    
    /**
     * Number of current page parameter.
     */
    public static final String PAGE_NUMBER = "pageNumber";
    
    /**
     * Quantity of pages with news parameter.
     */
    public static final String PAGES_COUNT = "pagesCount";
    
    /**
     * Author lists parameter.
     */
    public static final String AUTHOR_LIST = "authorList";
    
    /**
     * Tag lists parameter.
     */
    public static final String TAG_LIST = "tagList";
    
    /**
     * Full news lists parameter.
     */
    public static final String FULL_NEWS_LIST = "fullNewsList";

    /**
     * Author parameter.
     */
    public static final String AUTHOR = "author";
    
    /**
     * News id parameter.
     */
    public static final String NEWS_ID = "newsId";
    
    /**
     * Comment text parameter.
     */
    public static final String COMMENT_TEXT = "commentText";
    
    /**
     * Single news parameter.
     */
    public static final String SINGLE_NEWS = "singleNews";
    
    /**
     * Next news parameter.
     */
    public static final String NEXT_NEWS = "nextNews";
    
    /**
     * Previous news parameter.
     */
    public static final String PREVIOUS_NEWS = "previousNews";
    
    /**
     * Previous news parameter.
     */
    public static final String CURRENT_NEWS = "currentNews";
    
    /**
     * Previous news parameter.
     */
    public static final String CURRENT_NEWS_TO_EDIT = "currentNewsToEdit";

    /**
     * Error login parameter.
     */
    public static final String ERROR = "error";
    
    /**
     * Logout message parameter for reading.
     */
    public static final String LOGOUT_MESSAGE = "msg";
    
    /**
     * Logout parameter.
     */
    public static final String LOGOUT = "logout";
    
    /**
     * Time zone parameter.
     */
    public static final String TIME_ZONE = "timeZone";
}
