package com.epam.newsmanagement.controller;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.PostedNews;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.TagService;

import static com.epam.newsmanagement.util.ConstantPool.SEARCH_CRITERIA;
import static com.epam.newsmanagement.util.ConstantPool.AUTHOR_LIST;
import static com.epam.newsmanagement.util.ConstantPool.TAG_LIST;

/**
 * Controller for save-news page. The controller is in charge of saving news
 * record in data base with list of authors and tags.
 * 
 * @author Kiryl Sprynchan
 */
@Controller
public class AddNewsController {

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	@Autowired
	private NewsManagementService newsManagerService;

	/**
	 * Show add-news page, with list of active authors and list of tags.
	 * 
	 * @return add-news page
	 * @throws ServiceException
	 */
	@RequestMapping("/add-news")
	public String addNews(Model model, HttpSession session)
			throws ServiceException {
		session.removeAttribute(SEARCH_CRITERIA);

		List<Author> authorList = authorService.readAll();
		List<Tag> tagList = tagService.readAll();

		model.addAttribute(AUTHOR_LIST, authorList);
		model.addAttribute(TAG_LIST, tagList);
		return "add-news";
	}

	/**
	 * Save new News object into data base, with selected author and list of
	 * tags.
	 * 
	 * @param title
	 *            news title
	 * @param Date
	 *            news creation date
	 * @param shortText
	 *            news brief text
	 * @param fullText
	 *            news full text
	 * @param authorId
	 *            author binding with news
	 * @param tagList
	 *            tags binding with news
	 * @return show-single-news page
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/add-new-news", method = RequestMethod.POST)
	public String addNewNews(@RequestParam String title,
		 @RequestParam String short_text,
			@RequestParam String full_text, @RequestParam Long author,
			@RequestParam(required = false)  String[] tagList, Model model, HttpSession session)
			throws ServiceException {
		Author newAuthor = authorService.read(author);

		News news = new News();

		List<Tag> newTagList = new ArrayList<Tag>();	
		if(tagList != null){
			for (String str : tagList) {
				Tag tag = new Tag();
				
				tag.setTagId( Long.parseLong(str));
				newTagList.add(tag);
			}
		}
		
		List<Comment> commentList = new ArrayList<Comment>();

		news.setTitle(title);
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		news.setShortText(short_text);
		news.setFullText(full_text);

		PostedNews newNews = new PostedNews(news, newAuthor, newTagList,
				commentList);

		Long newsId = newsManagerService.saveNews(newNews);
		return "redirect:/show-single-news?newsId=" + newsId;
	}

}
