package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static com.epam.newsmanagement.util.ConstantPool.ERROR;
import static com.epam.newsmanagement.util.ConstantPool.LOGOUT_MESSAGE;
import static com.epam.newsmanagement.util.ConstantPool.LOGOUT;
/**
 * Controller, that handles authentication failure or logout success
 * 
 * @author Kiryl Sprynchan
 *
 */
@Controller
public class LoginController {

	private static final String INVALID_USERNAME_OR_PASSWORD_MESSAGE = "error";

	private static final String LOGOUT_SUCCESS = "logout";

	/**
	 * Returned login page with special message, depend on situation which
	 * called this mapping method.(authentication failure, logout success)
	 * 
	 * @param error
	 *            parameter which tells us about authentication failure
	 * @param logout
	 *            parameter which tells us about logout success
	 * @return login page
	 */
	@RequestMapping("/login")
	public String login(
			@RequestParam(value = ERROR, required = false) String error,
			@RequestParam(value = LOGOUT, required = false) String logout,
			/*@RequestParam String timeZone,*/
			Model model) {

		/*System.out.println(timeZone);*/
		if (error != null) {
			model.addAttribute(ERROR, INVALID_USERNAME_OR_PASSWORD_MESSAGE);
		}

		if (logout != null) {
			model.addAttribute(LOGOUT_MESSAGE, LOGOUT_SUCCESS);
		}

		return "login";
	}
	
}
