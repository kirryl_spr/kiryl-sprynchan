package com.epam.newsmanagement.controller;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.PostedNews;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.TagService;

import static com.epam.newsmanagement.util.ConstantPool.CURRENT_NEWS_TO_EDIT;
import static com.epam.newsmanagement.util.ConstantPool.SINGLE_NEWS;
import static com.epam.newsmanagement.util.ConstantPool.AUTHOR_LIST;
import static com.epam.newsmanagement.util.ConstantPool.TAG_LIST;
import static com.epam.newsmanagement.util.ConstantPool.TIME_ZONE;

/**
 * Controller for edit-news page. The controller is in charge of editing news
 * record.
 * 
 * @author Kiryl Sprynchan
 *
 */
@Controller
public class EditNewsController {
    
	@Autowired
	private NewsManagementService newsManagerService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;
	
	/**
	 * Show edit-news page with field textfields by current news information.
	 * 
	 * @param newsId
	 *            news unique identifier of selected to edit news
	 * @return update-news page
	 * @throws ServiceException
	 */
	@RequestMapping("/editing-news")
	public String editingNews(@RequestParam(required = false) Long newsId, @RequestParam(required = false)  String timeZone, Model model, HttpSession session) throws ServiceException{
		if(newsId != null){
			session.setAttribute(CURRENT_NEWS_TO_EDIT, newsId);
		}
		else{
			newsId = (Long) session.getAttribute(CURRENT_NEWS_TO_EDIT);
		}
		
		PostedNews singleNews = newsManagerService.readSingleNews(newsId);	
		
		List<Author> authorList = authorService.readAll();
		List<Tag> tagList = tagService.readAll();

		model.addAttribute(AUTHOR_LIST, authorList);
		model.addAttribute(TAG_LIST, tagList);
		model.addAttribute(SINGLE_NEWS, singleNews);
		session.setAttribute(TIME_ZONE, timeZone);
		return "edit-news";
	}
	
	/**
	 * Editing news with list of bind authors and tags.
	 * 
	 * @param newsId
	 *            current news for updating
	 * @param title
	 *            new news title
	 * @param Date
	 *            modification date
	 * @param shortText
	 *            new news shot text
	 * @param fullText
	 *            new news full text
	 * @param author
	 *            new news author
	 * @param tagListId
	 *            new news list of tags
	 * @return show-single-news page
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/edit-news", method = RequestMethod.POST)
	public String editNews(@RequestParam Long newsId, @RequestParam String title,
			@RequestParam String short_text,
			@RequestParam String full_text,  @RequestParam Long author,
			@RequestParam(required = false)  String[] tagList, @RequestParam String creationDate, @RequestParam String timeZone, 
			Model model, HttpSession session)
			throws ServiceException {
		Author newAuthor = authorService.read(author);
		News news = new News();
		List<Tag> newTagList = new ArrayList<Tag>();	
		if(tagList != null){
			for (String str : tagList) {
				Tag tag = new Tag();
				
				tag.setTagId( Long.parseLong(str));
				newTagList.add(tag);
			}
		}
		
		List<Comment> commentList = new ArrayList<Comment>();

		news.setNewsId(newsId);
		news.setTitle(title);
		news.setModificationDate(new Date());
		news.setShortText(short_text);
		news.setFullText(full_text);

		PostedNews newNews = new PostedNews(news, newAuthor, newTagList,
				commentList);

		newsManagerService.updateNews(newNews);
		return "redirect:/show-single-news?newsId="+newsId+"&timeZone="+timeZone;
	}
	
}
