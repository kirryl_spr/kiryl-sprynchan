<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add news</title>
</head>
<body>
	<div class="add-news-content">
		<div id="add-news-content">
			<form action="add-new-news" class="new-news-container" id="save-form" method="POST">
				<table>
					<tr>
						<td><p><spring:message code="label.title"/>:</p></td>
						<td><input type="text" name="title" size="68" required maxlength="30"></td>
						<td class="hidden-message" id="hidden-title-message" style="display: none;">
							<spring:message code="empty_title"/>
						</td>
					</tr>
					
					<tr>
						<td><p><spring:message code="label.brief"/>:</p></td>
						<td><textarea id="brief" name="short_text" cols="70" rows="5" required maxlength="100"></textarea></td>
						<td class="hidden-message" id="hidden-brief-message" style="display: none;">
							<spring:message code="empty_brief"/>
						</td>
					</tr>

					<tr>
						<td><p><spring:message code="label.content"/>:</p></td>
						<td><textarea id="full_text" name="full_text" cols="70" rows="5"
								required maxlength="2000"></textarea></td>
						<td class="hidden-message" id="hidden-content-message" style="display: none;">
							<spring:message code="empty_content"/>
						</td>
					</tr>
					
					<tr>
						<td></td>
						<td>
							<div class="author-tag">
								<select id="authors" name="author" required>
									<option value><spring:message code="label.select_author"/></option>
									<c:forEach items="${authorList}" var="author">
										<c:if test="${author.expired == null }">
											<option value="${author.authorId}">${author.authorName}</option>
										</c:if>
									</c:forEach>
								</select> <select class="multi-select" multiple name="tagList" data-placeholder="<spring:message
							code="label.select_tags" />">
									<c:forEach items="${tagList}" var="tag">

										<option value="${tag.tagId }"><c:out value="${tag.tagName}"/></option>

									</c:forEach>
								</select>

							</div>
					</tr>
				</table>
				<input type="button" id="save-button" value="<spring:message code="label.save"/>">
			</form>
		</div>
	</div>

	<script>
		"use strict";
		$(".multi-select").chosen();
		
		$("#save-button").on("click", function() {
			var title = $('input[name=title]').val();
			var brief = $('textarea[name=short_text]').val();
			var full_text = $('textarea[name=full_text]').val();
			var author = $("#authors").val();

			$('#authors')
			  .css({'width':'225px', 'padding': '2px 0px', 'border-color': '#A5A5A5'});
			$('#hidden-title-message').hide();
			$('#hidden-brief-message').hide();
			$('#hidden-content-message').hide();
			
			if(title.length == 0)
				$('#hidden-title-message').show(500);
			if(brief.length == 0)
				$('#hidden-brief-message').show(500);
			if(full_text.length == 0)
				$('#hidden-content-message').show(500);
			if(author.length == 0)
				$('#authors')
				  .css({'width':'225px', 'padding': '2px 0px', 'border-color': '#F73A3A'});
			else if(title.length != 0 && brief.length != 0 && full_text.length != 0 && author.length != 0){
				$('#save-form').submit();
				} 
		});
	</script>
</body>
</html>