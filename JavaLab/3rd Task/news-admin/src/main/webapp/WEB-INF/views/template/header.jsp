<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
	<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>

<body>
	<div class="header-container">
		<p class="page-title">
			News portal -
			<spring:message code="label.administration" />
		</p>
		
		<div class="hello-block">
			<div class="admin-fields">

				<sec:authorize access="hasRole('ROLE_ADMIN')">
					<p class="hello-admin">
						<spring:message code="label.hello_admin" />
						,<security:authentication property="principal.username"/> 
					</p>
					<form action="logout" class="submit-form">
						<input type="submit"
							value="<spring:message code="label.logout.button"/>"
							class="logout-button">
					</form>
				</sec:authorize>


				<sec:authorize access="permitAll">
					<span class="localizationLogin"> <a href="?lang=en"><spring:message
						code="label.EN" /></a> <a href="?lang=ru"><spring:message
						code="label.RU" /></a>
					</span>	
				</sec:authorize>
			</div>
		</div>
	</div>
</body>

</html>