<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Authors page</title>
</head>
<body>

	<div class="show-authors-content">
		<div id="show-authors-content">
			<c:forEach items="${authorList}" var="author">
				<div class="single-author-info">
					<form action="edit-author" id="edit-author${author.authorId}" class="edit-author" method="POST">
						<spring:message code="label.author"/>  :
						
						<c:choose>
							<c:when test="${author.expired == null }">
								<input id="authorName${author.authorId}" type="text" name="authorName"
									value="<c:out value="${author.authorName}"/>" size="50" class="authorNamePole"
									disabled maxlength="30">
							</c:when>

							<c:otherwise>
								<input id="authorName${author.authorId}" type="text" name="authorName"
									value="${author.authorName}" size="50"
									style="background-color: #FB8A8A;" class="authorNamePole"
									disabled maxlength="30">
							</c:otherwise>
						</c:choose>

						<input type="hidden" name="authorId" value="${author.authorId}" id="${author.authorId}">
						<u class="edit-link" id="hide${author.authorId}" onClick="showButtons(${author.authorId})">
							<spring:message code="label.edit"/>
						</u> 
						<u
							class="update-link" id="update${author.authorId}"
							style="display: none;"
							onclick="checkAuthorNamePoleUpdate('${author.authorId}')">
							<spring:message code="label.update"/>
						</u>
						<input type="hidden" name="expired" value="${author.expired}">
					</form>
					
					
					<c:choose>
						<c:when test="${author.expired == null }">
							<form action="expire-author" id="expire-author${author.authorId}" class="expire-author" method="POST">
								<input type="hidden" name="authorId" value="${author.authorId }">
								<u class="expire-link" id="expire${author.authorId}" style="display: none;"
								 onclick="checkAuthorNamePoleExpire('${author.authorId}')">
									<spring:message code="label.expire"/>
								</u>
								 
								<u
									class="cancel-link" id="cancel${author.authorId}"
									style="display: none;"
									onclick="hideButtons(${author.authorId})"><spring:message code="label.cancel"/></u>
							</form>
						</c:when>
						<c:otherwise>
							<form action="unexpire-author" id="expire-author" class="expire-author" method="POST">
								<input type="hidden" name="authorId" value="${author.authorId }">
								<u class="expire-link" id="expire${author.authorId}"
									style="display: none;"
									onclick="$(this).closest('form').submit();"><spring:message code="label.unexpire"/> </u> <u
									class="cancel-link" id="cancel${author.authorId}"
									style="display: none;"
									onclick="hideButtons(${author.authorId})"><spring:message code="label.cancel"/></u>
							</form>
						</c:otherwise>
					</c:choose>

				</div>
				<div class="edit-author-hidden-message" id="input-author-hidden-message${author.authorId}" style="display: none;"><spring:message code="empty_author"/></div>
			</c:forEach>

			<form action="add-author" class="add-author-form" method="POST">
				<p class="add-author-label"><spring:message code="label.add_author"/> :</p>
				<input type="text" class="new-author" name="authorName" size="50" maxlength="30"> 
				<u class="save-link"><spring:message code="label.add"/></u>
				<div id="hidden-author-message" style="display: none;"><spring:message code="empty_author"/></div>
			</form>
		</div>
	</div>
	
		<script>

		$(".save-link").on("click",function(){
			var authorName = $('input[type=text].new-author');
			
			if(authorName.val()==0){
				$('#hidden-author-message').show(500);
			}else{
				$(".add-author-form").submit();
			}
		})
	</script>
</body>
</html>