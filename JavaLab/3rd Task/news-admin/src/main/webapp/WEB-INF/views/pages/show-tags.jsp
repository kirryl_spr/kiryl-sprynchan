<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tags page</title>
</head>
<body>
	<div class="show-authors-content">
		<div id="show-authors-content">
			<c:forEach items="${tagList}" var="tag">
				<div class="single-author-info">
					<form action="edit-tag" id="edit-tag${tag.tagId}" class="expire-author" method="POST">
						<spring:message code="label.tag"/> : <input id="tagName${tag.tagId}" type="text" name="tagName"
							value="<c:out value="${tag.tagName}"/>" size="60" class="authorNamePole" disabled maxlength="30">

						<input id="${tag.tagId}" type="hidden" name="tagId" value="${tag.tagId }"> 
						<u class="edit-link" id="hide${tag.tagId}" onClick="showButtonsForTag(${tag.tagId})"><spring:message code="label.edit"/></u> 
						
						<u class="update-link" id="update${tag.tagId}" style="display: none;"
							onclick="checkTagNameUpdate('${tag.tagId}')"><spring:message code="label.update"/></u>
					</form>

					<form action="delete-tag" id="expire-author" class="expire-author" method="POST">
						<input type="hidden" name="tagId" value="${tag.tagId }"> 
						<u class="expire-link" id="expire${tag.tagId}" style="display: none;"
							onclick="$(this).closest('form').submit();"><spring:message code="label.delete_tag"/></u> 
							
						<u class="cancel-link" id="cancel${tag.tagId}"
							style="display: none;" onclick="hideButtonsForTag(${tag.tagId})"><spring:message code="label.cancel"/></u>
					</form>
				</div>
				<div class="edit-tag-hidden-message" id="input-tag-hidden-message${tag.tagId}" style="display: none;"><spring:message code="empty_tag"/></div>
			</c:forEach>

			<form action="add-tag" class="add-author-form" method="POST" id="add-tag">
				<p class="add-tag-label"><spring:message code="label.add_tag"/>:</p>
				<input type="text" class="new-tag" name="tagName" size="60" required maxlength="30"> 
				<u class="save-link"><spring:message code="label.add"/></u>
				<div id="hidden-tag-message" style="display: none;"><spring:message code="empty_tag"/></div>
			</form>
		</div>
	</div>
	
	<script>
		$(".save-link").on("click",function(){
			var tagName = $('input[type=text].new-tag');
			
			if(tagName.val()==0){
				$('#hidden-tag-message').show(500);
			}else{
				$("#add-tag").submit();
			}
		})
	</script>
</body>
</html>