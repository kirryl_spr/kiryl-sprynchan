<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>403-page</title>
<link rel="stylesheet" href="resources/css/style.css" type="text/css" />
</head>
<body>
	<div class="main-container">
		<div class="error-content">
			403. You have no rights to this content. <a
				href="${pageContext.request.contextPath}/login" id="back-to-login">back
				to login page</a>
		</div>
	</div>
</body>
</html>