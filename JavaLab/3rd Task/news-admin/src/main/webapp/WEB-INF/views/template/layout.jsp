<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script src="resources/script/jquery-1.11.2.min.js"></script>
<script src="resources/script/chosen.jquery.js"></script>
<script src="resources/script/chosen.jquery.min.js"></script>
<script src="resources/script/chosen.proto.js"></script>
<script src="resources/script/chosen.proto.min.js"></script>

<script src="resources/script/myScripts.js"></script>

<link rel="stylesheet" href="resources/css/chosen.css"  type="text/css" />
<link rel="stylesheet" href="resources/css/chosen.min.css"  type="text/css" />
<link rel="stylesheet" href="resources/css/style.css"  type="text/css" />
<link rel="icon" href="${pageContext.request.contextPath}/resources/images/logo.png">
</head>
<body>
<div class="main-container">
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="menu" /> 
	<tiles:insertAttribute name="content" />
	<tiles:insertAttribute name="footer" />
</div>
</body>
</html>