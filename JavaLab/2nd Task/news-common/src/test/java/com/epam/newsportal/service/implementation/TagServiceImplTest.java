package com.epam.newsportal.service.implementation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.implementation.TagServiceImpl;

/**
 * Public class <code>TagServiceImplTest</code> content list of public Mockito
 * Unit tests, which check right work of Service layer class
 * <code>TagServiceImpl</code> methods.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.service.TagService
 * @see com.epam.newsmanagement.service.implementation.TagServiceImpl
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {

	/**
	 * mock object, which will be inject into TagServiceImpl class
	 */
	@Mock
	private TagDao tagDao;

	/**
	 * service layer object, which we will testing
	 */
	@InjectMocks
	private TagServiceImpl tagService;
	
	/**
	 * Public Mockito Unit test, that verify work create() method in the
	 * TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception{
		Long tagId = null;
		
		Tag tag =  new Tag();
		when(tagDao.create(tag)).thenReturn(Long.valueOf(1L));
		
		tagId = tagService.create(tag);
		verify(tagDao, times(1)).create(tag);
		
		assertEquals(tagId, Long.valueOf(1L));
	}
	
	/**
	 * Public Mockito Unit test, that verify work read() method in the
	 * TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception{
		Long tagId = 1L;
		String tagName = "MockTag";
		
		Tag tag = new Tag();
		tag.setTagId(tagId);
		tag.setTagName(tagName);
		when(tagDao.read(tagId)).thenReturn(tag);
		
		Tag mockTag = tagService.read(tagId);
		verify(tagDao, times(1)).read(tagId);
		
		assertEquals(tagId, mockTag.getTagId());
		assertEquals(tagName, mockTag.getTagName());
	}
	
	/**
	 * Public Mockito Unit test, that verify work readAll() method in the
	 * TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception{
		List<Tag> tagList = null;
		
		when(tagDao.readAll()).thenReturn(new ArrayList<Tag>());
		
		tagList = tagService.readAll();
		
		verify(tagDao, times(1)).readAll();
		assertNotNull(tagList);
	}
	
	/**
	 * Public Mockito Unit test, that verify work update() method in the
	 * TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception{
		Tag tag = new Tag();
		
		tagService.update(tag);
		verify(tagDao, times(1)).update(tag);
	}
	
	/**
	 * Public Mockito Unit test, that verify work delete() method in the
	 * TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception{
		Long tagId = 1L;
		
		tagService.delete(tagId);
		verify(tagDao, times(1)).delete(tagId);
	}
	
	/**
	 * Public Mockito Unit test, that verify work searchTagByAuthor() method in
	 * the TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchTagsByAuthor() throws Exception{
		List<Tag> tagList = null;
		
		Author author = new Author();
		when(tagDao.searchTagsByAuthor(author)).thenReturn(new ArrayList<Tag>());
		
		tagList = tagService.searchTagsByAuthor(author);
		
		verify(tagDao, times(1)).searchTagsByAuthor(author);
		assertNotNull(tagList);
	}
}
