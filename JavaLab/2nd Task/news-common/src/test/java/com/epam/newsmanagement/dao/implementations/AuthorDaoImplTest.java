package com.epam.newsmanagement.dao.implementations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Class <code>AuthorDaoImplTest</code> content list of public Data Base
 * Unit tests, which check right work of Data Access layer class
 * <codes>AuthorDao</code> methods.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.dao.AuthorDao
 * @see com.epam.newsmanagement.domain.Author
 * @since 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = { "classpath:/SpringTest.xml" })
@DatabaseSetup(value = "classpath:/data.before.operation.xml")
public class AuthorDaoImplTest {

	/**
	 * reference on AuthorDao class, to use it all public methods
	 */
	@Autowired
	private AuthorDao authorDao;	

	/**
	 * Data Base Unit test, that verify work create() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws DaoException {
		Long authorId = null;
		String authorName = "Taylor";

		Author author = new Author();
		author.setAuthorName(authorName);

		authorId = authorDao.create(author);
		author.setAuthorId(authorId);
		
		Author createdAuthor = authorDao.read(authorId);
		
		assertEqualsAuthor(author, createdAuthor);
	}

	/**
	 * Data Base Unit test, that verify work read() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws DaoException {
		Author author = new Author();
		
		Long authorId = 1L;
		author.setAuthorId(authorId);
		String authorName = "Bob";
		author.setAuthorName(authorName);

		Author readedAuthor = new Author();
		readedAuthor = authorDao.read(authorId);

		assertEqualsAuthor(author, readedAuthor);
	}

	/**
	 * Data Base Unit test, that verify work readAll() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws DaoException {
		Author author = new Author();
		
		Long authorId = 3L;
		author.setAuthorId(authorId);
		String authorName = "Robby";
		author.setAuthorName(authorName);
		
		int expectedSize = 4;
		
		List<Author> authorList = authorDao.readAll();
		
		Author readedAuthor = authorList.get(2);
		
		assertEquals(authorList.size(), expectedSize);

		assertEqualsAuthor(author, readedAuthor);
	}

	/**
	 * Data Base Unit test, that verify work update() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws DaoException {
		Long authorId = 1L;
		String authorName = "SuperNewBob";
		
		Author author = new Author();
		author.setAuthorId(authorId);
		author.setAuthorName(authorName);

		authorDao.update(author);
		
		Author updatedAuthor = authorDao.read(authorId);
		
		assertEqualsAuthor(author, updatedAuthor);
	}

	/**
	 * Data Base Unit test, that verify work delete() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 * @throws DaoException 
	 */
	@Test
	public void delete() throws  DaoException {
		Long authorId = 1L;
		
		Author author = new Author();
		author = authorDao.read(authorId);
		assertNotNull(author);
		
		authorDao.delete(authorId);
		author = authorDao.read(authorId);
		assertNull(author);
	}

	/**
	 * Private method that is used for comparing two objects
	 * 
	 * @param firstAuthor
	 * @param secondAuthor
	 */
	private void assertEqualsAuthor(Author firstAuthor, Author secondAuthor){
		assertEquals(firstAuthor.getAuthorId(), secondAuthor.getAuthorId());
		assertEquals(firstAuthor.getAuthorName(), secondAuthor.getAuthorName());
	}
}
