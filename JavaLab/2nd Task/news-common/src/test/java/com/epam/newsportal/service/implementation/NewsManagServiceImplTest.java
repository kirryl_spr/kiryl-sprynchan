package com.epam.newsportal.service.implementation;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.PostedNews;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.implementation.NewsManagementServiceImpl;

/**
 * Public class <code>NewsManagServiceImplTest</code> content list of public
 * Mockito Unit tests, which check right work of Service layer class
 * <code>NewsManagerServiceImpl</code> methods.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.service.NewsManagementService
 * @see com.epam.newsmanagement.service.implementation.NewsManagServiceImplTestImpl
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsManagServiceImplTest {

	/**
	 * authorService object, which will be inject into CommentServiceImpl class
	 */
	@Mock
	private AuthorService authorService;

	/**
	 * newsService object, which will be inject into CommentServiceImpl class
	 */
	@Mock
	private NewsService newsService;

	/**
	 * commentService object, which will be inject into CommentServiceImpl class
	 */
	@Mock
	private CommentService commentService;

	/**
	 * tagService object, which will be inject into CommentServiceImpl class
	 */
	@Mock
	private TagService tagService;

	/**
	 * service manager object, which we will testing
	 */
	@InjectMocks
	private NewsManagementServiceImpl newsManagementService;
	
	/**
	 * Public Mockito Unit test, that verify work saveNews() method in the
	 * NewsManagServiceImpl class. This method consists of calls few simple
	 * service methods.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void saveNews() throws Exception {

		News news = new News();
		Author author = new Author();
		List<Tag> tagList = new ArrayList<Tag>();
		for (int i = 0; i < 3; i++) {
			tagList.add(new Tag());
		}
		List<Comment> commentList = new ArrayList<Comment>();
		for (int i = 0; i < 3; i++) {
			commentList.add(new Comment());
		}

		PostedNews postedNews = new PostedNews();
		postedNews.setNews(news);
		postedNews.setAuthor(author);
		postedNews.setTagList(tagList);
		postedNews.setCommentList(commentList);
		
		newsManagementService.saveNews(postedNews);
		verify(newsService, times(1)).create(news);
		verify(newsService, times(1)).saveNewsAuthor(news, author);
		verify(newsService, times(1)).saveNewsTagList(news, tagList);
	}
	
	/**
	 * Public Mockito Unit test, that verify work deleteNews() method in the
	 * NewsManagServiceImpl class. This method consists of calls few simple
	 * service methods.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void deleteNews() throws Exception {

		List<Long> newsIdList = new ArrayList<Long>();
		for (int i = 1; i < 4; i++) {
			newsIdList.add((long) i);
		}
			
		for(int i = 0; i < newsIdList.size(); i++){
		newsManagementService.deleteNews(newsIdList.get(i));
		}
		
		Iterator<Long> it = newsIdList.iterator();
		
		while(it.hasNext()){
			Long newsId = (Long) it.next();
			
			verify(commentService, times(1)).deleteAllCommentsForNews(newsId);
			verify(authorService, times(1)).deleteNewsAuthor(newsId);
			verify(tagService, times(1)).deleteNewsTagByNews(newsId);
		}	
	}
}
