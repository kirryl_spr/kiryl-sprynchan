package com.epam.newsportal.service.implementation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.implementation.NewsServiceImpl;

/**
 * Public class <code>NewsServiceImplTest</code> content list of public Mockito
 * Unit tests, which check right work of Service layer class
 * <code>NewsManagerServiceImpl</code> methods.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.service.NewsService
 * @see com.epam.newsmanagement.service.implementation.NewsManagerServiceImpl
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {

	/**
	 * mock object, which will be inject into NewsServiceImpl class
	 */
	@Mock
	private NewsDao newsDao;
	
	/**
	 * service layer object, which we will testing
	 */
	@InjectMocks
	private NewsServiceImpl newsService;
	
	/**
	 * Public Mockito Unit test, that verify work create() method in the
	 * NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long newsId = null;

		News news = new News();
		when(newsDao.create(news)).thenReturn(Long.valueOf(1L));

		newsId = newsService.create(news);
		verify(newsDao, times(1)).create(news);
		
		assertEquals(newsId, Long.valueOf(1L));
	}
	
	/**
	 * Public Mockito Unit test, that verify work read() method in the
	 * NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long newsId = 1L;
		String shortText = "Mock short text";
		String fullText = "Mock full text";
		String title = "Mock title";
		java.util.Date creationDate = Timestamp.valueOf("2015-08-21 10:45:24");
		java.util.Date modificationDate = Date.valueOf("2015-08-21");

		News news = new News();
		news.setNewsId(newsId);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setTitle(title);
		news.setCreationDate(creationDate);
		news.setModificationDate(modificationDate);

		when(newsDao.read(newsId)).thenReturn(news);

		news = newsService.read(newsId);
		verify(newsDao, times(1)).read(newsId);
		
		assertEquals(news.getNewsId(), newsId);
		assertEquals(news.getShortText(), shortText);
		assertEquals(news.getFullText(), fullText);
		assertEquals(news.getTitle(), title);
		assertEquals(news.getCreationDate(), creationDate);
		assertEquals(news.getModificationDate(), modificationDate);

	}
	
	/**
	 * Public Mockito Unit test, that verify work readAll() method in the
	 * NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		List<News> tagList;
		when(newsDao.readAll()).thenReturn(new ArrayList<News>());

		tagList = newsService.readAll();
		verify(newsDao, times(1)).readAll();
		
		assertNotNull(tagList);
	}
	
	@Test
	public void update() throws Exception {
		News news = new News();

		newsService.update(news);
		verify(newsDao, times(1)).update(news);
	}
	
	/**
	 * Public Mockito Unit test, that verify work delete() method in the
	 * NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long newsId = 1L;
		
		newsService.delete(newsId);
		verify(newsDao, times(1)).delete(newsId);
	}
	
	/**
	 * Public Mockito Unit test, that verify work searchNewsByAuthor() method in
	 * the NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchNewsByAuthor() throws Exception {
		List<News> newsList = null;

		Author author = new Author();
		
		when(newsDao.searchNewsByAuthor(author)).thenReturn(
				new ArrayList<News>());

		newsList = newsService.searchNewsByAuthor(author);
		verify(newsDao, times(1)).searchNewsByAuthor(author);
		
		assertNotNull(newsList);
	}
	
	/**
	 * Public Mockito Unit test, that verify work searchNewsByTag() method in
	 * the NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchNewsByTag() throws Exception {
		List<News> newsList = null;

		Tag tag = new Tag();
		when(newsDao.searchNewsByTag(tag)).thenReturn(new ArrayList<News>());

		newsList = newsService.searchNewsByTag(tag);
		verify(newsDao, times(1)).searchNewsByTag(tag);
		
		assertNotNull(newsList);
	}
	
	/**
	 * Public Mockito Unit test, that verify work getSortedNews() method
	 * in the NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void getSortedNews() throws Exception {
		List<News> newsList = null;
		
		SearchCriteria searchCritteria = new SearchCriteria();

		when(newsDao.getSortedNews(searchCritteria)).thenReturn(new ArrayList<News>());

		newsList = newsService.getSortedNews(searchCritteria);
		verify(newsDao, times(1)).getSortedNews(searchCritteria);
		assertNotNull(newsList);
	}
	
	/**
	 * Public Mockito Unit test, that verify work saveNewsAuthor() method
	 * in the AuthorServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void saveNewsAuthor() throws Exception {

		News news = new News();
		Author author = new Author();
		
		newsService.saveNewsAuthor(news, author);
		verify(newsDao, times(1)).saveNewsAuthor(news.getNewsId(), author.getAuthorId());
	}
	
	/**
	 * Public Mockito Unit test, that verify work saveNewsTag() method
	 * in the TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void saveNewsTagList() throws Exception {
		News news = new News();
		news.setNewsId(1L);
		
		Tag firstTag = new Tag();
		firstTag.setTagId(1L);
		
		Tag secondTag = new Tag();
		secondTag.setTagId(2L);
		
		List<Tag> tagList = new ArrayList<Tag>();
		tagList.add(firstTag);
		tagList.add(secondTag);

		newsService.saveNewsTagList(news, tagList);
		Iterator<Tag> it = tagList.iterator();
		
		while(it.hasNext()){
			Tag obj = (Tag) it.next();
			verify(newsDao, times(1)).saveNewsTag(news.getNewsId(), obj.getTagId());
		}
		
	}
}
