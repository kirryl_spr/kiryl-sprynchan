package com.epam.newsmanagement.dao.implementations;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;


/**
 * Public class <code>TagDaoImplTest</code> content list of public Data Base
 * Unit tests, which check right work of Data Access layer class
 * <code>TagDao</code> methods.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.dao.TagDao
 * @see com.epam.newsmanagement.domain.Tag
 * @since 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, 
	TransactionalTestExecutionListener.class,
	DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = { "classpath:/SpringTest.xml" })
@DatabaseSetup(value = "classpath:/data.before.operation.xml" )

public class TagDaoImplTest {
	
	/**
	 * reference on NewsDao class, to use it all public methods
	 */
	@Autowired
	private TagDao tagDao;
	
	/**
	 * Data Base Unit test, that checks work create() method in the TagDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws DaoException{
		Long tagId = null;
		String tagName = "RealTestTag";
		
		Tag tag = new Tag();
		tag.setTagName(tagName);
		tagId = tagDao.create(tag);
		tag.setTagId(tagId);
		
		Tag createdTag = tagDao.read(tagId);

		assertEqualsTag(tag, createdTag);
		}
	
	/**
	 * Data Base Unit test, that checks work read() method in the TagDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws DaoException{
		Long tagId = 1L;
		String tagName = "FirstTag";

		Tag tag = new Tag();
		tag.setTagId(tagId);
		tag.setTagName(tagName);
		
		Tag readedTag = tagDao.read(tagId);

		assertEqualsTag(tag, readedTag);
	}
	
	/**
	 * Data Base Unit test, that checks work readAll() method in the TagDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws DaoException {
		String tagName = "SecondTag";
		List<Tag> tagList = tagDao.readAll();

		Tag tag = tagList.get(1);
		assertEquals(tag.getTagName(), tagName);
	}
	
	/**
	 * Data Base Unit test, that checks work update() method in the TagDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws DaoException {
		Long tagId = 1L;
		String tagName = "SuperNewTag";

		Tag tag = new Tag();
		tag.setTagId(tagId);
		tag.setTagName(tagName);
		tagDao.update(tag);

		Tag updatedTag = tagDao.read(tagId);
		
		assertEqualsTag(tag, updatedTag);
	}
	
	/**
	 * Data Base Unit test, that checks work delete() method in the TagDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws DaoException {
		Long tagId = 1L;

		Tag tag = new Tag();
		tag = tagDao.read(tagId);
		assertNotNull(tag);

		tagDao.delete(tagId);
		tag = tagDao.read(tagId);
		assertNull(tag);
	}
	
	/**
	 * Data Base Unit test, that checks work searchTagsByAuthor() method in the
	 * TagDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchTagsByAuthor() throws DaoException {
		Long authorId = 1L;
		
		String tagName = "FirstTag";
		
		Author author = new Author();
		author.setAuthorId(authorId);
		
		List<Tag> tagList = tagDao.searchTagsByAuthor(author);
		
		Tag tag = tagList.get(0);
		
		assertEquals(tag.getTagName(), tagName);
	}
	
	private void assertEqualsTag(Tag firstTag, Tag secondTag){
		assertEquals(firstTag.getTagId(), secondTag.getTagId());
		assertEquals(firstTag.getTagName(), secondTag.getTagName());
	}
}
