package com.epam.newsmanagement.domain;

import org.springframework.security.core.GrantedAuthority;

/**
 * Public class <code>Role</code> is one of Entities classes. Its content is
 * fully consistent with Table Author in data base, which we use for. The main
 * role is to store associated with the table information(data). Can be an
 * element <code>HashTable</code>
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @since 1.0
 */
public class Role implements GrantedAuthority{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4451400907690901123L;
	
	/**
	 * Parameter for user-id attribute
	 */
	private Long userId;
	
	/**
	 * Parameter for role name attribute
	 */
	private String roleName;
	
	public Long getUserId() {
		return userId;
	}


	public void setUserId(Long userId) {
		this.userId = userId;
	}


	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}


	@Override
	public String getAuthority() {
		return roleName;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((roleName == null) ? 0 : roleName.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (roleName == null) {
			if (other.roleName != null)
				return false;
		} else if (!roleName.equals(other.roleName))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Role [userId=" + userId + ", roleName=" + roleName + "]";
	}
}
