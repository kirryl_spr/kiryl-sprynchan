package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.User;
import com.epam.newsmanagement.exception.DaoException;

/**
 * Interface of using daoMethods for working with USERS table in database
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.domain.User
 * @since 1.0
 */
public interface UserDao {
	
	/**
	 * Reading user from database by login value.
	 *
	 * @return list of entity records to read
	 * @throws com.epam.newsmanagement.exception.DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	User readUserByLogin(String login)throws DaoException;
}
