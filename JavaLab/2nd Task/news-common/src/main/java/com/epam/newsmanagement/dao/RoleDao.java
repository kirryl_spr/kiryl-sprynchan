package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.domain.Role;

/**
 * Interface of using daoMethods for working with ROLES table in database
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.domain.Role
 * @since 1.0
 */
public interface RoleDao {


	/**
	 * Reading roles for user from database by login value.
	 *
	 * @return list of entity records to read
	 * @throws com.epam.newsmanagement.exception.DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	List<Role> readRolesByLogin(String login)throws DaoException;
	
}
