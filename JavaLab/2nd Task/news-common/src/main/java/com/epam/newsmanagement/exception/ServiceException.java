package com.epam.newsmanagement.exception;

/**
 * Public class <code>ServiceException</code> user-define exception class
 * and can be used for catching any subclasses exceptions on the height level of
 * application.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @since 1.0
 */
public class ServiceException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7476913747591402181L;

	/**
	 * Constructs an {@code DaoException} with the specified
	 * detail message and error object that trow up when exception generate.
	 *
	 * @param message
	 *            some string which describe exception cause
	 * @param cause
	 *            exception object that throw up
	 */
	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	
	/**
	 * Constructs an {@code DaoException} with the specified
	 * detail message.
	 *
	 * @param message
	 *            some string which describe exception cause
	 */
	public ServiceException(String message) {
		super(message);
	}

	/**
	 * Constructs an {@code DaoException} with the specified
	 * object exception message.
	 *
	 * @param cause
	 *            exception object that throw up
	 */
	public ServiceException(Exception cause) {
		super(cause);
	}

}
