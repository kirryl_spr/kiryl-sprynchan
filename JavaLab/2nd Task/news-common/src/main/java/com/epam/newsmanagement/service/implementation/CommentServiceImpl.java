package com.epam.newsmanagement.service.implementation;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;

import static com.epam.newsmanagement.utils.ServiceUtil.isObjectNull;

/**
 * Public class <code>CommentServiceImpl</code> is an element of Service layer
 * and working with Comment data base instance. This is a realization of
 * <code>CommentService</code> interface and it performs all operations
 * described in this interface.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.service.CommentService
 * @see com.epam.newsmanagement.domain.Comment
 * @since 1.0
 */
@Service
public class CommentServiceImpl implements CommentService {
	
	/**
	 * object to conduct logging
	 */
	private static final Logger logger = Logger.getLogger(CommentServiceImpl.class.getName());

	/**
	 * link to the interface part, providing access to basic operations
	 */
	@Autowired
	private CommentDao commentDao;

	/**
	 * Override method used to add record to the table Comment, which calls
	 * create() method from CommentDao interface.
	 *
	 * @param comment
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 */
	public Long create(Comment comment) throws ServiceException {
		Long commentId = null;
		
		isObjectNull(comment, "Comment is null.");

		try {
			commentId = commentDao.create(comment);
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot create comment.", e);
		}
		return commentId;
	}

	/**
	 * Override method used to read record from the table Comment, which calls
	 * read() method from CommentDao interface.
	 *
	 * @param commentId
	 *            unique identifier of instance which will be found
	 * @return find Comment instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 */
	public Comment read(Long commentID) throws ServiceException {
		Comment comment = null;

		try {

			comment = commentDao.read(commentID);
			return comment;

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot read comment.", e);
		}
	}

	/**
	 * Override method used to read all records from the table Comment, which
	 * calls readAll() method from CommentDao interface.
	 *
	 * @return list of find Comment instances
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 */
	public List<Comment> readAll() throws ServiceException {
		List<Comment> commentList = null;

		try {

			commentList = commentDao.readAll();
			return commentList;

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot read all comments", e);
		}
	}

	/**
	 * Override method used to update record in the table Comment, which calls
	 * update() method from CommentDao interface.
	 *
	 * @param comment
	 *            instance which will be update in the data base
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 */
	public void update(Comment comment) throws ServiceException {

		isObjectNull(comment, "Comment is null.");
		
		try {

			commentDao.update(comment);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot update comment.", e);
		}
	}

	/**
	 * Override method used to delete record from the table Comment, which calls
	 * delete() method from CommentDao interface.
	 *
	 * @param commentId
	 *            unique identifier of instance which will be delete
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 */
	public void delete(Long commentID) throws ServiceException {

		try {

			commentDao.delete(commentID);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot delete comment", e);
		}
	}

	/**
	 * Defining search operation from Comment table in data base by sent News
	 * instance, using the searchCommentsByNews() method in CommentDao interface.
	 *
	 * @param news
	 *            element of News instance to search for Comment list
	 * @return list of Comment which was find by the News instance
	 * @@throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public List<Comment> searchCommentsByNews(News news)
			throws ServiceException {
		List<Comment> commentList = null;

		isObjectNull(news, "News is null.");

		try {
			commentList = commentDao.searchCommentsByNews(news);
			return commentList;
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Can not find comments be news");
		}
	}
	
	
	/**
	 * Override method used to delete operation from Comments table in data base by sent Long
	 * instance, using the deleteAllCommentsForNews(Long newsId) method in CommentDao interface.
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @@throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public void deleteAllCommentsForNews(Long newsId) throws ServiceException {
		try {

			commentDao.deleteAllCommentsForNews(newsId);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot delete all comments for news", e);
		}
	}
}
