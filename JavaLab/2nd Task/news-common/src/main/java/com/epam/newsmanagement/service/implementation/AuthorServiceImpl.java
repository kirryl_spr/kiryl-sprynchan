package com.epam.newsmanagement.service.implementation;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

import static com.epam.newsmanagement.utils.ServiceUtil.isObjectNull;

/**
 * Public class <code>AuthorServiceImpl</code> is an element of Service layer
 * and working with Author data base instance. This is a realization of
 * <code>AuthorService</code> interface and it performs all operations described
 * in this interface.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.service.AuthorService
 * @see com.epam.newsmanagement.domain.Author
 * @since 1.0
 */
@Service
public class AuthorServiceImpl implements AuthorService {
	
	/**
	 * object to conduct logging
	 */
	private static final Logger logger = Logger.getLogger(AuthorServiceImpl.class.getName());

	/**
	 * link to the interface part, providing access to basic operations
	 */
	@Autowired
	private AuthorDao authorDao;

	/**
	 * Override method used to add record to the table Author, which calls
	 * create() method from AuthorDao interface.
	 *
	 * @param author
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @throws ServiceException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 */
	public Long create(Author author) throws ServiceException {
		Long authorId = null;

		isObjectNull(author, "Author is null");
		
		try {
			authorId = authorDao.create(author);
		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot create author", e);
		}
		return authorId;

	}

	/**
	 * Override method used to read record from the table Author, which calls
	 * read() method from AuthorDao interface.
	 *
	 * @param authorId
	 *            unique identifier of instance which will be found
	 * @return find Author instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 */
	public Author read(Long authorID) throws ServiceException {
		Author author = null;

		try {

			author = authorDao.read(authorID);
			return author;

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot read author", e);
		}
	}

	/**
	 * Override method used to read all records from the table Author, which
	 * calls readAll() method from AuthorDao interface.
	 *
	 * @return list of find Author instances
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 */
	public List<Author> readAll() throws ServiceException {
		List<Author> authorList = null;

		try {

			authorList = authorDao.readAll();
			return authorList;

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot read all authors", e);
		}
	}

	/**
	 * Override method used to update record in the table Author, which calls
	 * update() method from AuthorDao interface.
	 *
	 * @param author
	 *            instance which will be update in the data base
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 */
	public void update(Author author) throws ServiceException {
		
		isObjectNull(author, "Author is null.");
		
		try {

			authorDao.update(author);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot update author", e);
		}
	}

	/**
	 * Override method used to delete record from the table Author, which calls
	 * delete() method from AuthorDao interface.
	 *
	 * @param authorId
	 *            unique identifier of instance which will be delete
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 */
	public void delete(Long authorID) throws ServiceException {
		try {

			authorDao.delete(authorID);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot delete author", e);
		}
	}

	/**
	 * Override method used to search Author record from the table Author by
	 * News instance.
	 *
	 * @param author
	 *            element of News instance to search for Author instance
	 * @return Author which was find by the News instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	public Author searchAuthorByNews(News news) throws ServiceException {
		Author author = null;
		
		isObjectNull(news, "News is null");

		try {

			author = authorDao.searchAuthorByNews(news);
			return author;

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot update author", e);
		}
	}
	
	/**
	 * Override method used to delete data from table NEWS_AUTHOR 
	 * when news is deleting
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	public void deleteNewsAuthor(Long newsId) throws ServiceException {
		try {

			authorDao.deleteNewsAuthor(newsId);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot delete news author.", e);
		}
	}

	/**
	 * Defining making author unexpired operation
	 *
	 * @param authorId
	 *            element of Long instance to delete necessary rows
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.domain.Author
	 */
	@Override
	public void unexpireAuthor(Long authorId) throws ServiceException {
		try {

			authorDao.unexpireAuthor(authorId);

		} catch (DaoException e) {
			logger.error("DaoException caught", e);
			throw new ServiceException("Cannot unexpire author", e);
		}
		
	}

}
