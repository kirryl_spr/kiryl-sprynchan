package com.epam.newsmanagement.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Public class <code>Author</code> is one of Entities classes. Its content is
 * fully consistent with Table Author in data base, which we use for. The main
 * role is to store associated with the table information(data). Can be an
 * element <code>HashTable</code>
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @since 1.0
 */
public class Author implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -151800943121899813L;

	/**
	 * unique identifier of instance
	 */
	private Long authorId; 

	/**
	 * parameter describe author last Name
	 */
	private String authorName;
	
	/**
	 * parameter describing whether author is expired
	 */
	private Date expired;

	/**
	 * Constructor without parameters for creating an object.
	 */
	public Author(){
	}
	
	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	
	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (this != obj)
			return false;
		if (!(getClass() == obj.getClass()))
			return false;

		Author author = (Author) obj;
		if (authorId != null ? !authorId.equals(author.authorId)
				: author.authorId != null)
			return false;
		if (authorName != null ? !authorName.equals(author.authorName)
				: author.authorName != null)
			return false;

		return true;
	}
	
	@Override
	public int hashCode() {
		int result = (authorId != null ? authorId.hashCode() : 0);
		result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Author{" + "authorID=" + authorId + ", authorName='"
				+ authorName + '\'' + '}';
	}
}
