package com.epam.newsmanagement.utils;

import java.util.List;

import com.epam.newsmanagement.domain.SearchCriteria;

/**
 * Public class which is used for constructing SQL-queries.
 * 
 * @author Kiryl_Sprynchan
 * @version 1.0
 * @since 1.0
 */
public class QueryBuilderUtil {

	private static final String SQL_SEARCH_BY_SEARCH_CRITERIA_BY_PAGE_BEGINING = "SELECT NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, "
			+ "CREATION_DATE, MODIFICATION_DATE FROM(SELECT NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, "
			+ "CREATION_DATE, MODIFICATION_DATE, ROWNUM rn FROM(SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS "
			+ "LEFT OUTER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID "
			+ "LEFT OUTER JOIN NEWS_TAG ON NEWS.NEWS_ID=NEWS_TAG.NEWS_ID "
			+ "LEFT OUTER JOIN COMMENTS ON NEWS.NEWS_ID=COMMENTS.NEWS_ID "
			+ "WHERE ";

	private static final String SQL_SEARCH_BY_SEARCH_CRITERIA_BEGINING = "SELECT NEWS.NEWS_ID, "
			+ "NEWS.SHORT_TEXT,NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, "
			+ "NEWS.MODIFICATION_DATE FROM NEWS "
			+ "LEFT OUTER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID "
			+ "LEFT OUTER JOIN NEWS_TAG ON NEWS.NEWS_ID=NEWS_TAG.NEWS_ID "
			+ "LEFT OUTER JOIN COMMENTS ON NEWS.NEWS_ID=COMMENTS.NEWS_ID "
			+ "WHERE ";

	private static final String SQL_SEARCH_BY_SEARCH_CRITERIA_BY_PAGE_ENDING = " GROUP BY (NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE) "
			+ "ORDER BY COUNT (COMMENTS.COMMENT_ID)DESC, NEWS.MODIFICATION_DATE DESC ))"
			+ " where rn > ? AND rn <= ?";

	private static final String SQL_SEARCH_BY_SEARCH_CRITERIA_ENDING = " GROUP BY "
			+ "(NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE)"
			+ " ORDER BY COUNT (COMMENTS.COMMENT_ID)DESC, NEWS.MODIFICATION_DATE DESC";

	private static final String SQL_READ_NEWS_BY_PAGE_QUERY = "SELECT NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE,"
			+ " MODIFICATION_DATE FROM (SELECT NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, "
			+ "MODIFICATION_DATE, ROWNUM as RN FROM "
			+ "( SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, "
			+ " NEWS.MODIFICATION_DATE FROM NEWS LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY"
			+ " (NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE)"
			+ " ORDER BY COUNT(COMMENTS.COMMENT_ID)DESC, NEWS.MODIFICATION_DATE DESC)) where RN > ? AND RN <= ?";

	private static final String SQL_GET_SORTED_NEWS_QUERY = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS "
			+ "LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY (NEWS.NEWS_ID, NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE) "
			+ "ORDER BY COUNT(COMMENTS.COMMENT_ID)DESC, NEWS.MODIFICATION_DATE DESC";

	public static class BuilderWithoutPageOrder {

		public static String buildQuery(SearchCriteria searchCriteria) {

			String query = null;

			if (searchCriteria == null) {
				query = SQL_GET_SORTED_NEWS_QUERY;
			} else if (searchCriteria.getAuthorId() == null
					&& searchCriteria.getIdTagsList() != null) {
				query = buildQueryForTagList(searchCriteria.getIdTagsList());
			}

			else if (searchCriteria.getAuthorId() != null
					&& searchCriteria.getIdTagsList() == null) {
				query = buildQueryForAuthor(searchCriteria.getAuthorId());
			}

			else {
				query = buildQueryForTagsAndAuthor(
						searchCriteria.getAuthorId(),
						searchCriteria.getIdTagsList());
			}

			return query;

		}

		private static String buildQueryForTagList(List<Long> tagList) {
			StringBuilder scriptBuilder = new StringBuilder();

			scriptBuilder.append(SQL_SEARCH_BY_SEARCH_CRITERIA_BEGINING);

			for (Long tagId : tagList) {
				scriptBuilder.append("NEWS_TAG.TAG_ID=" + tagId + " OR ");
			}

			scriptBuilder.delete(scriptBuilder.length() - 3,
					scriptBuilder.length());

			scriptBuilder.append(SQL_SEARCH_BY_SEARCH_CRITERIA_ENDING);

			return new String(scriptBuilder);
		}

		private static String buildQueryForTagsAndAuthor(Long authorId,
				List<Long> tagList) {
			StringBuilder scriptBuilder = new StringBuilder();

			scriptBuilder.append(SQL_SEARCH_BY_SEARCH_CRITERIA_BEGINING
					+ " AUTHOR_ID = " + authorId + " OR ");

			for (Long tagId : tagList) {
				scriptBuilder.append("NEWS_TAG.TAG_ID=" + tagId + " OR ");
			}

			scriptBuilder.delete(scriptBuilder.length() - 3,
					scriptBuilder.length());

			scriptBuilder.append(SQL_SEARCH_BY_SEARCH_CRITERIA_ENDING);

			return new String(scriptBuilder);
		}

		private static String buildQueryForAuthor(Long authorId) {
			StringBuilder scriptBuilder = new StringBuilder();

			scriptBuilder.append(SQL_SEARCH_BY_SEARCH_CRITERIA_BEGINING
					+ " AUTHOR_ID = " + authorId);

			scriptBuilder.append(SQL_SEARCH_BY_SEARCH_CRITERIA_ENDING);

			return new String(scriptBuilder);
		}

	}

	public static class BuilderByPageOrder {

		public static String buildQuery(SearchCriteria searchCriteria) {
			String query = null;

			if (searchCriteria == null) {
				query = SQL_READ_NEWS_BY_PAGE_QUERY;
			} else if (searchCriteria.getAuthorId() == null
					&& searchCriteria.getIdTagsList() != null) {
				query = buildQueryForTagList(searchCriteria.getIdTagsList());
			}

			else if (searchCriteria.getAuthorId() != null
					&& searchCriteria.getIdTagsList() == null) {
				query = buildQueryForAuthor(searchCriteria.getAuthorId());
			}

			else {
				query = buildQueryForTagsAndAuthor(
						searchCriteria.getAuthorId(),
						searchCriteria.getIdTagsList());
			}

			return query;
		}

		/**
		 * Public static method that is used for constructing SQL-queries, when
		 * news are searched by list of tags only.
		 * 
		 * @param tagList
		 * @return String which is the SQL query
		 */
		private static String buildQueryForTagList(List<Long> tagList) {
			StringBuilder scriptBuilder = new StringBuilder();

			scriptBuilder
					.append(SQL_SEARCH_BY_SEARCH_CRITERIA_BY_PAGE_BEGINING);

			for (Long tagId : tagList) {
				scriptBuilder.append("NEWS_TAG.TAG_ID=" + tagId + " OR ");
			}

			scriptBuilder.delete(scriptBuilder.length() - 3,
					scriptBuilder.length());

			scriptBuilder.append(SQL_SEARCH_BY_SEARCH_CRITERIA_BY_PAGE_ENDING);

			return new String(scriptBuilder);
		}

		/**
		 * Public static method that is used for constructing SQL-queries, when
		 * news are searched by list of tags and Author.
		 * 
		 * @param tagList
		 * @param authorId
		 * @return String which is the SQL query
		 */
		private static String buildQueryForTagsAndAuthor(Long authorId,
				List<Long> tagList) {
			StringBuilder scriptBuilder = new StringBuilder();

			scriptBuilder.append(SQL_SEARCH_BY_SEARCH_CRITERIA_BY_PAGE_BEGINING
					+ " AUTHOR_ID = " + authorId + " OR ");

			for (Long tagId : tagList) {
				scriptBuilder.append("NEWS_TAG.TAG_ID=" + tagId + " OR ");
			}

			scriptBuilder.delete(scriptBuilder.length() - 3,
					scriptBuilder.length());

			scriptBuilder.append(SQL_SEARCH_BY_SEARCH_CRITERIA_BY_PAGE_ENDING);

			return new String(scriptBuilder);
		}

		private static String buildQueryForAuthor(Long authorId) {

			StringBuilder scriptBuilder = new StringBuilder();

			scriptBuilder.append(SQL_SEARCH_BY_SEARCH_CRITERIA_BY_PAGE_BEGINING
					+ " AUTHOR_ID = " + authorId);

			scriptBuilder.append(SQL_SEARCH_BY_SEARCH_CRITERIA_BY_PAGE_ENDING);

			return new String(scriptBuilder);
		}

	}

	public static class BuilderForCheckingNextNews {

		private static final String SQL_CHECK_NEXT_NEWS = "SELECT NEXT_NEWS FROM "
				+ "(SELECT NEWS_ID, LEAD(NEWS_ID, 1) OVER (ORDER BY ROWNUM) AS NEXT_NEWS FROM("
				+ "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE,"
				+ " NEWS.MODIFICATION_DATE FROM NEWS "
				+ "LEFT OUTER JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID "
				+ "GROUP BY (NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, "
				+ "NEWS.MODIFICATION_DATE) "
				+ "ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC) ) "
				+ "WHERE NEWS_ID = ?";

		private static final String SQL_CHECK_NEXT_NEWS_WITH_SEARCH_CRETERIA_BEGINING = "SELECT NEXT_NEWS FROM "
				+ "(SELECT NEWS_ID, LEAD(NEWS_ID, 1) OVER (ORDER BY ROWNUM) AS NEXT_NEWS FROM"
				+ "(SELECT NEWS.NEWS_ID,NEWS.SHORT_TEXT,NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE,"
				+ "NEWS.MODIFICATION_DATE FROM NEWS "
				+ "LEFT OUTER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID "
				+ "LEFT OUTER JOIN NEWS_TAG ON NEWS.NEWS_ID=NEWS_TAG.NEWS_ID "
				+ "LEFT OUTER JOIN COMMENTS ON NEWS.NEWS_ID=COMMENTS.NEWS_ID "
				+ "WHERE ";

		private static final String SQL_CHECK_NEXT_NEWS_WITH_SEARCH_CRETERIA_ENDING = "GROUP BY "
				+ "(NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE) "
				+ "ORDER BY COUNT (COMMENTS.COMMENT_ID)DESC, NEWS.MODIFICATION_DATE DESC)) "
				+ "WHERE NEWS_ID = ?";
				
				
		public static String buildQuery(SearchCriteria searchCriteria) {
			String query = null;

			if (searchCriteria == null) {
				query = SQL_CHECK_NEXT_NEWS;
			} else if (searchCriteria.getAuthorId() == null
					&& searchCriteria.getIdTagsList() != null) {
				query = buildQueryForTagList(searchCriteria.getIdTagsList());
			}

			else if (searchCriteria.getAuthorId() != null
					&& searchCriteria.getIdTagsList() == null) {
				query = buildQueryForAuthor(searchCriteria.getAuthorId());
			}

			else {
				query = buildQueryForTagsAndAuthor(
						searchCriteria.getAuthorId(),
						searchCriteria.getIdTagsList());
			}

			return query;
		}

		/**
		 * Public static method that is used for constructing SQL-queries, when
		 * news are searched by list of tags only.
		 * 
		 * @param tagList
		 * @return String which is the SQL query
		 */
		private static String buildQueryForTagList(List<Long> tagList) {
			StringBuilder scriptBuilder = new StringBuilder();

			scriptBuilder
					.append(SQL_CHECK_NEXT_NEWS_WITH_SEARCH_CRETERIA_BEGINING);

			for (Long tagId : tagList) {
				scriptBuilder.append("NEWS_TAG.TAG_ID=" + tagId + " OR ");
			}

			scriptBuilder.delete(scriptBuilder.length() - 3,
					scriptBuilder.length());

			scriptBuilder.append(SQL_CHECK_NEXT_NEWS_WITH_SEARCH_CRETERIA_ENDING);

			return new String(scriptBuilder);
		}

		/**
		 * Public static method that is used for constructing SQL-queries, when
		 * news are searched by list of tags and Author.
		 * 
		 * @param tagList
		 * @param authorId
		 * @return String which is the SQL query
		 */
		private static String buildQueryForTagsAndAuthor(Long authorId,
				List<Long> tagList) {
			StringBuilder scriptBuilder = new StringBuilder();

			scriptBuilder.append(SQL_CHECK_NEXT_NEWS_WITH_SEARCH_CRETERIA_BEGINING
					+ " AUTHOR_ID = " + authorId + " OR ");

			for (Long tagId : tagList) {
				scriptBuilder.append("NEWS_TAG.TAG_ID=" + tagId + " OR ");
			}

			scriptBuilder.delete(scriptBuilder.length() - 3,
					scriptBuilder.length());

			scriptBuilder.append(SQL_CHECK_NEXT_NEWS_WITH_SEARCH_CRETERIA_ENDING);

			return new String(scriptBuilder);
		}

		private static String buildQueryForAuthor(Long authorId) {

			StringBuilder scriptBuilder = new StringBuilder();

			scriptBuilder.append(SQL_CHECK_NEXT_NEWS_WITH_SEARCH_CRETERIA_BEGINING
					+ " AUTHOR_ID = " + authorId);

			scriptBuilder.append(SQL_CHECK_NEXT_NEWS_WITH_SEARCH_CRETERIA_ENDING);

			return new String(scriptBuilder);
		}

	}

	public static class BuilderForCheckingPreviousNews {

		private static final String SQL_CHECK_PREVIOUS_NEWS = "SELECT NEXT_NEWS FROM "
				+ "(SELECT NEWS_ID, LAG(NEWS_ID, 1) OVER (ORDER BY ROWNUM) AS NEXT_NEWS FROM("
				+ "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE,"
				+ " NEWS.MODIFICATION_DATE FROM NEWS "
				+ "LEFT OUTER JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID "
				+ "GROUP BY (NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, "
				+ "NEWS.MODIFICATION_DATE) "
				+ "ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC) ) "
				+ "WHERE NEWS_ID = ?";

		private static final String SQL_CHECK_PREVIOUS_NEWS_WITH_SEARCH_CRETERIA_BEGINING = "SELECT NEXT_NEWS FROM "
				+ "(SELECT NEWS_ID, LAG(NEWS_ID, 1) OVER (ORDER BY ROWNUM) AS NEXT_NEWS FROM"
				+ "(SELECT NEWS.NEWS_ID,NEWS.SHORT_TEXT,NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE,"
				+ "NEWS.MODIFICATION_DATE FROM NEWS "
				+ "LEFT OUTER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID "
				+ "LEFT OUTER JOIN NEWS_TAG ON NEWS.NEWS_ID=NEWS_TAG.NEWS_ID "
				+ "LEFT OUTER JOIN COMMENTS ON NEWS.NEWS_ID=COMMENTS.NEWS_ID "
				+ "WHERE ";

		private static final String SQL_CHECK_PREVIOUS_NEWS_WITH_SEARCH_CRETERIA_ENDING = "GROUP BY "
				+ "(NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE) "
				+ "ORDER BY COUNT (COMMENTS.COMMENT_ID)DESC, NEWS.MODIFICATION_DATE DESC)) "
				+ "WHERE NEWS_ID = ?";
				
				
		public static String buildQuery(SearchCriteria searchCriteria) {
			String query = null;

			if (searchCriteria == null) {
				query = SQL_CHECK_PREVIOUS_NEWS;
			} else if (searchCriteria.getAuthorId() == null
					&& searchCriteria.getIdTagsList() != null) {
				query = buildQueryForTagList(searchCriteria.getIdTagsList());
			}

			else if (searchCriteria.getAuthorId() != null
					&& searchCriteria.getIdTagsList() == null) {
				query = buildQueryForAuthor(searchCriteria.getAuthorId());
			}

			else {
				query = buildQueryForTagsAndAuthor(
						searchCriteria.getAuthorId(),
						searchCriteria.getIdTagsList());
			}

			return query;
		}

		/**
		 * Public static method that is used for constructing SQL-queries, when
		 * news are searched by list of tags only.
		 * 
		 * @param tagList
		 * @return String which is the SQL query
		 */
		private static String buildQueryForTagList(List<Long> tagList) {
			StringBuilder scriptBuilder = new StringBuilder();

			scriptBuilder
					.append(SQL_CHECK_PREVIOUS_NEWS_WITH_SEARCH_CRETERIA_BEGINING);

			for (Long tagId : tagList) {
				scriptBuilder.append("NEWS_TAG.TAG_ID=" + tagId + " OR ");
			}

			scriptBuilder.delete(scriptBuilder.length() - 3,
					scriptBuilder.length());

			scriptBuilder.append(SQL_CHECK_PREVIOUS_NEWS_WITH_SEARCH_CRETERIA_ENDING);

			return new String(scriptBuilder);
		}

		/**
		 * Public static method that is used for constructing SQL-queries, when
		 * news are searched by list of tags and Author.
		 * 
		 * @param tagList
		 * @param authorId
		 * @return String which is the SQL query
		 */
		private static String buildQueryForTagsAndAuthor(Long authorId,
				List<Long> tagList) {
			StringBuilder scriptBuilder = new StringBuilder();

			scriptBuilder.append(SQL_CHECK_PREVIOUS_NEWS_WITH_SEARCH_CRETERIA_BEGINING
					+ " AUTHOR_ID = " + authorId + " OR ");

			for (Long tagId : tagList) {
				scriptBuilder.append("NEWS_TAG.TAG_ID=" + tagId + " OR ");
			}

			scriptBuilder.delete(scriptBuilder.length() - 3,
					scriptBuilder.length());

			scriptBuilder.append(SQL_CHECK_PREVIOUS_NEWS_WITH_SEARCH_CRETERIA_ENDING);

			return new String(scriptBuilder);
		}

		private static String buildQueryForAuthor(Long authorId) {

			StringBuilder scriptBuilder = new StringBuilder();

			scriptBuilder.append(SQL_CHECK_PREVIOUS_NEWS_WITH_SEARCH_CRETERIA_BEGINING
					+ " AUTHOR_ID = " + authorId);

			scriptBuilder.append(SQL_CHECK_PREVIOUS_NEWS_WITH_SEARCH_CRETERIA_ENDING);

			return new String(scriptBuilder);
		}

	}
}
