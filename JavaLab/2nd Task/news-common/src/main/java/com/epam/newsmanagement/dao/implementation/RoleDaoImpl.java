package com.epam.newsmanagement.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.RoleDao;
import com.epam.newsmanagement.domain.Role;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.utils.DataBaseUtil;

/**
 * Realization of using daoMethods for working with ROLES table in database
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.domain.Role
 * @since 1.0
 */
@Repository
public class RoleDaoImpl implements RoleDao{

	/**
	 * Object which used for connecting to the database
	 */
	@Autowired
	private DataBaseUtil dataBaseUtil;
	
	/**
	 * Sql query for reading users roles from database by login.
	 */
	private String SQL_READ_ROLES = "SELECT ROLES.USER_ID, ROLES.ROLE_NAME FROM ROLES JOIN USERS ON USERS.USER_ID = ROLES.USER_ID WHERE LOGIN = ?";
	
	/**
	 * Reading roles for user from database by login value.
	 *
	 * @return list of entity records to read
	 * @throws com.epam.newsmanagement.exception.DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	@Override
	public List<Role> readRolesByLogin(String login) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		List<Role> usersRoles = new ArrayList<Role>();
		try{
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ_ROLES);
			
			preparedStatement.setString(1, login);
			
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				Role role = new Role();

				role.setUserId(resultSet.getLong(1));
				role.setRoleName(resultSet.getString(2));
				
				usersRoles.add(role);
			}
			return usersRoles;
		}catch(SQLException e){
			throw new DaoException("Cannot read roles for user" + login, e);
		}
	}

}
