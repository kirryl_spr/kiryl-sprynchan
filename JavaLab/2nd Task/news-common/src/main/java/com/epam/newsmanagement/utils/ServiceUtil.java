package com.epam.newsmanagement.utils;

import com.epam.newsmanagement.exception.ServiceException;

public class ServiceUtil {
	/**
	 *Override method for checking whether the object is null.
	 *If it is true throws exception with message
	 *
	 * @param obj Object
	 * @param String message
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public static void isObjectNull(Object obj, String message)
			throws ServiceException {
		if (obj == null) {
			throw new ServiceException(message);
		}
	}
}
