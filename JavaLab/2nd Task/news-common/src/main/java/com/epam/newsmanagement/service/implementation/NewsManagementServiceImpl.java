package com.epam.newsmanagement.service.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.PostedNews;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

/**
 * Public class <code>NewsManagementServiceImpl<code/> is an
 * implementation of interface <code>NewsManagementService</code> and overrides
 * all methods describing transactional operations for which, you must use a few
 * simple Service layer interfaces.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.service.NewsManagementService
 * @since 1.0
 */
@Transactional(rollbackFor=ServiceException.class)
public class NewsManagementServiceImpl implements NewsManagementService{
	
	/**
	 * author instance for using all service methods
	 */
	@Autowired
	private AuthorService authorService;
	
	/**
	 * comment instance for using all service methods
	 */
	@Autowired
	private CommentService commentService;
	
	/**
	 * news instance for using all service methods
	 */
	@Autowired
	private NewsService newsService;

	/**
	 * news instance for using all service methods
	 */
	@Autowired
	private TagService tagService;
	
	/**
	 * Override method used to save news with list of binding tags and author in one step. 
	 * This is a transactional operation.
	 *
	 * @param postedNews
	 *            PostedNews instance for saving in database
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */

	public Long saveNews(PostedNews postedNews) throws ServiceException {
		Long newsId = newsService.create(postedNews.getNews());
		
		postedNews.getNews().setNewsId(newsId);
		
		newsService.saveNewsAuthor(postedNews.getNews(), postedNews.getAuthor());
		newsService.saveNewsTagList(postedNews.getNews(), postedNews.getTagList());
		
		return newsId;
	}
	
	/**
	 * Override method used to update news with list of binding tags and author in one step. 
	 * This is a transactional operation.
	 *
	 * @param postedNews
	 *            PostedNews instance for saving in database
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */

	public void updateNews(PostedNews postedNews) throws ServiceException {
		newsService.update(postedNews.getNews());
		
		newsService.updateNewsAuthor(postedNews.getNews(), postedNews.getAuthor());
		tagService.deleteNewsTagByNews(postedNews.getNews().getNewsId());
		newsService.saveNewsTagList(postedNews.getNews(), postedNews.getTagList());
	}
	
	/**
	 * Override method used to delete news in one step. 
	 * This is a transactional operation.
	 *
	 * @param postedNews
	 *            PostedNews instance for saving in database
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */

	public void deleteNews(Long newsId) throws ServiceException{
		try{
			commentService.deleteAllCommentsForNews(newsId);
			authorService.deleteNewsAuthor(newsId);
			newsService.deleteNewsTag(newsId);
			newsService.delete(newsId);
		}catch(ServiceException e){
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	/**
	 * Reading news on current page according to search criteria
	 *
	 * @param pageNumber of int what is describing number of a current page
	 * 
	 * @param newsOnPageCount of int describing quantity of news in all
	 * @param searchCriteria of SearchCriteria instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public List<PostedNews> readNewsOnPage(int pageNumber, int newsOnPageCount, SearchCriteria searchCriteria) throws ServiceException {
		Author author = null;
		PostedNews fullNewsVO = null;
		List<Tag> tagList = null;
		List<Comment> commentList = null;
		
		List<PostedNews> fullNewsList = new ArrayList<PostedNews>();
		List<News> newsList = newsService.readNewsByPageNumber(pageNumber, newsOnPageCount, searchCriteria);
		
		for(News news : newsList) {
			
			author = authorService.searchAuthorByNews(news);
			tagList = tagService.searchTagsByNews(news);
			commentList = commentService.searchCommentsByNews(news);
		
			fullNewsVO = new PostedNews(news, author, tagList, commentList);
			fullNewsList.add(fullNewsVO);
		}
		
		return fullNewsList;
		
	}
	
	/**
	 * Reading news single news
	 *
	 * @param newsId of Long instance what is describing ID of news
	 * 
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public PostedNews readSingleNews(Long newsId) throws ServiceException{
		News news = newsService.read(newsId);
		Author author = authorService.searchAuthorByNews(news);
		List<Tag> tagList = tagService.searchTagsByNews(news);
		List<Comment> commentList = commentService.searchCommentsByNews(news);
		
		PostedNews singleNews = new PostedNews(news, author, tagList, commentList);
		
		return singleNews;
	}
}
