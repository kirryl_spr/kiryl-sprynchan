package com.epam.newsmanagement.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.utils.DataBaseUtil;

/**
 * Public class <code>AuthorDaoImpl</code> is an element of Data Access layer
 * and working with Author data base instance. This is a singleton realization
 * of <code>AuthorDao</code> interface and it performs all operations described
 * in this interface. Realized pattern Singleton.
 *
 * @author Kiryl Sprynchan
 * @version 1.0
 * @see com.epam.newsmanagement.dao.CommentDao
 * @see com.epam.newsmanagement.domain.Author
 * @since 1.0
 */
@Repository
public class AuthorDaoImpl implements AuthorDao{
	
	/**
	 * Object which used for connecting to the database
	 */
	@Autowired
	 private DataBaseUtil dataBaseUtil;
	
	/**
	 * SQL query for create author
	 */
	private static final String SQL_CREATE_QUERY = "INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (AUTHOR_ID_SEQ.NEXTVAL, ?)";

	/**
	 * SQL query for read author by id
	 */
	private static final String SQL_READ_QUERY = "SELECT AUTHOR_ID, AUTHOR_NAME FROM AUTHOR WHERE AUTHOR_ID = ?";
	
	/**
	 * SQL query for readAll author records
	 */
	private static final String SQL_READ_ALL_QUERY = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR ORDER BY AUTHOR_ID";

	/**
	 * SQL query for update author
	 */
	private static final String SQL_UPDATE_QUERY = "UPDATE AUTHOR SET AUTHOR_NAME = ? WHERE AUTHOR_ID = ?";
	
	/**
	 * SQL query for delete author
	 */
	private static final String SQL_EXPIRE_QUERY = "UPDATE AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";

	/**
	 * SQL query for delete author
	 */
	private static final String SQL_UNEXPIRE_QUERY = "UPDATE AUTHOR SET EXPIRED = NULL WHERE AUTHOR_ID = ?";

	
	/**
	 * SQL query for delete association news with author
	 */
	private static final String SQL_DELETE_NEWS_AUTHOR_QUERY = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID =?";

	
	/**
	 * SQL query for searching author by news
	 */
	private static final String SQL_SEARCH_BY_NEWS_QUERY = "SELECT AUTHOR.AUTHOR_ID, AUTHOR.AUTHOR_NAME FROM AUTHOR "
			+ "INNER JOIN NEWS_AUTHOR on AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID WHERE NEWS_AUTHOR.NEWS_ID = ? "
			+ "ORDER BY AUTHOR_ID";
	
	
	/**
	 * Override public method used to add record to the table Author.
	 *
	 * @param author
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public Long create(Author author) throws DaoException {
		String columnAuthorId = "AUTHOR_ID";
		Long authorId = null;

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		String generatedColumns[] = { columnAuthorId };
		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE_QUERY,
					generatedColumns);
			preparedStatement.setString(1, author.getAuthorName());
			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				authorId = resultSet.getLong(1);
			}

		} catch (SQLException e) {
			throw new DaoException("SQL error in create 'author' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}

		return authorId;
	}

	
	/**
	 * Override public method used to read a record from the table Author.
	 *
	 * @param authorId
	 *            unique identifier of instance which will be found
	 * @return find Author instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public Author read(Long authorId) throws DaoException{
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Author author = null;
		
		try{
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_READ_QUERY);
			preparedStatement.setLong(1, authorId);
			
			resultSet = preparedStatement.executeQuery();
			
			if(resultSet.next()){
				author = new Author();
				
				author.setAuthorId(resultSet.getLong(1));
				author.setAuthorName(resultSet.getString(2));
			}
			
			return author;
			
		} catch(SQLException e){
			throw new DaoException("SQL error in create 'author' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}

	
	}

	/**
	 * Override public method used to read all records from the table Author.
	 *
	 * @return list of find Author instances
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public List<Author> readAll() throws DaoException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		List<Author> authors = null;
		Author author = null;

		try {
			connection = dataBaseUtil.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_READ_ALL_QUERY);

			authors = new ArrayList<Author>();
			while (resultSet.next()) {
				author = new Author();

				author.setAuthorId(resultSet.getLong(1));
				author.setAuthorName(resultSet.getString(2));
				author.setExpired(resultSet.getDate(3));

				authors.add(author);
			}
			return authors;

		} catch (SQLException e) {
			throw new DaoException("SQL error in readAll 'authors' operation.",
					e);
		} finally {
			dataBaseUtil.closeResources(resultSet, statement, connection);
		}
	}

	/**
	 * Override public method used to update a record in the table Author.
	 *
	 * @param author
	 *            instance which will be update in the data base
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void update(Author author) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_QUERY);
			preparedStatement.setString(1, author.getAuthorName());
			preparedStatement.setLong(2, author.getAuthorId());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in update 'author' operation", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
		
	}

	/**
	 * Override public method used to delete a record from the table Author.
	 *
	 * @param authorId
	 *            unique identifier of instance which will be delete
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void delete(Long authorId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_EXPIRE_QUERY);

			preparedStatement.setTimestamp(1, new Timestamp(new Date().getTime()));
			preparedStatement.setLong(2, authorId);

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in expireing 'author'operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
		
	}

	/**
	 * Override method used to search Author record from the table Author by
	 * News instance.
	 *
	 * @param author
	 *            element of News instance to search for Author instance
	 * @return Author which was find by the News instance
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 * @see java.sql.SQLException
	 */
	public Author searchAuthorByNews(News news) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection
					.prepareStatement(SQL_SEARCH_BY_NEWS_QUERY);

			preparedStatement.setLong(1, news.getNewsId());

			resultSet = preparedStatement.executeQuery();

			Author author = new Author();
			while (resultSet.next()) {

				author.setAuthorId(resultSet.getLong(1));
				author.setAuthorName(resultSet.getString(2));
			}

			return author;

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in search 'author' by news operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}
	
	/**
	 * Override method used to delete data from table NEWS_AUTHOR 
	 * when news is deleting
	 *
	 * @param newsId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.News
	 */
	public void deleteNewsAuthor(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR_QUERY);

			preparedStatement.setLong(1, newsId);

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in delete 'author'operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Making author unexpired
	 *
	 * @param authorId
	 *            element of Long instance to delete necessary rows
	 * @throws DaoException
	 *             user-defined exception occurs when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsmanagement.domain.Author
	 */
	@Override
	public void unexpireAuthor(Long authorId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataBaseUtil.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UNEXPIRE_QUERY);

			preparedStatement.setLong(1, authorId);

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in unexpiring 'author' operation.", e);
		} finally {
			dataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
		
	}

}
