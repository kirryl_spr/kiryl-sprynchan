<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
</head>
<body>

	<div class="login-container">
		<c:if test="${not empty error}">
			<div class="error"><spring:message
						code="label.error_login" /></div>
		</c:if>
		<c:if test="${not empty msg}">
			<div class="msg"><spring:message
						code="label.logout_succes" /></div>
		</c:if>

		<form name="loginForm" action="j_spring_security_check" method="post"
			id="loginForm">

			<table>
				<tr>
					<td><big><spring:message
						code="label.login" />:</big></td>
					<td><input type='text' name='j_username' value='' autofocus></td>
				</tr>
				<tr>
					<td><big><spring:message
						code="label.password" />:</big></td>
					<td><input type='password' name='j_password'/></td>
				</tr>
			</table>
			<div class="login-button">
				<input class="login-button" type="button" value="<spring:message
						code="label.login_button" />" />
			</div>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
		<div id="hidden-messages">		
			<div id="login-hidden-message" style="display: none;">
				<spring:message code="insert.login" />
			</div>
			<div id="password-hidden-message" style="display: none;">
				<spring:message code="insert.password" />
			</div>
		</div>

	</div>
</body>

<script>
$(".login-button").on("click", function() {
	
	$('#login-hidden-message').hide();
	$('#password-hidden-message').hide();
	
	var login = $('input[name=j_username]').val();
	var password = $('input[name=j_password]').val();
	
	if(login.length == 0)
		$('#login-hidden-message').show(500);
	if(password.length == 0)
		$('#password-hidden-message').show(500);
	else if(login.length != 0 || password.length != 0){
		$('#loginForm').submit();
		}
});
</script>
</html>