<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${singleNews.news.title}</title>
</head>
<body>
	<div class="show-single-news-content">
		<div id="show-single-news-content">
			<div class="head-information-news">
				<div class="news-title"><c:out value="${singleNews.news.title}"/></div>

				<div class="author-date">
					<a id="author">(<spring:message code="label.by"/> ${singleNews.author.authorName})</a> <a
						id="date">
						
						<fmt:formatDate timeZone="GMT+${timeZone}" type="both" 
            					dateStyle="short" pattern="YYYY-MM-dd HH:mm"
						value="${singleNews.news.creationDate}"/>
						</a>
				</div>
			</div>

			<div class="full-text"><c:out value="${singleNews.news.fullText}"/></div>

			<c:forEach items="${singleNews.commentList}" var="comment">
				<div class="comment-content">
					<div class="single-comment-admin">
						<a id="comment-date"><c:out value="${comment.creationDate}"/></a>
						<div class="comment-text">
							<form action="delete-comment" id="delete-comment" method="POST">
								<input type="submit" value="X"> <input type="hidden"
									name="newsId" value="${singleNews.news.newsId}"> <input
									type="hidden" name="commentId" value="${comment.commentId}">
							</form>
							<div class="comment-text-value"><c:out value="${comment.commentText}"/></div>
						</div>
					</div>
				</div>
			</c:forEach>

			<div class="adding-comment-hidden" style="display: none;"><spring:message code="empty_comment"/></div>
			<div class="add-comment">
				<form action="add-comment" method="POST" id="add-comment">
					<textarea name="commentText" id="new-comment" maxlength="100"></textarea>
					<input type="hidden" name="newsId"
						value="${singleNews.news.newsId}"> <input type="button"
						id="post-comment" value="<spring:message code="label.post_comment.button"/>">
				</form>
				
			</div>
		</div>
		
		<div class="change-news">
			<c:if test="${previousNews ne 0}">
				<a onclick="changeNews('${previousNews}')" id="prev-link">
					<spring:message code="label.previous"/>
				</a>
			</c:if>

			<c:if test="${nextNews ne 0}">
				<a onclick="changeNews('${nextNews}')" id="next-link">
					<spring:message code="label.next"/>
				</a>
			</c:if>
		</div>

	</div>
	<script>
	$("#post-comment").on("click", function() {
		var commentText = $("#new-comment").val();
		if (commentText.length > 100 || commentText.length == 0) {
					$(".adding-comment-hidden:hidden").show(); 	
			
		} else {
			$('#add-comment').submit();
		}
	});
	
		function changeNews(newsId){
			var x = new Date();
			document.location.href = "${pageContext.request.contextPath}/show-single-news?newsId="+newsId+"&timeZone="+-x.getTimezoneOffset()/60;
		}
	</script>
</body>
</html>