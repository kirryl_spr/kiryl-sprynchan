<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News-Portal Administration</title>
</head>
<body>
	<div class="show-news-content">
		<div class="search-criteria">
			<div class="unchosen">
				<div id="unchosen-hidden" style="display: none;">
					<spring:message code="label.choose_something" />
					!!!
				</div>
			</div>

			<form action="filter-news" id="filter-news" name="filterForm">

				<select name="author" id="authorSelect">
					<option value="" disabled selected><spring:message
							code="label.select_author" />:
					</option>

					<c:forEach items="${authorList}" var="author">
						<option value="${author.authorId}"><c:out value="${author.authorName}"/></option>
					</c:forEach>

				</select> <select id="tagSelect" class="multi-select" multiple name="tagList"
					data-placeholder="<spring:message
							code="label.select_tags" />">
					<c:forEach items="${tagList}" var="tag">
						<option value="${tag.tagId }"><c:out value="${tag.tagName}"/></option>
					</c:forEach>
				</select>
				<input type="button" id="filter-button"
					value="<spring:message code="label.filter.button"/>"
					name="filterButton">

			</form>

			<form action="reset-searchCriteria" id="reset-reset-searchCriteria">
				<input type="submit"
					value="<spring:message code="label.reset.button"/>">
			</form>
		</div>


		<c:if test="${fn:length(fullNewsList) == 0 }">
			<a id="no-news-label"><spring:message code="label.no_news" /></a>
		</c:if>

		<c:if test="${fn:length(fullNewsList) != 0 }">
			<form action="delete-news" name="deleteNews" method="POST">

				<c:forEach items="${fullNewsList}" var="fullNews">

					<div class="news">
						<div class="news-title-link">
							<a id="news-title-link" onclick="goToShowSingleNews('${fullNews.news.newsId}')"
								><c:out value="${fullNews.news.title}"/></a>
						</div>

						<div class="by-author-label">
							(
							<spring:message code="label.by" />
							<c:out value="${fullNews.author.authorName}"/> )
						</div>

						<div class="creation-date-label">${fullNews.news.modificationDate}</div>

						<div class="short-text"><c:out value="${fullNews.news.shortText}"/> </div>

						<div class="data-about-news">
							<div class="news-tags">
								<c:forEach items="${fullNews.tagList}" var="tag">
						<c:out value="${tag.tagName}"/>,
					</c:forEach>
							</div>

							<div class="news-comments">
								<spring:message code="label.comments" />
								(${fn:length(fullNews.commentList)})
							</div>

							<div class="edit-news-checkbox">
								<a
									onclick="goToEditNewsPage('${fullNews.news.newsId}')">
									<spring:message code="label.Edit" />
								</a> <input type="checkbox" id="edit-news" name="edit"
									value="${fullNews.news.newsId}"
									onClick="checkStateForDelete();">
							</div>
						</div>

					</div>

				</c:forEach>

				<input type="submit" class="delete-news-button" name="deleteButton"
					value="<spring:message code="label.delete.button"/>" disabled>
			</form>
		</c:if>

		<div class="paginal-nav-bar">
			<form action="show-news">
				<c:forEach var="i" begin="1" end="${pagesCount}">
					<c:choose>
						<c:when test="${pageNumber eq i}">
							<input type="submit" id="current-page" value="${i}"
								name="pageNumber" />
						</c:when>

						<c:otherwise>
							<input type="submit" value="${i}" name="pageNumber" />
						</c:otherwise>
					</c:choose>

				</c:forEach>
			</form>
		</div>
	</div>
	<script>
		$(document).ready(selectedTags('${searchCriteria.idTagsList}'));
		$(document).ready(selectedAuthor('${searchCriteria.authorId}'));

		"use strict";
		$(".multi-select").chosen();

		$("#filter-button").on("click", function() {

			var tags = $(".search-choice");

			if ($("#authorSelect").val() || tags.length != 0) {
				$("#filter-news").submit();
			} else {
				$("#unchosen-hidden").show(500);
			}
		})

		function goToShowSingleNews(newsId){
			var x = new Date();
			document.location.href = "${pageContext.request.contextPath}/show-single-news?newsId="+newsId+"&timeZone="+-x.getTimezoneOffset()/60;
		}
		
		function goToEditNewsPage(newsId){
			var x = new Date();
			window.location.href = "${pageContext.request.contextPath}/editing-news?newsId="+newsId+"&timeZone="+-x.getTimezoneOffset()/60;
		}
	</script>
</body>
</html>