<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>404-page</title>
<script src="resources/script/jquery-1.11.2.min.js"></script>
<script src="resources/script/chosen.jquery.js"></script>
<script src="resources/script/chosen.jquery.min.js"></script>
<script src="resources/script/chosen.proto.js"></script>
<script src="resources/script/chosen.proto.min.js"></script>

<link rel="stylesheet" href="resources/css/chosen.css" type="text/css" />
<link rel="stylesheet" href="resources/css/chosen.min.css"
	type="text/css" />
<link rel="stylesheet" href="resources/css/style.css" type="text/css" />
</head>
<body>
	<div class="main-container">
		<div class="header-container">
			<p class="page-title">
				News portal -
				<spring:message code="label.administration" />
			</p>
			<div class="hello-block">
				<div class="admin-fields">

					<sec:authorize access="isAuthenticated()">
						<p class="hello-admin">
							<spring:message code="label.hello_admin" />
							,
							<security:authentication property="principal.username" />
						</p>
						<form action="logout" class="submit-form">
							<input type="submit"
								value="<spring:message code="label.logout.button"/>"
								class="logout-button">
						</form>

						<span class="localization"> <a href="show-news?lang=en"><spring:message
									code="label.EN" /></a> <a href="show-news?lang=ru"><spring:message
									code="label.RU" /></a>
						</span>
					</sec:authorize>


					<sec:authorize access="!isAuthenticated()">
						<span class="localizationLogin"> <a href="?lang=en"><spring:message
									code="label.EN" /></a> <a href="?lang=ru"><spring:message
									code="label.RU" /></a>
						</span>
					</sec:authorize>


				</div>
			</div>
		</div>

		<div class="error-content"><spring:message
									code="404-error"/></div>


		<div class="footer-container">
			<spring:message code="footer.label" />
		</div>
	</div>
</body>
</html>