function selectedAuthor(authorId) {
	var authorSelect = document.getElementById('authorSelect');
	if (authorId != "") {
		authorSelect[authorId].selected = true;
	}

}

function selectedTags(str) {
	var tagOptions = document.getElementById("tagSelect").options;

	var trimedString = str.substring(1, str.length - 1);

	var regex = /\s*,\s*/;
	var tagsIdList = trimedString.split(regex);

	for (var i = 0; i < tagOptions.length; i++) {
		if (tagsIdList.indexOf(tagOptions[i].value) != -1) {
			tagOptions[i].selected = true;
		}
	}
}



function showButtons(elementId) {
	var author = $("input[id=authorName"+elementId+"]");
	author.prop("disabled", false);
			$("#update"+elementId+":hidden").show();
		$("#expire"+elementId+":hidden").show();	
		$("#cancel"+elementId+":hidden").show();
		$("#hide"+elementId).hide();
}

function hideButtons(elementId) {
	
	var author = $("input[id=authorName"+elementId+"]");
	author.prop("disabled", true);
	$("#update"+elementId).hide();
	$("#expire"+elementId).hide();
	$("#cancel"+elementId).hide();
	$("#hide"+elementId+":hidden").show();
}

function checkStateForDelete() {
	var checkboxes = $('input[type=checkbox]#edit-news:checked');
	if (checkboxes.length != 0) {
		deleteNews.deleteButton.disabled = false;
	} else {
		deleteNews.deleteButton.disabled = true;
	}
}


function checkAuthorNamePoleUpdate(authorId){
	var authorName = $("input[id=authorName"+authorId+"]").val();
	if(authorName.length<=0){
		$("#input-author-hidden-message"+authorId).show(500);
	}
	else{
		$("#edit-author"+authorId).submit();
	}
}

function checkAuthorNamePoleExpire(authorId){
	var authorName = $("input[id=authorName"+authorId+"]").val();
	if(authorName.length<=0){
		$("#input-author-hidden-message"+authorId).show(500);
	}
	else{
		$("#expire-author"+authorId).submit();
	}
}

function checkTagNameUpdate(tagId){
	var tagName = $("input[id=tagName"+tagId+"]").val();
	if(tagName.length<=0){
		$('#input-tag-hidden-message'+tagId).show(500);
	}
	else{
		$("#edit-tag"+tagId).submit();
	}
}

function showButtonsForTag(elementId) {
	var author = $("input[id=tagName"+elementId+"]");
	author.prop("disabled", false);
			$("#update"+elementId+":hidden").show();
		$("#expire"+elementId+":hidden").show();	
		$("#cancel"+elementId+":hidden").show();
		$("#hide"+elementId).hide();
}

function hideButtonsForTag(elementId) {
	
	var author = $("input[id=tagName"+elementId+"]");
	author.prop("disabled", true);
	$("#update"+elementId).hide();
	$("#expire"+elementId).hide();
	$("#cancel"+elementId).hide();
	$("#hide"+elementId+":hidden").show();
}
