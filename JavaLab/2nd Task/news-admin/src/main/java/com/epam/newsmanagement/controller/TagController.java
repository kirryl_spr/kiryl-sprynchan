package com.epam.newsmanagement.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

import static com.epam.newsmanagement.util.ConstantPool.SEARCH_CRITERIA;
import static com.epam.newsmanagement.util.ConstantPool.TAG_LIST;
/**
 * Controller for show-tags page. The controller is in charge of deleting, updating
 * and saving authors record.
 * 
 * @author Kiryl Sprynchan
 *
 */
@Controller
public class TagController {
	
	@Autowired
	private TagService tagService;

	/**
	 * Show show-tags page, with list of all tags.
	 * 
	 * @return show-tags-page
	 * @throws ServiceException
	 */
	@RequestMapping("/show-tags")
	public String showTags(Model model, HttpSession session) throws ServiceException{
		session.removeAttribute(SEARCH_CRITERIA);
		
		setPageInformation(model);
		return "show-tags";
	}
	
	/**
	 * Edit current Tag by id sended from jsp page.
	 * 
	 * @param tagId
	 *            unique identifier of tag entity
	 * @param tagName
	 *            new tag name
	 * @return show-tags-page
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/edit-tag", method = RequestMethod.POST)
	public String editTag(@RequestParam String tagName,@RequestParam Long tagId, Model model) throws ServiceException {

		Tag tag = new Tag();
		tag.setTagId(tagId);
		tag.setTagName(tagName);
		
		tagService.update(tag);
		
		setPageInformation(model);
		return "redirect:/show-tags";
	}
	
	/**
	 * Add  Tag by into databse.
	 * 
	 * @param tagName
	 *            new tag name
	 * @return show-tags-page
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/add-tag", method = RequestMethod.POST)
	public String addTag(@RequestParam String tagName, Model model) throws ServiceException{
		Tag tag = new Tag();
		tag.setTagName(tagName);
		tagService.create(tag);
		
		setPageInformation(model);
		return "redirect:/show-tags";
	}
	
	/**
	 * Delete Tag from databse.
	 * 
	 * @param tagId
	 *            id of tag
	 * @return show-tags-page
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/delete-tag", method = RequestMethod.POST)
	public String deleteTag(@RequestParam Long tagId, Model model, HttpSession session) throws ServiceException{
		tagService.delete(tagId);
		
		setPageInformation(model);
		return "redirect:/show-tags";
	}
	
	/**
	 * Set tag list on the show-tags jsp page.
	 * 
	 * @throws ServiceException
	 */
	private void setPageInformation(Model model) throws ServiceException{
		List<Tag> tagList = tagService.readAll();
		model.addAttribute(TAG_LIST, tagList);
	}
	
}
