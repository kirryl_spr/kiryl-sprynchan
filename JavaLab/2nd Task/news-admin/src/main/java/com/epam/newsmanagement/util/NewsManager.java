package com.epam.newsmanagement.util;

import java.util.List;

import com.epam.newsmanagement.domain.News;

/**
 * NewsManager class, contain all additional methods for news-admin application
 * 
 * @author Kiryl Sprynchan
 *
 */
public class NewsManager {

	/**
	 * Method returned count of news page, for pagination on news-list page
	 * 
	 * @param newsCount
	 *            all news count from database
	 * @param newsOnPageCount
	 *            news count on one page
	 * @return count of pages
	 */
	public static int getPagesCount(List<News> newsList, int newsOnPageCount) {
		int pagesCount = (newsList.size() + (newsOnPageCount - 1))
				/ newsOnPageCount;
		return pagesCount;
	}
	
}
