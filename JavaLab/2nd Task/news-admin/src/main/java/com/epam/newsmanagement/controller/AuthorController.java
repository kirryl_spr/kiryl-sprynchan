package com.epam.newsmanagement.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

import static com.epam.newsmanagement.util.ConstantPool.SEARCH_CRITERIA;
import static com.epam.newsmanagement.util.ConstantPool.AUTHOR_LIST;

/**
 * Controller for show-authors page. The controller is in charge of expiring,
 * updating and saving authors record.
 * 
 * @author Kiryl Sprynchan
 */
@Controller
public class AuthorController {

	@Autowired
	private AuthorService authorService;
	
	/**
	 * Show show-authors page, with list of all authors.
	 * 
	 * @return show-authors page
	 * @throws ServiceException
	 */
	@RequestMapping("/show-authors")
	public String showAuthorList(Model model, HttpSession session) throws ServiceException {
		session.removeAttribute(SEARCH_CRITERIA);
		
		setPageInformation(model);
		return "show-authors";
	}
	
	/**
	 * Add new Author object into data base
	 * 
	 * @param authorName
	 *            author name to adding
	 * @return show-authors page
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/add-author", method = RequestMethod.POST)
	public String addAuthor(@RequestParam String authorName,  Model model) throws ServiceException {
		Author author = new Author();
		author.setAuthorName(authorName);
		
		authorService.create(author);
		
		setPageInformation(model);
		return "redirect:/show-authors";
	}
	
	/**
	 * Editing author name
	 * 
	 * @param authorName
	 *            author name to adding
	 * @return show-authors page
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/edit-author", method = RequestMethod.POST)
	public String editAuthor(@RequestParam String authorName,@RequestParam Long authorId, Model model) throws ServiceException {
		Author author = new Author();
		author.setAuthorId(authorId);
		author.setAuthorName(authorName);
		
		authorService.update(author);
		
		setPageInformation(model);
		return "redirect:/show-authors";
	}
	
	/**
	 * Expire author author
	 * 
	 * @param authorId
	 *            authorId for expiring
	 * @return show-authors page
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/expire-author", method = RequestMethod.POST)
	public String expireAuthor(@RequestParam Long authorId, Model model) throws ServiceException {		
		authorService.delete(authorId);
		
		setPageInformation(model);
		return "redirect:/show-authors";
	}
	
	/**
	 * Unexpire author author
	 * 
	 * @param authorId
	 *            authorId for unexpiring
	 * @return show-authors page
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/unexpire-author", method = RequestMethod.POST)
	public String unexpireAuthor(@RequestParam Long authorId, Model model) throws ServiceException {	
		authorService.unexpireAuthor(authorId);
		
		setPageInformation(model);
		return "redirect:/show-authors";
	}
	
	private void setPageInformation(Model model) throws ServiceException{
		List<Author> authorList = authorService.readAll();
		model.addAttribute(AUTHOR_LIST, authorList);
	}
	
}
