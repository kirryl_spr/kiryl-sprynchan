package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.PostedNews;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.util.NewsManager;

import static com.epam.newsmanagement.util.ConstantPool.NEWS_COUNT_ON_PAGE;
import static com.epam.newsmanagement.util.ConstantPool.SEARCH_CRITERIA;
import static com.epam.newsmanagement.util.ConstantPool.PAGE_NUMBER;
import static com.epam.newsmanagement.util.ConstantPool.PAGES_COUNT;
import static com.epam.newsmanagement.util.ConstantPool.AUTHOR_LIST;
import static com.epam.newsmanagement.util.ConstantPool.TAG_LIST;
import static com.epam.newsmanagement.util.ConstantPool.FULL_NEWS_LIST;
/**
 * Controller for show-news page. The controller is in charge of showing,
 * deleting and filtering full news records.
 * 
 * @author Kiryl Sprynchan
 */
@Controller
public class NewsListController {
    
	@Autowired
	private NewsManagementService newsManagerService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private TagService tagService;

	/**
	 * Show show-news page, with list of news according to search criteria.
	 * 
	 * @param pageNumber
	 *            number of current page
	 * @return show-news page
	 * @throws ServiceException
	 */
	@RequestMapping("/show-news")
	public String showNewsList(
			@RequestParam(defaultValue = "1") int pageNumber, Model model,
			HttpSession session) throws ServiceException {
		SearchCriteria searchCriteria = (SearchCriteria) session
				.getAttribute(SEARCH_CRITERIA);

		session.setAttribute(PAGE_NUMBER, pageNumber);
		setPageInformation(pageNumber, model, searchCriteria);
		return "show-news";
	}

	/**
	 * Delete list of news according to checked check boxes on the news-list
	 * page.
	 * 
	 * @param editList
	 *            list of news to delete
	 * @return show-news page
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/delete-news", method = RequestMethod.POST)
	public String deleteNews(
			@RequestParam(value = "edit", required = false) String[] editList,
			Model model, HttpSession session) throws ServiceException {
		Long newsId = null;
		int pageNumber = (Integer) session.getAttribute(PAGE_NUMBER);

		SearchCriteria searchCriteria = (SearchCriteria) session
				.getAttribute(SEARCH_CRITERIA);

		for (String str : editList) {
			newsId = Long.parseLong(str);
			newsManagerService.deleteNews(newsId);
		}
		
		setPageInformation(pageNumber, model, searchCriteria);
		return "redirect:/show-news";
	}

	/**
	 * Set filter value, according to selected on news-list page filter
	 * parameters.
	 * 
	 * @param authorId
	 *            author id for searching
	 * @param tagsIdList
	 *            tags id list for searching
	 * @return show-news page
	 * @throws ServiceException
	 */
	@RequestMapping("/filter-news")
	public String filterNews(
			@RequestParam(required = false) Long author, @RequestParam(required = false) String[] tagList,
			Model model, HttpSession session) throws ServiceException {
			session.removeAttribute(SEARCH_CRITERIA);
			SearchCriteria searchCriteria = new SearchCriteria();
			searchCriteria.setAuthorId(author);
			
			
			List<Long> newTagList = new ArrayList<Long>();
			if(tagList != null){
				for (String str : tagList) {
					newTagList.add(Long.parseLong(str));
				}
				searchCriteria.setIdTagsList(newTagList);
			}

			session.setAttribute(SEARCH_CRITERIA, searchCriteria);

			setPageInformation(1, model, searchCriteria);

			
		return "show-news";
	}
	
	/**
	 * Reset filter status, by creating a new Search Criteria object.
	 * 
	 * @param model
	 *            of Model class
	 * @return show-news page
	 */
	@RequestMapping("/reset-searchCriteria")
	public String resetSearchCriteria(
			Model model, HttpSession session) throws ServiceException {
		session.removeAttribute(SEARCH_CRITERIA);

		setPageInformation(1, model, null);
		return "show-news";
	}
	
	/**
	 * Set news, author list and tag list on the show-news jsp page.
	 * 
	 * @param pageNumber
	 *            number of page
	 * @param searchCriteria
	 * 				of SearchCriteria class
	 * @throws ServiceException
	 */
	private void setPageInformation(int pageNumber, Model model, SearchCriteria searchCriteria) throws ServiceException {

			List<Author> authorList = authorService.readAll();
			List<Tag> tagList = tagService.readAll();


			List<PostedNews> fullNewsList = newsManagerService.readNewsOnPage(
					pageNumber, NEWS_COUNT_ON_PAGE, searchCriteria);
			
			List<News> newsList = newsService.getSortedNews(searchCriteria);
			int pagesCount = NewsManager.getPagesCount(newsList,
					NEWS_COUNT_ON_PAGE);
			
			model.addAttribute(PAGES_COUNT, pagesCount);
			model.addAttribute(AUTHOR_LIST, authorList);
			model.addAttribute(TAG_LIST, tagList);
			model.addAttribute(FULL_NEWS_LIST, fullNewsList);

	}
}
