package com.epam.newsmanagement.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.epam.newsmanagement.exception.ServiceException;

@ControllerAdvice
public class ExceptionHandleController {
	/**
     * This is logger which print some messages to log file
     */
    private static final Logger logger = Logger.getLogger(ExceptionHandleController.class);

	/**
	 * Handle exceptions that can be thrown in controllers` methods
	 * 
	 * @param ex
	 * 				of ServiceException class
	 */
	@ExceptionHandler(ServiceException.class)
	public String handleServiceException(ServiceException ex){
		logger.error("Caught an exception!", ex);
		return "server-error-page";
	}
}
