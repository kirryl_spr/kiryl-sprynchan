<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="Resources"/>

<div class="header-container">
	<p class="page-title"><fmt:message key="news_portal"/></p>
	<div class="localization">
		<span> 
		<a href="controller?command=CHANGE_LOCALE&locale=en"><fmt:message key="en"/></a> 
		<a href="controller?command=CHANGE_LOCALE&locale=ru"><fmt:message key="ru"/></a>
		</span>
	</div>
</div>