package com.epam.newsmanagement.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.command.CommandFactory;
import com.epam.newsmanagement.exception.CommandException;

/**
 * This class implements the pattern MVC
 * This is Servlet which handles requests
 *
 * @author Kirill
 */

@WebServlet(name = "controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 7116146758491777282L;
	/**
     * This is logger which print some messages to log file
     */
    private static final Logger logger = Logger.getLogger(Controller.class);

    /**
     * URL to error page
     */
	private static String FORWARD_ERROR_PAGE = "WEB-INF/views/pages/error-page.jsp";
	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws java.io.IOException
	 *
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		  try {
	            Command command = CommandFactory.getCommand(request);

	            command.processRequest(request, response);

	            if(request.getMethod().equals("POST")){
	            	response.sendRedirect(command.getForward());
	            }
	            else{
	            	request.setCharacterEncoding("UTF-8");
		            request.getRequestDispatcher(command.getForward()).forward(request, response);
	            }
	            
	        } catch (IOException ex) {
	        	request.getRequestDispatcher(FORWARD_ERROR_PAGE).forward(request, response);
	            logger.error("Caught an exception!",ex);
	        } catch (NumberFormatException ex) {
	        	request.getRequestDispatcher(FORWARD_ERROR_PAGE).forward(request, response);
	            logger.error("Caught an exception!", ex);
	        } catch (CommandException ex) {
	        	request.getRequestDispatcher(FORWARD_ERROR_PAGE).forward(request, response);
	            logger.error("Caught an exception!", ex);
	        }
		  }
	
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
