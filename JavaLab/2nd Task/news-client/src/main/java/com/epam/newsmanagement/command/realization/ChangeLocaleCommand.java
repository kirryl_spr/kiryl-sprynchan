package com.epam.newsmanagement.command.realization;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.util.NewsClientManager;

import static com.epam.newsmanagement.util.ConstantPool.LOCALE;
import static com.epam.newsmanagement.util.ConstantPool.SEARCH_CRITERIA;


/**
 * This class implements a pattern command Realization of changing locale in
 * whole application action
 * 
 * @author Kirill
 */
@Component
public class ChangeLocaleCommand extends Command {

	/**
	 * Required service
	 */
	@Autowired
	private AuthorService authorService;

	/**
	 * Required service
	 */
	@Autowired
	private NewsManagementService newsManagerService;

	/**
	 * Required service
	 */
	@Autowired
	private NewsService newsService;

	/**
	 * Required service
	 */
	@Autowired
	private TagService tagService;

	/**
	 * Util bean for static methods
	 */
	@Autowired
	private NewsClientManager newsClientManager;

	/**
	 * URL of page after action.
	 */
	private static final String FORWARD_PAGE = "WEB-INF/views/pages/main-page.jsp";

	/**
	 * This is abstract method which causes methods a business-logic and sends
	 * results on jsp
	 *
	 * @param request
	 *            a httpServletRequest
	 * @param response
	 *            a httpServletResponse
	 * @throws com.epam.sprynchan.web_project.command.exception.CommandException
	 *             a ServletException
	 */
	@Override
	public void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws CommandException {
		try {
			String locale = request.getParameter(LOCALE);
			validateLocale(locale);
			request.getSession().setAttribute(LOCALE, locale);
			
			SearchCriteria searchCriteria = (SearchCriteria) request
					.getSession().getAttribute(SEARCH_CRITERIA);

			newsClientManager.setPageInformation(request, searchCriteria);

			setForward(FORWARD_PAGE);
		} catch (ServiceException e) {
			throw new CommandException(e.getMessage(), e);
		}
	}
	
	/**
	 * This method which checks valid locale state
	 *
	 * @param locale
	 * @throws com.epam.sprynchan.web_project.command.exception.CommandException
	 *             a ServletException
	 */
	private void validateLocale(String locale) throws CommandException{
		if(locale.equals("ru") || locale.equals("en")){

		}else{
			throw new CommandException("Wrong locale in request!");
		}
	}
}
