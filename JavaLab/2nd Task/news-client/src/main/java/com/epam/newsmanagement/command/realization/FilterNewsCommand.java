package com.epam.newsmanagement.command.realization;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.util.NewsClientManager;

import static com.epam.newsmanagement.util.ConstantPool.SEARCH_CRITERIA;
import static com.epam.newsmanagement.util.ConstantPool.TAG_LIST;
import static com.epam.newsmanagement.util.ConstantPool.AUTHOR;

/**
 * This class implements a pattern command
 * Realization of filtering news by Search criteria object action
 * @author Kirill
 */
@Component
public class FilterNewsCommand extends Command{
	
	/**
	 * Required service
	 */
	@Autowired
	private AuthorService authorService;

	/**
	 * Required service
	 */
	@Autowired
	private NewsManagementService newsManagerService;
	
	/**
	 * Required service
	 */
	@Autowired
	private NewsService newsService;

	/**
	 * Required service
	 */
	@Autowired
	private TagService tagService;
	
	/**
	 * Util bean for static methods
	 */
	@Autowired
	private NewsClientManager newsClientManager;
	/**
	 * URL of page after action.
	 */
	private static String FORWARD_PAGE = "WEB-INF/views/pages/main-page.jsp";
	
	/**
     * This is abstract method which causes methods a business-logic and sends results on jsp
     *
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws com.epam.sprynchan.web_project.command.exception.CommandException a ServletException
     */
	@Override
	public void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws CommandException {
		try {
			SearchCriteria searchCriteria = checkSearchCriteria(request);
			
			
			newsClientManager.setPageInformation(request, searchCriteria);
			
			request.getSession().setAttribute(SEARCH_CRITERIA, searchCriteria);
			
			setForward(FORWARD_PAGE);
		} catch (ServiceException e) {
			throw new CommandException(e.getMessage(), e);
		}
	}
	
	/**
     * Private methods that checks the state of search criteria object.
     *
     * @param request  a httpServletRequest
     * @return SearchCriteria
     */
	private SearchCriteria checkSearchCriteria(HttpServletRequest request){
		SearchCriteria searchCriteria = new SearchCriteria();
		
		Long authorId = null;
		String author = request.getParameter(AUTHOR);
		if (author != null) {
			authorId = new Long(author);
			searchCriteria.setAuthorId(authorId);
		}
		
		
		String[] searchedTagList = null;
		if (request.getParameter(TAG_LIST) != null) {
			searchedTagList = request.getParameterValues(TAG_LIST);
		}
			
		List<Long> newTagList = new ArrayList<Long>();
		if(searchedTagList != null){
			for (String str : searchedTagList) {
				newTagList.add(Long.parseLong(str));
			}
			searchCriteria.setIdTagsList(newTagList);
		}
		
		return searchCriteria;
	}

}
